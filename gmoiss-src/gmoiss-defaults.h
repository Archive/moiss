/* This file is part of MOISS
   
   Copyright (C) 1998-99 Alan Aspuru Guzik
			 Raul Perusquia Flores
			 Carlos Amador Bedolla
			 Francisco Bustamante
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
               
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
                           
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
                                    
*/

#ifndef __GMOISS_DEFAULTS_H__
#define __GMOISS_DEFAULTS_H__

const char * problem_description = "
{
\"GMoiss Version\" = 0.5;
Potential = \"Hydrogen atom\";
\"Trial Function\" = \"Hydrogen (Slater)\";
Dimensions = \"3\";
Particles = 1;
Walkers = 1000;
\"Diffusion Constant\" = 0.500000;
\"Time step\" = 0.002000;
\"Maximum Branches\" = 3;
\"dt convergence factor\" = 1.000000;
\"Initial Position\" = \"Random\";
\"Gaussian Spread Range\" = 1.000000;
\"Gaussian Trial Function Alpha\" = 0.950000;
\"Random Number Generator Seed\" = 1;
\"Potential Box X\" = 1.000000;
\"Potential Box Y\" = 1.000000;
\"Potential Box Z\" = 1.000000;
\"Tunnel Potential A\" = 0.500000;
\"Tunnel Potential B\" = 0.700000;
\"Tunnel Potential H\" = 100.000000;
\"Atomic Coordinates\" = ({x = 0;y = 0;z = 0;\"Atomic Number\" = 1;});
\"Lehnard-Jones Sigma\" = 3.405;
\"Lehnard-Jones Epsilon\" = 119.4;
\"Graph: Divisions per Dimension\" = 200;
\"Graph: Minimum\" = \"-3.000000\";
\"Graph: Maximum\" = 3.000000;
\"Electronic Repulsion Scale\" = 1.000000;
\"Spin Parity Operator\" = 1;
\"CE Number of Terms\" = 1;
\"Electron-Electron TF\" = \"Jastrow\";
\"Electron-Nuclei TF\" = \"Jastrow\";
\"CE Sym: Number of Terms\" = 4;
\"CE Sym: Matrix\" = ((1, 0), (1, 0), (1, 0), (1, 0));
\"CE Electron-Electron Symmetry\" = Yes;
\"CE Electron-Nuclei Symmetry\" = No;
\"CE Electron-Nuclei Parameters\" = 6;
\"CE Prefactor Terms\" = 2;
\"CE Prefactors\" = ((1.000),(1.000));
\"Parameters File Path\" = \"./psi.inp\";
\"Output Files Path\" = \"/tmp/moiss/\";
\"Moiss Binary Path\" = \"moiss\";
\"Job Name\" = \"hydrogen-1\";
}
";

#endif /* __GMOISS_DEFAULTS_H__ */

