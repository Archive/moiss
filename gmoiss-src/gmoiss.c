/* This is the gnome-gui for gmoiss */
 /* Alan Aspuru 1998 */
 /* All code under GPL */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <unistd.h>
#include <gnome.h>
#include <glib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <gdk/gdkx.h>
#include <zvt/zvtterm.h>
// this should be deprecated and use the gnome hack for this instead...
#include <utmp.h>

#include "pl.h"
#include "gmoiss.h"
#include "ptable.h"

#include "gmoiss-defaults.h"
#include "weed.xpm"
#include "mc.xpm"
#include "stat.xpm"
#define PLRelease(x) 
#define MAX_BUF 128 /* maximum buffer, originally 1024 */
void gmoiss_add_stats_to_tree(GtkWidget *tree, Stat *stats);
void gmoiss_calc_new(GtkWidget *widget, gpointer *data);

GtkWidget *gmoiss_window;
GtkWidget *gmoiss_tree;
GtkWidget *gmoiss_other_vbox, *gmoiss_sto_vbox, *gmoiss_atomic_vbox;
GtkWidget *gmoiss_path_vbox;
GtkWidget *gmoiss_pot_vbox; /* Potential options notebook tab */
GtkWidget *gmoiss_vbox;
GtkWidget *gmoiss_corr_vbox; /* Correlated Exponential notebook tab */
GtkWidget *gmoiss_tree_vbox;
GtkWidget *gmoiss_notebook;
GtkWidget *gmoiss_setup_vbox;
GtkWidget *gmoiss_output_vbox; /* output execution vertical box */
GtkWidget *txtOutput; /* output widget for execution of moiss see crescendo */
GtkCList *atomic_clist;
/*several entries for filenames */
GtkWidget *parameters_entry, *moiss_entry, *output_entry, *job_entry;

GtkWidget *gmoiss_output; /* zvterm where moiss is executed */

GList *mc_list; /* list of monte carlo's to perform */
GtkCTreeNode *selected_node; /* node of the list that's currently selected */
Stat *selected_stats;

GMoissData gmoiss_data; /* data only relevant to gmoiss */
GtkSpinButton *dt_spin; /* hack for changing the dt change adjustment */
GtkSpinButton *terms_spinner; /* hack for changing the number of terms */
int particles; /* needed for the set_mass_and_charges function */
int sym_terms = 4; /* needed for the set_symmetry function */ 
int prefactor_terms = 2; /* number of pf terms in the correlated exponential tf */
double * charges; /* charges for the particles */
double * masses; /* masses for the particles */
int lock = 1;
int selected_element = 1;
int atomic_current_row = 0;
int saved = 0; /* have we saved? */
int pipes[2]; /* p�pes for running moiss */
gint inputid; /* id for the pipe that runs moiss */
gsl_matrix_int * symmetry; /* matrix for the correlated exponential symmetry */
gsl_matrix_float * prefactors; /* 1xn matrix of num_prefactor for corr. exp. */

/* its an array of [particles][symterms] */

static gint about_box_visible = FALSE; /* for not having two open about boxes*/

GtkStyle *greek_style; /* style for the symbols in my_greek_label */
GnomeCanvasItem *table_selection;

/* this data strucutre is obsolete, here only for reminding which
   elements are lacking in the gui */

ProblemData problem_data  = {
	HYDROGEN_POTENTIAL, /*potential_type*/
	HYDROGEN,           /*trial_function_type*/
	THREED,             /*dimensions*/
	1,                  /*particles*/
	1000,               /*walkers*/
	0.5,                /*D */
	0.001,              /*dt*/
	3,                  /*max_branches*/
	1.0,                /*dt_convergence_factor*/
	GAUSSIAN_INITIAL,   /*initial_position*/
	1.0,                /*gaussian_spread_range */
	0.95,               /*gaussian_tf_alpha */
	11,                  /*rng_seed  */
	1.0,                /*box_x no gui */
	1.0,                /*box_y no gui*/
	1.0,                /*box_z no gui*/
	0.5,                /*tunnel_a no gui*/
	0.7,                /*tunnel_b no gui*/
	100                 /*tunnel_h no gui*/
};

GMoissData def_gmoiss = {
	0.0001 /* dt_stepvalue */
};




MonteCarlo def_mc = {
	MC_VARIATIONAL, /* MonteCarloType type */
	20,             /* blocks */
	100,            /* iterations */
	NULL,           /* GList stats */
	NULL,           /* Glist enhancements */
};

/* Pixmap stuff */

typedef struct {
	GdkPixmap *pixmap;
	GdkBitmap *mask;

	gint width;
	gint height;
} pix;

struct {
	GtkWidget *menu;
	GtkWidget *opt_menu;
	GtkEntry *block_entry;
	GtkWidget *iter_entry;
	GtkWidget *toggles[7];
} t_level;

pix *mc_pix, *stat_pix;

void my_new_greek_choice ( GtkWidget *box , char * names[], 
			   GtkSignalFunc signal_func , int default_value );
GtkWidget*  my_new_hbox ();

typedef struct {
	GtkWidget *dialog;
	GtkEntry *entry1;
	GtkEntry *entry2;
	GtkEntry *entry3;	
} AtomData;

pix *mc_pix, *stat_pix;
gushort red[] = 
{0x0000,0xaaaa,0x0000,0xaaaa,0x0000,0xaaaa,0x0000,0xaaaa,
 0x5555,0xffff,0x5555,0xffff,0x5555,0xffff,0x5555,0xffff,
 0xfafa, 0x0000};

gushort grn[] = 
{0x0000,0x0000,0xaaaa,0x5555,0x0000,0x0000,0xaaaa,0xaaaa,
 0x5555,0x5555,0xffff,0xffff,0x5555,0x5555,0xffff,0xffff,
 0xebeb, 0x0000};

gushort blu[] = 
{0x0000,0x0000,0x0000,0x0000,0xaaaa,0xaaaa,0xaaaa,0xaaaa,
 0x5555,0x5555,0x5555,0x5555,0xffff,0xffff,0xffff,0xffff,
 0xd7d7, 0x0000};



int create_pipe(int argc, char *plist[]) {

	/*
	char *plist[2] = { "/usr/local/bin/tf", NULL };
	*/
	int pid;
	int pipein[2], pipeout[2];
	
	
	/*pipe(pipein);  
	*/
	
	if(pipe(pipein) == -1){
		fprintf(stderr, "Error: %d\n", errno );
		exit(errno);
	}
	

	
	/* pipe(pipeout);
	*/
	
	
	if(pipe(pipeout) == -1){
		fprintf(stderr, "Error: %d\n", errno );
		exit(errno);
	}
	
	
	pipes[0] = pipeout[0];
	pipes[1] = pipein[1];
	pid = fork();
	if(pid == 0){
		close(0);
		dup(pipein[0]);
		close(1);
		dup(pipeout[1]);

		/*This is where it should execute tf */

		close(pipein[1]); close(pipeout[0]);
		/*
		execv(tfbin, plist);
		*/
		execvp(my_pl_get_string(dictionary_names[MOISS_PATH]), plist);
		
		_exit(0);  /* This shouldn't be called unless there's a problem. */
	}
	close(pipein[0]); close(pipeout[1]);
	return FALSE;
}


void gmoiss_init_greek (void) {
    GdkFont *font = NULL;
    
    greek_style = gtk_style_copy(gtk_widget_get_style(gmoiss_window));
    font = gdk_font_load(GREEK_FONT);
    if (font != NULL) {
	greek_style->font = font;
    }
    
}

GtkWidget * my_greek_label(char * label_string) {
    GtkWidget *label;
    gtk_widget_set_style(label, greek_style);
    
    return label;
}

    
    
void gmoiss_int_matrix_dialog(int rows, int cols, int ** data, int max_numbers,  char * entry_text) {
    GtkWidget *d, *table, *entry;
    int return_button_no;
    int i, j;
    char * text;
    GtkWidget *** entries;

    /*malloc'ing the entire widget array */

    entries = malloc (rows * sizeof(GtkWidget **));
    for (i =0; i < rows; i++)
	entries[i] = malloc(cols * sizeof (GtkWidget *));
    
    d = gnome_dialog_new(entry_text, GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
    gnome_dialog_set_default(GNOME_DIALOG(d),0);

    /* adding the default data and fill the vbox with it */
    table = gtk_table_new (rows,cols, FALSE);
    gtk_container_border_width(GTK_CONTAINER(table), GNOME_PAD_SMALL);
    gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(d)->vbox), table);
    for (i = 0; i < rows; i++)
	{
	    for (j = 0; j < cols; j++) {
		/* creating the entry widget */
		entry = gtk_entry_new();
		text = g_malloc(max_numbers);
		snprintf(text, max_numbers, "%d", data[i][j]);
		gtk_entry_set_text(GTK_ENTRY(entry), text);
		g_free(text);
		entries[i][j] = entry;
		gtk_table_attach (GTK_TABLE(table), entry, j, j+1, i, i+1, GTK_SHRINK, GTK_SHRINK, 0,0);
	    }
	}
    
    /* creating the table */
    	gtk_table_set_row_spacings (GTK_TABLE (table), 0);
	gtk_table_set_col_spacings (GTK_TABLE (table), 0);
    gtk_widget_show_all(table);
    gnome_dialog_close_hides(GNOME_DIALOG(d), YES);
    return_button_no = gnome_dialog_run_and_close(GNOME_DIALOG(d));
    switch(return_button_no) {
	case GNOME_OK:
	    /* fetching the values from the entries and putting them in
	       the array.
	       We take advantage of the loop and g_free the entries.
	    */
	    for (i = 0; i < rows; i++)
		{
		    for (j = 0; j < cols; j++)
			{
			    text = gtk_entry_get_text(GTK_ENTRY(entries[i][j]));
			    data[i][j] = atoi(text);
			    
			}
		    g_free(entries[i]);
		}
	    g_free(entries);
	    gtk_widget_destroy(d);
	    break;
	case GNOME_CANCEL:
	    gtk_widget_destroy(d);
	    return;
	    break;
    }
    
}
void gmoiss_float_matrix_dialog(int rows, int cols, gsl_matrix_float * data, int max_numbers,  char * entry_text) {
    GtkWidget *d, *table, *entry;
    int return_button_no;
    int i, j;
    char * text;
    GtkWidget *** entries;

    /*malloc'ing the entire widget array */

    entries = malloc (rows * sizeof(GtkWidget **));
    for (i =0; i < rows; i++)
	entries[i] = malloc(cols * sizeof (GtkWidget *));
    
    d = gnome_dialog_new(entry_text, GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
    gnome_dialog_set_default(GNOME_DIALOG(d),0);

    /* adding the default data and fill the vbox with it */
    table = gtk_table_new (rows,cols, FALSE);
    gtk_container_border_width(GTK_CONTAINER(table), GNOME_PAD_SMALL);
    gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(d)->vbox), table);
    for (i = 0; i < rows; i++)
	{
	    for (j = 0; j < cols; j++) {
		/* creating the entry widget */
		entry = gtk_entry_new();
		text = g_malloc(max_numbers);
		snprintf(text, max_numbers, "%f", gsl_matrix_float_get(data , i , j));
		gtk_entry_set_text(GTK_ENTRY(entry), text);
		g_free(text);
		entries[i][j] = entry;
		gtk_table_attach (GTK_TABLE(table), entry, j, j+1, i, i+1, GTK_SHRINK, GTK_SHRINK, 0,0);
	    }
	}
    
    /* creating the table */
    	gtk_table_set_row_spacings (GTK_TABLE (table), 0);
	gtk_table_set_col_spacings (GTK_TABLE (table), 0);
    gtk_widget_show_all(table);
    gnome_dialog_close_hides(GNOME_DIALOG(d), YES);
    return_button_no = gnome_dialog_run_and_close(GNOME_DIALOG(d));
    switch(return_button_no) {
	case GNOME_OK:
	    /* fetching the values from the entries and putting them in
	       the array.
	       We take advantage of the loop and g_free the entries.
	    */
	    for (i = 0; i < rows; i++)
		{
		    for (j = 0; j < cols; j++)
			{
			    text = gtk_entry_get_text(GTK_ENTRY(entries[i][j]));
			    gsl_matrix_float_set(data, i , j , atof(text));
			    
			}
		    g_free(entries[i]);
		}
	    g_free(entries);
	    gtk_widget_destroy(d);
	    break;
	case GNOME_CANCEL:
	    gtk_widget_destroy(d);
	    return;
	    break;
    }
    
}

gboolean
check_toggles (GtkCTree *ctree, GtkCTreeNode *node, gpointer data)
{
	MonteCarlo *mc;
	
	mc = (MonteCarlo *)gtk_ctree_node_get_row_data (GTK_CTREE(gmoiss_tree), node);

/*  	if (mc->type < 6) */
/*  		gtk_toggle_button_set_state (GTK_TOGGLE_BUTTON(t_level.toggles[mc->type]), TRUE); */
}

void
t_level_set_toggles (GtkWidget *widget, GtkCTreeNode *node)
{
    int i;
    
    lock = 0;
    
	for(i = 0; *stat_names[i]; i++) {
	    gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(t_level.toggles[i]), FALSE);
	}
	gtk_ctree_post_recursive (GTK_CTREE(gmoiss_tree), selected_node,
				  (GtkCTreeFunc)check_toggles, NULL);

	lock = 1;
}

int
get_menu_index (gchar *string)
{
	int i = 0;
	
	while(i < 4) {
		if(strcmp(string, mc_names[i]) == 0) {
			return i;
		}
		i++;
	}
	return 0;
}

proplist_t
mc_list_to_pl (GList *list)
{
    proplist_t mc_pl, this_mc_type, this_mc_steps,
       this_mc_blocks, this_mc_stats, this_stat, mc_pl_key, this_mc,
       ths_mc_blocks_key, this_mc_stats_key, this_mc_steps_key,
       this_mc_type_key, this_mc_blocks_key, this_stat_key;
 
   char * text[2];
   
   GList *l, *m, *stat_list;
   MonteCarlo *mc;
   Stat *stat;

   int length;
   
    this_mc_type_key = PLMakeString(calculation_names[CALCULATION_TYPE]);
    this_mc_blocks_key = PLMakeString(calculation_names[BLOCKS]);
    this_mc_steps_key = PLMakeString(calculation_names[STEPS]);
    this_mc_stats_key = PLMakeString(calculation_names[STATISTICS]);

    length = g_list_length(list);
    mc_pl = PLMakeArrayFromElements(NULL);
    
    printf("List length: %d\n", length);
    for (l = list; l != NULL; l = l->next)
	{
	    text[0] = g_malloc(11);
	    text[1] = g_malloc(11);
	    
	  
	    
	    mc = (MonteCarlo *) l->data;
	    printf("El numero es %d y el tipo es %s\n", mc->type ,mc_names[mc->type-MC_VARIATIONAL]);
	    this_mc_type = PLMakeString(mc_names[mc->type-MC_VARIATIONAL]);
	    
	    snprintf(text[0], 11, "%d", mc->blocks);
	    snprintf(text[1], 11, "%d", mc->iterations);
	    
	    this_mc_blocks = PLMakeString(text[0]);
	    this_mc_steps = PLMakeString(text[1]);
	    
	    /* Stats stuff */
	    
	    this_mc_stats = PLMakeArrayFromElements(NULL);
	    
	    for (m = mc->stats; m != NULL; m = m->next) {
		stat = (Stat *) m->data;
		this_stat = PLMakeString(stat_names[stat->type]);
		PLAppendArrayElement(this_mc_stats , this_stat);
	    }
	    
	    /* End Stats stuff */
	    
	    
	    
	    this_mc = PLMakeDictionaryFromEntries(this_mc_type_key,
						  this_mc_type,
						  this_mc_blocks_key,
						  this_mc_blocks,
						  this_mc_steps_key,
						  this_mc_steps,
						  this_mc_stats_key,
						  this_mc_stats,
						  NULL);
	    PLAppendArrayElement (mc_pl, this_mc);
	    g_free(text[0]);
	    g_free(text[1]);
	}
   
    
    
   
    /* Releasing all the proplists used */
    PLRelease(this_mc_stats_key);
    PLRelease(this_mc_steps_key);
    PLRelease(this_mc_type_key);
    PLRelease(this_mc_blocks_key);
         
    return mc_pl;
}
void size_allocate (GtkWidget *widget) {

        ZvtTerm *term;
        GnomeApp *app;
        XSizeHints sizehints;

        g_assert (widget != NULL);
        term = ZVT_TERM (widget);

        app = GNOME_APP (gtk_widget_get_toplevel (GTK_WIDGET (term)));
        g_assert (app != NULL);

        sizehints.base_width = 
          (GTK_WIDGET (app)->allocation.width) +
          (GTK_WIDGET (term)->style->klass->xthickness * 2) -
          (GTK_WIDGET (term)->allocation.width);

        sizehints.base_height =
          (GTK_WIDGET (app)->allocation.height) +
          (GTK_WIDGET (term)->style->klass->ythickness * 2) -
          (GTK_WIDGET (term)->allocation.height);

        sizehints.width_inc = term->charwidth;
        sizehints.height_inc = term->charheight;
        sizehints.min_width = sizehints.base_width + sizehints.width_inc;
        sizehints.min_height = sizehints.base_height + sizehints.height_inc;

        sizehints.flags = (PBaseSize|PMinSize|PResizeInc);

        XSetWMNormalHints (GDK_DISPLAY(),
                           GDK_WINDOW_XWINDOW (GTK_WIDGET (app)->window),
                           &sizehints);
        gdk_flush ();

}



void
mc_add_callback (GtkWidget *widget, gpointer data)
{
	MonteCarlo *mc;
	
	mc = g_malloc(sizeof(MonteCarlo));

	mc->type = def_mc.type;
	mc->blocks = def_mc.blocks;
	mc->iterations = def_mc.iterations;
	mc->stats = NULL;
	printf ("ADD\n");

	gmoiss_add_mc_to_tree(gmoiss_tree, mc);

	mc_list = g_list_append(mc_list, mc);
}

void
mc_remove_callback (GtkWidget *widget, gpointer data)
{
	if (selected_node) {
		gtk_ctree_remove_node (GTK_CTREE(gmoiss_tree), selected_node);
		selected_node = NULL;
	}
}

void change_spin (GtkWidget *widget, gpointer data) {}

void init_globals(void)
{
    int i;
    mc_list = NULL;
    calculation_filename = PLMakeString(DEFAULT_FILE);
    gmoiss_tree = NULL;
    calculation_dict = PLGetProplistWithDescription(problem_description);
    
    /* setting the number of particles, charges and masses to the defaults */
    particles = my_pl_get_int(dictionary_names[PARTICLES]);
	/* now setting the sym_terms accordingly */
	switch (particles) {
	case 2:
	  sym_terms = 2;
	  break;
	case 3:
	  sym_terms = 4;
	  break;
	case 4:
	  sym_terms = 16;
	  break;
	default:
	  sym_terms = my_pl_get_int(dictionary_names[SYM_TERMS]);
	  break;
	}
    charges = g_new(double, particles);
    masses = g_new(double, particles);
    /* setting the default values */
    for (i = 0; i < particles; i ++)
	{
	    charges[i] = -1.0;
	    masses[i] = 1.0;
	}
	/* now reading the default symmetry matrix */
	symmetry = my_pl_get_matrix_int(dictionary_names[SYM_MATRIX]);
	/* now reading the default prefactors */
	prefactors = my_pl_get_matrix_float(dictionary_names[CE_PREFACTORS]);
	prefactor_terms = my_pl_get_int(dictionary_names[CE_PREFACTOR_TERMS]);
}

pix *pix_new(char **xpm)
{
	GdkImlibImage *image;
	pix *new_pix;

	new_pix = g_malloc(sizeof(pix));

	image = gdk_imlib_create_image_from_xpm_data(xpm);
	gdk_imlib_render (image, 16, 16);
	new_pix->pixmap = gdk_imlib_move_image (image);
	new_pix->mask = gdk_imlib_move_mask (image);
	gdk_imlib_destroy_image (image);
	gdk_window_get_size(new_pix->pixmap, 
			    &(new_pix->width), &(new_pix->height));

	return new_pix;
}

void input_read ( gpointer data, gint source, GdkInputCondition condition )
{
        
  gint length;
  gchar buffer[MAX_BUF+1];
  gchar zvt_buf[(MAX_BUF*2)+1]; 
  gint i;
  static  gint i3 = 0;

  length = read(pipes[0], buffer, MAX_BUF);
  
  if(length == 0){
    gdk_input_remove(inputid);
    return;
  }
  
  /* Now to clean up buffer */
  i=0;
  i3=0;
  
  while (i < length) {
    if (buffer[i] == '\n') {
      zvt_buf[i3] = '\r';
      zvt_buf[i3 + 1] = '\n';
      i3 += 2;
    } else {
      zvt_buf[i3] = buffer[i];
      i3++;
    }
    i++;
  }
  
  
	
	
  zvt_buf[i3]=0x00; 
  zvt_term_feed(ZVT_TERM(gmoiss_output), zvt_buf, i3); 
  
}

void gmoiss_moiss_in_notebook (int argc, char *argv[]) {
  GtkWidget *vpane, *output, *vscrollbar;
  
  
  /* gmoiss_output_vbox, gmoiss_output */
  /* entirely stolen from crescendo */
  
  
  output = gtk_hbox_new(FALSE, 0);
  
  gtk_box_pack_start(GTK_BOX(gmoiss_output_vbox), output, TRUE, TRUE, 0);
  vpane = gtk_vpaned_new();
  
  gtk_box_pack_start(GTK_BOX(output), vpane, TRUE, TRUE, 0);
  gtk_widget_realize(vpane);
  gmoiss_output = zvt_term_new();
  
  gtk_paned_add2(GTK_PANED(vpane), gmoiss_output);
  zvt_term_set_size(ZVT_TERM(gmoiss_output), 10, 20);
  zvt_term_set_scrollback(ZVT_TERM(gmoiss_output), 2000);
  zvt_term_set_bell(ZVT_TERM(gmoiss_output), TRUE);
  // using the default FONT FIXME later;
  //zvt_term_set_font_name(ZVT_TERM(gmoiss_output), trmfont);
  //gtk_widget_realize(gmoiss_output);
  //gtk_widget_realize(vpane);
  //zvt_term_set_color_scheme( (ZvtTerm *)gmoiss_output, red, grn, blu );
  gtk_signal_connect_after (GTK_OBJECT (gmoiss_output), "size_allocate",
							GTK_SIGNAL_FUNC (size_allocate), gmoiss_output);
  
  vscrollbar = gtk_vscrollbar_new  (GTK_ADJUSTMENT (ZVT_TERM(gmoiss_output)->adjustment));
  gtk_box_pack_start(GTK_BOX(output), vscrollbar, FALSE, FALSE, 0);
  //gtk_widget_show(vscrollbar);
  
  // gtk_widget_show(gmoiss_output);
  
  //gtk_widget_show(vpane);
	
  // gtk_widget_show(output);
  gtk_widget_show_all(gmoiss_output_vbox);
  create_pipe(argc, argv);

  /* and now hooking the input */
  inputid = gdk_input_add(pipes[0], GDK_INPUT_READ, input_read, txtOutput);
}

void gmoiss_cancel(GtkWidget *widget, gpointer data)
{
	gtk_widget_destroy(widget);
}

void general_cancel(GtkWidget *widget, gpointer data)
{
	GtkWidget *tmp = data;
	
	gtk_widget_destroy(tmp);
}

void atomic_clist_select_callback (GtkWidget *widget, gint row, gint column,
				   GdkEventButton *event, gpointer data)
{
	atomic_current_row = row;
}

GtkWidget *create_list (void) {
	GtkWidget *list, *scrolled_win;
	int i = 0;
	char *row[2];
	char tmp[5];
	char *title1 = "Symbol";
	char *title2 = "Z";

	row[0] = title1;
	row[1] = title2;
	
	list = gtk_clist_new_with_titles (2, row);
	scrolled_win = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_win),
					GTK_POLICY_NEVER, 
					GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (scrolled_win), list);
	
	gtk_widget_show (list);
	row[1] = tmp;
	while (atomic_names[i] != NULL) {
		sprintf(tmp, "%d", i);
		row[0] = atomic_names[i];
		gtk_clist_insert (GTK_CLIST(list), i, row);
		i++;
	}
	gtk_signal_connect (GTK_OBJECT (list), "select_row",
			    GTK_SIGNAL_FUNC (atomic_clist_select_callback),
			    NULL);
	gtk_clist_select_row (GTK_CLIST(list), 0, 0);
	gtk_clist_set_selection_mode (GTK_CLIST(list), GTK_SELECTION_BROWSE);
	return scrolled_win;
}

void element_clicked (GtkWidget *widget, GdkEvent *event, TableElement *element)
{
	double dx, dy;
	double current_x = 0.0, current_y = 0.0;

	switch (event->type) {
		case GDK_BUTTON_PRESS:
			current_x = (pt_elements[selected_element - 1].x * 28);
			current_y = (pt_elements[selected_element - 1].y * 30);
			
			dx = (element->x * 28) - current_x;
			dy = (element->y * 30) - current_y;

			gnome_canvas_item_move (table_selection, dx, dy);
			
			selected_element = element->index;
			break;
		default:
			break;
	}
}

GtkWidget *create_periodic_table (void) {
	GtkWidget *canvas;
	GnomeCanvasItem *item;
	GnomeCanvasGroup *group;
	gchar fill_color[8];
	int i = 0;
	int num_elements = sizeof (pt_elements) / sizeof (pt_elements[0]);

	selected_element = 1;
	canvas = gnome_canvas_new ();
	gtk_widget_pop_colormap();
	gtk_widget_pop_visual ();

	gtk_widget_set_usize (canvas, 508, 304);

	gnome_canvas_set_scroll_region (GNOME_CANVAS (canvas),
					0.0, 0.0, 508.0, 304.0);
	
	while (i < num_elements) {
		sprintf (fill_color, "#%.2X%.2X%.2X", pt_elements[i].red, pt_elements[i].green, pt_elements[i].blue);
		group = GNOME_CANVAS_GROUP (gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS(canvas)),
								   gnome_canvas_group_get_type (),
								   "x", 0.0,
								   "y", 0.0,
								   NULL));
		item = gnome_canvas_item_new (group,
					      gnome_canvas_rect_get_type (),
					      "x1", (double)(2 + pt_elements[i].x * 28),
					      "x2", (double)(2 + (pt_elements[i].x + 1) * 28),
					      "y1", (double)(2 + pt_elements[i].y * 30),
					      "y2", (double)(2 + (pt_elements[i].y + 1) * 30),
					      "fill_color", fill_color,
					      "outline_color", "black",
					      NULL);
		gnome_canvas_item_show (item);
		gtk_signal_connect (GTK_OBJECT(group), "event",
				    (GtkSignalFunc)element_clicked,
				    &pt_elements[i]);
 		item = gnome_canvas_item_new (group, 
 					      gnome_canvas_text_get_type (), 
 					      "text", pt_elements[i].symbol, 
 					      "anchor", GTK_ANCHOR_CENTER, 
 					      "font", "-b&h-lucida-bold-r-normal-*-12-*-*-*-p-*-iso8859-1", 
 					      "justification", GTK_JUSTIFY_CENTER,  
 					      "x", (double)(16 + pt_elements[i].x * 28), 
 					      "y", (double)(17 + pt_elements[i].y * 30), 
 					      NULL); 
 		gnome_canvas_item_show (item); 
		i++;
	}
	table_selection = item = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS(canvas)),
							gnome_canvas_rect_get_type (),
							"x1", (double)(4 + pt_elements[0].x * 28),
							"x2", (double)(1 + (pt_elements[0].x + 1) * 28),
							"y1", (double)(4 + pt_elements[0].y * 30),
							"y2", (double)(1 + (pt_elements[0].y + 1) * 30),
							"width_pixels", 2,
							"outline_color", "red",
							NULL);
	gnome_canvas_item_show (item);
	
	return canvas;
}

void add_atom (GtkWidget *widget, AtomData *atom_data)
{
	char *text[6];
	gint row;
	
	text[0] = malloc (4);
	strcpy (text[0], "0");
	text[1] = malloc (5);
	sprintf (text[1], "%d", selected_element);
	text[2] = atomic_names[selected_element];
	text[3] = malloc (strlen(gtk_entry_get_text (atom_data->entry1)) + 1);
	strcpy (text[3], gtk_entry_get_text (atom_data->entry1));
	text[4] = malloc (strlen(gtk_entry_get_text (atom_data->entry2)) + 1);
	strcpy (text[4], gtk_entry_get_text (atom_data->entry2));
	text[5] = malloc (strlen(gtk_entry_get_text (atom_data->entry3)) + 1);
	strcpy (text[5], gtk_entry_get_text (atom_data->entry3));

	row = gtk_clist_append (atomic_clist ,text);
	gtk_clist_select_row (GTK_CLIST(atomic_clist), row, 0);
	gtk_clist_moveto (atomic_clist, row, -1, 0.0, 0.0);
	gtk_widget_destroy (atom_data->dialog);
}

void atomic_add_callback(GtkWidget *widget, gpointer data)
{
	GtkWidget *dialog;
	GtkWidget *button;
	GtkWidget *entry;
	GtkWidget *table;
	GtkWidget *label;
	GtkWidget *frame;
	GtkWidget *list;
	GtkDialog *d;
	GtkWidget *periodic_tbl;
	AtomData *new_atom_data;

	new_atom_data = malloc (sizeof(AtomData));
	
	new_atom_data->dialog = dialog = gtk_dialog_new ();

	d = GTK_DIALOG (dialog);

	/*	gtk_signal_connect (GTK_OBJECT(pref_dialog), "delete_event",
		(GtkSignalFunc) atom_add_cancel, NULL);*/
	
	frame = gtk_frame_new ("Coordinates");
	gtk_box_pack_start (GTK_BOX(d->vbox), frame, FALSE, TRUE, GNOME_PAD_SMALL);
	
	table =	gtk_table_new (3, 3, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE(table), 5);
	gtk_table_set_col_spacings (GTK_TABLE(table), 5);
	gtk_container_add (GTK_CONTAINER (frame), table);
	gtk_container_border_width (GTK_CONTAINER (d->vbox), GNOME_PAD_SMALL);

	label = gtk_label_new ("x = ");
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1,
			  GTK_FILL, GTK_FILL, 0, 0);
	
	new_atom_data->entry1 = GTK_ENTRY(entry = gtk_entry_new ());
	gtk_entry_set_text (GTK_ENTRY(entry), "0.00");
	gtk_table_attach (GTK_TABLE(table), entry, 1, 2, 0, 1,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0, 0);

	label = gtk_label_new ("y = ");
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2,
			  GTK_FILL, GTK_FILL, 0, 0);
	
	new_atom_data->entry2 = GTK_ENTRY(entry = gtk_entry_new ());
	gtk_entry_set_text (GTK_ENTRY(entry), "0.00");
	gtk_table_attach (GTK_TABLE(table), entry, 1, 2, 1, 2,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0, 0);

	label = gtk_label_new ("z = ");
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3,
			  GTK_FILL, GTK_FILL, 0, 0);
	
	new_atom_data->entry3 = GTK_ENTRY(entry = gtk_entry_new ());
	gtk_entry_set_text (GTK_ENTRY(entry), "0.00");
	gtk_table_attach (GTK_TABLE(table), entry, 1, 2, 2, 3,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0, 0);

	frame = gtk_frame_new ("Atom");
	gtk_box_pack_start (GTK_BOX(d->vbox), frame, TRUE, TRUE, GNOME_PAD_SMALL);

	table = gtk_table_new (1, 2, FALSE);
	gtk_container_add (GTK_CONTAINER (frame), table);

	
	periodic_tbl = create_periodic_table ();
	gtk_table_attach (GTK_TABLE (table), periodic_tbl, 0, 2, 0, 1,
			  GTK_FILL, GTK_FILL, 0, 0);

	button = gtk_button_new_with_label (_("Add"));
	gtk_box_pack_start (GTK_BOX(d->action_area), button, TRUE, TRUE, GNOME_PAD_SMALL);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    (GtkSignalFunc)add_atom,
			    new_atom_data);

	button = gtk_button_new_with_label (_("Cancel"));
	gtk_box_pack_start (GTK_BOX(d->action_area), button, TRUE, TRUE, GNOME_PAD_SMALL);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    (GtkSignalFunc) general_cancel, dialog);

	gtk_widget_show_all(dialog);
}

void atomic_remove_callback(GtkWidget *widget, gpointer data)
{
	gtk_clist_remove (atomic_clist, atomic_current_row);
}

void set_prefactor_terms(GtkWidget *widget, gpointer data) {
  if (prefactors != NULL)
	g_free(prefactors);
  prefactors = gsl_matrix_float_calloc(prefactor_terms,1);
  gmoiss_float_matrix_dialog(prefactor_terms, 1, prefactors, 12, "Enter Prefactor Terms");
}

void set_symmetry(GtkWidget *widget, gpointer data) {
    GtkWidget *d, *table, *entry, *label;
    int return_button_no;
    int i,j;
    char * text;
    int max_numbers = 12;
    GtkWidget ** row_signs;
    GtkWidget *** entries;
    GtkWidget *choice, *button;
    GSList **groups;

    if (symmetry != NULL) g_free(symmetry);
   	
	symmetry = gsl_matrix_int_calloc(sym_terms, particles+1);

    groups = malloc (sym_terms * sizeof (GtkWidget *));
    
    entries = malloc (particles * sizeof(GtkWidget **));
    for (i =0; i < particles; i++)
	  entries[i] = malloc(sym_terms * sizeof (GtkWidget *));
	
    row_signs = malloc (  sym_terms* sizeof (GtkWidget *));
      d = gnome_dialog_new(_("Set Trial Function Symmetry"), GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
	  gnome_dialog_set_default(GNOME_DIALOG(d),0);
    table = gtk_table_new (sym_terms+1,particles+1, FALSE);
    gtk_container_border_width(GTK_CONTAINER(table), GNOME_PAD_SMALL);
    gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(d)->vbox), table);
    
    /* creating the title options */
    for (i = 1; i < (particles + 1); i++)
	  {
	  choice = my_new_hbox();
	    my_new_greek_choice(choice,
							spin_names,
							GTK_SIGNAL_FUNC(change_spin), 0);
	    gtk_table_attach(GTK_TABLE (table), choice, i, i+1, 0, 1, GTK_SHRINK, GTK_SHRINK, 0, 0);
	}

    /* now adding the row signs and the arrays. */
    for (i = 0; i < sym_terms; i++) {
	/* row_signs */
	  choice = my_new_hbox();
	button = gtk_radio_button_new_with_label (NULL, _("+"));
	gtk_box_pack_start (GTK_BOX (choice), button, TRUE, TRUE, 0);
	/* turning it "on" */
	gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(button) , TRUE );
	/* adding it to the list for later checks */
	row_signs[i] = button;
	gtk_widget_show (button);
	groups[i] = gtk_radio_button_group (GTK_RADIO_BUTTON (button));
	button = gtk_radio_button_new_with_label (groups[i], _("-"));
	gtk_box_pack_start (GTK_BOX (choice), button, TRUE, TRUE, 0);
	gtk_widget_show (button);
	groups[i] = gtk_radio_button_group (GTK_RADIO_BUTTON (button));
	gtk_table_attach (GTK_TABLE(table), choice, 0, 1, i+1, i+2, GTK_SHRINK, GTK_SHRINK, 0,0);
    }
    for (i = 0; i < (particles); i++)
	  for (j = 0; j < (sym_terms );j++) {
		/* entries */
		entry = gtk_entry_new();
	    text = g_malloc(max_numbers);
	    snprintf(text, max_numbers, "%d", i);
	    gtk_entry_set_text(GTK_ENTRY(entry), text);
	    g_free(text);
	    entries[i][j] = entry;
	    gtk_table_attach (GTK_TABLE(table), entry, i+1,i+2, j+1, j+2, GTK_SHRINK, GTK_SHRINK, 0,0);
	  }
    

    gtk_table_set_row_spacings (GTK_TABLE (table), 0);
    gtk_table_set_col_spacings (GTK_TABLE (table), 0);
    gtk_widget_show_all(table);
    gnome_dialog_close_hides(GNOME_DIALOG(d), YES);
    return_button_no = gnome_dialog_run_and_close(GNOME_DIALOG(d));
    switch(return_button_no) {
	case GNOME_OK:
	  for (i = 0; i < particles; i++){
		  for (j = 0; j < sym_terms; j++)
		    {
			  text = gtk_entry_get_text(GTK_ENTRY(entries[i][j]));
			  gsl_matrix_int_set(symmetry,j,i+1, atoi(text));
			  
			  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(row_signs[j])))
				{
				  gsl_matrix_int_set(symmetry,j, 0, +1);
				}
			  else
				{
				  gsl_matrix_int_set(symmetry,j, 0, -1);				  
				}
						
		    }
		  g_free(entries[i]);
	    }
	    g_free(entries);
	    gtk_widget_destroy(d);
	    break;
	case GNOME_CANCEL:
	  gtk_widget_destroy(d);
	  return;
	  break;
    }
    
}
void set_mass_and_charges(GtkWidget *widget, gpointer data) {
    GtkWidget *d, *table, *entry, *label;
    int return_button_no;
    int i;
    char * text;
    int max_numbers = 12;
    GtkWidget ** charge_entries;
    GtkWidget ** mass_entries;
    
    charge_entries = malloc ( particles * sizeof (GtkWidget *));
    mass_entries = malloc ( particles * sizeof (GtkWidget *));
      d = gnome_dialog_new(_("Change Mass and Charges"), GNOME_STOCK_BUTTON_OK, GNOME_STOCK_BUTTON_CANCEL, NULL);
    gnome_dialog_set_default(GNOME_DIALOG(d),0);
    table = gtk_table_new (particles,3, FALSE);
    gtk_container_border_width(GTK_CONTAINER(table), GNOME_PAD_SMALL);
    gtk_container_add(GTK_CONTAINER(GNOME_DIALOG(d)->vbox), table);
    
    /* creating the titles */
    label = gtk_label_new(_("Par #"));
    gtk_table_attach(GTK_TABLE(table), label, 0, 1, 0, 1, GTK_SHRINK, GTK_SHRINK, 0, 0);
    label = gtk_label_new(_("Charge"));
    gtk_table_attach(GTK_TABLE(table), label, 1, 2, 0, 1, GTK_SHRINK, GTK_SHRINK, 0, 0);
    label = gtk_label_new(_("Mass"));
    gtk_table_attach(GTK_TABLE(table), label, 2, 3, 0, 1, GTK_SHRINK, GTK_SHRINK, 0, 0);

    /* now adding the entries and setting everything up */
    for (i = 0; i < particles; i++) {
	/* particle # */
	text = g_malloc(max_numbers);
	snprintf(text, max_numbers, "%d", i);
	label = gtk_label_new(text);
	gtk_table_attach (GTK_TABLE(table), label, 0, 1, i+1, i+2, GTK_SHRINK, GTK_SHRINK, 0,0);
	g_free(text);
	/* charge */
	entry = gtk_entry_new();
	text = g_malloc(max_numbers);
	snprintf(text, max_numbers, "%f", charges[i]);
	gtk_entry_set_text(GTK_ENTRY(entry), text);
	g_free(text);
	charge_entries[i] = entry;
	gtk_table_attach (GTK_TABLE(table), entry, 1,2, i+1, i+2, GTK_SHRINK, GTK_SHRINK, 0,0);
	/* mass */
		entry = gtk_entry_new();
	text = g_malloc(max_numbers);
	snprintf(text, max_numbers, "%f", masses[i]);
	gtk_entry_set_text(GTK_ENTRY(entry), text);
	g_free(text);
	mass_entries[i] = entry;
	gtk_table_attach (GTK_TABLE(table), entry, 2,3, i+1, i+2, GTK_SHRINK, GTK_SHRINK, 0,0);
	
    }
    gtk_table_set_row_spacings (GTK_TABLE (table), 0);
    gtk_table_set_col_spacings (GTK_TABLE (table), 0);
    gtk_widget_show_all(table);
    gnome_dialog_close_hides(GNOME_DIALOG(d), YES);
    return_button_no = gnome_dialog_run_and_close(GNOME_DIALOG(d));
    switch(return_button_no) {
	case GNOME_OK:
	    for (i = 0; i < particles; i++)
		{
		    text = gtk_entry_get_text(GTK_ENTRY(charge_entries[i]));
		    charges[i] = atof(text);
		    
		    text = gtk_entry_get_text(GTK_ENTRY(mass_entries[i]));
		    masses[i] = atof(text);
		    
		    
		    
		}
	    g_free(charge_entries);
	    g_free(mass_entries);
	    gtk_widget_destroy(d);
	    break;
	case GNOME_CANCEL:
	    gtk_widget_destroy(d);
	    return;
	    break;
    }
    
}
void parameters_open_call(GtkWidget *widget, gpointer data) {
  GtkWidget *w;
  my_pl_add_string(dictionary_names[PARAMETERS_PATH], gtk_file_selection_get_filename(GTK_FILE_SELECTION(widget)));
   gtk_entry_set_text(GTK_ENTRY(parameters_entry), gtk_file_selection_get_filename(GTK_FILE_SELECTION(widget)));
}

void moiss_open_call(GtkWidget *widget, gpointer data) {
  GtkWidget *w;
  my_pl_add_string(dictionary_names[MOISS_PATH], gtk_file_selection_get_filename(GTK_FILE_SELECTION(widget)));
   gtk_entry_set_text(GTK_ENTRY(moiss_entry), gtk_file_selection_get_filename(GTK_FILE_SELECTION(widget)));
}

void output_open_call(GtkWidget *widget, gpointer data) {
  GtkWidget *w;
  my_pl_add_string(dictionary_names[OUTPUT_PATH], gtk_file_selection_get_filename(GTK_FILE_SELECTION(widget)));
  gtk_entry_set_text(GTK_ENTRY(output_entry), gtk_file_selection_get_filename(GTK_FILE_SELECTION(widget)));
}


void gmoiss_open_call(GtkWidget *widget, gpointer data ) {
   
	GtkWidget *w;
      
	switch (my_pl_open_call(calculation_dict, gtk_file_selection_get_filename(GTK_FILE_SELECTION(widget))))
		{
			case OPEN_NOT_VALID:
				w = gnome_message_box_new(_("The selected file is not a valid calculation file.\n Please select a valid file."),
							  GNOME_MESSAGE_BOX_ERROR, 
							  GNOME_STOCK_BUTTON_OK, NULL);
				GTK_WINDOW(w)->position = GTK_WIN_POS_MOUSE;
				gtk_widget_show(w);
				return;
				break;
			case OPEN_NOT_GMOISS:
				w = gnome_message_box_new(_("The file appears to be a GNUStep Properties file, but it's not in the valid format."),
							  GNOME_MESSAGE_BOX_ERROR, 
							  GNOME_STOCK_BUTTON_OK, NULL);
				GTK_WINDOW(w)->position = GTK_WIN_POS_MOUSE;
				gtk_widget_show(w); 
				return;
			case OPEN_OK:
				w = gnome_message_box_new(_("File was succesfully loaded."),
							  GNOME_MESSAGE_BOX_INFO, 
							  GNOME_STOCK_BUTTON_OK, NULL);
				GTK_WINDOW(w)->position = GTK_WIN_POS_MOUSE;
				gtk_widget_show(w);
				/*FIXME something is wrong here, OPEN doesn't work as expected */
				/*  gtk_widget_destroy(gmoiss_notebook);  */
				gmoiss_calc_new(widget, data);
				gtk_widget_show_all(gmoiss_window);
				/*  gtk_widget_destroy(widget); */
				break;
			case OPEN_NOT_CORRECT_VERSION:
				w = gnome_message_box_new(_("The file was saved in an older Gmoiss \n
Please create a new calculation file with this version."),
							  GNOME_MESSAGE_BOX_ERROR, 
							  GNOME_STOCK_BUTTON_OK, NULL);
				GTK_WINDOW(w)->position = GTK_WIN_POS_MOUSE;
				gtk_widget_show(w);
				break;
			default:
				w = gnome_message_box_new(_("Unexpected error while loading, contact your nearest service representative. 1-800-CALL-MOISS."),
							  GNOME_MESSAGE_BOX_ERROR, 
							  GNOME_STOCK_BUTTON_OK, NULL);		 
				gtk_widget_show(w);
				break;
		}
   
   
   
}






/* Temporal function, will dissapear when we have fully implemented the
   atomic coordinates code */

void create_atomic_array() {
  
	proplist_t atoms, atom, x, y, z, Z, x_key, y_key, z_key, Z_key, atoms_key;
  
	x = PLMakeString("0.");
	x_key = PLMakeString(atomic_coordinates[ATOMIC_X]);
	y = PLMakeString("0.");
	y_key = PLMakeString(atomic_coordinates[ATOMIC_Y]);
	z = PLMakeString("0.");
	z_key = PLMakeString(atomic_coordinates[ATOMIC_Z]);
	Z = PLMakeString("1");
	Z_key = PLMakeString(atomic_coordinates[ATOMIC_NUMBER]);
	atom = PLMakeDictionaryFromEntries(x_key, x, y_key, y, z_key, z, Z_key, Z, NULL);
	atoms = PLMakeArrayFromElements(atom, NULL);
	atoms_key = PLMakeString(dictionary_names[ATOMIC_COORDINATES]);
	calculation_dict = PLInsertDictionaryEntry(calculation_dict, atoms_key, atoms
						   );

  
	PLRelease(x); PLRelease(x_key); PLRelease (y); PLRelease(y_key);
	PLRelease(z); PLRelease(z_key); PLRelease (atom); PLRelease (atoms_key);
	PLRelease(atoms); PLRelease(Z); PLRelease (Z_key);
}

void parameters_file_dialog(GtkWidget *widget, gpointer data) {
  GtkWidget *fsel;
  fsel = gtk_file_selection_new(_("Parameters File location..."));
  gtk_file_selection_show_fileop_buttons(GTK_FILE_SELECTION(fsel));
  gtk_file_selection_set_filename(GTK_FILE_SELECTION(fsel), my_pl_get_string(dictionary_names[PARAMETERS_PATH]));
  gtk_signal_connect_object(GTK_OBJECT(GTK_FILE_SELECTION(fsel)->ok_button),
							"clicked", GTK_SIGNAL_FUNC(parameters_open_call),
							GTK_OBJECT(fsel));
  gtk_signal_connect_object(GTK_OBJECT(GTK_FILE_SELECTION(fsel)->cancel_button),
							"clicked", GTK_SIGNAL_FUNC(gmoiss_cancel),
							GTK_OBJECT(fsel));
  gtk_widget_show(fsel);

}

void moiss_file_dialog(GtkWidget *widget, gpointer data) {
  GtkWidget *fsel;
  fsel = gtk_file_selection_new(_("'moiss' binary file location..."));
  gtk_file_selection_show_fileop_buttons(GTK_FILE_SELECTION(fsel));
   gtk_file_selection_set_filename(GTK_FILE_SELECTION(fsel), my_pl_get_string(dictionary_names[MOISS_PATH]));
  gtk_signal_connect_object(GTK_OBJECT(GTK_FILE_SELECTION(fsel)->ok_button),
							"clicked", GTK_SIGNAL_FUNC(moiss_open_call),
							GTK_OBJECT(fsel));
  gtk_signal_connect_object(GTK_OBJECT(GTK_FILE_SELECTION(fsel)->cancel_button),
							"clicked", GTK_SIGNAL_FUNC(gmoiss_cancel),
							GTK_OBJECT(fsel));
  gtk_widget_show(fsel);

}

void output_file_dialog(GtkWidget *widget, gpointer data) {
  GtkWidget *fsel;
  fsel = gtk_file_selection_new(_("Output Files location..."));
  gtk_file_selection_show_fileop_buttons(GTK_FILE_SELECTION(fsel));
  gtk_file_selection_set_filename(GTK_FILE_SELECTION(fsel), my_pl_get_string(dictionary_names[OUTPUT_PATH]));
  gtk_signal_connect_object(GTK_OBJECT(GTK_FILE_SELECTION(fsel)->ok_button),
							"clicked", GTK_SIGNAL_FUNC(output_open_call),
							GTK_OBJECT(fsel));
  gtk_signal_connect_object(GTK_OBJECT(GTK_FILE_SELECTION(fsel)->cancel_button),
							"clicked", GTK_SIGNAL_FUNC(gmoiss_cancel),
							GTK_OBJECT(fsel));
  gtk_widget_show(fsel);

}


void gmoiss_open(void) {

	GtkWidget *fsel;

	fsel = gtk_file_selection_new(_("Opening Calculation..."));
	gtk_file_selection_hide_fileop_buttons(GTK_FILE_SELECTION(fsel));
	gtk_signal_connect_object(GTK_OBJECT(GTK_FILE_SELECTION(fsel)->ok_button),
				  "clicked", GTK_SIGNAL_FUNC(gmoiss_open_call),
				  GTK_OBJECT(fsel));
	gtk_signal_connect_object(GTK_OBJECT(GTK_FILE_SELECTION(fsel)->cancel_button),
				  "clicked", GTK_SIGNAL_FUNC(gmoiss_cancel),
				  GTK_OBJECT(fsel));
	gtk_widget_show(fsel);
}

 
void gmoiss_save(void) {
    proplist_t mc_pl, mc_pl_key;
    proplist_t charge_pl, mass_pl, symmetry_pl;
    proplist_t charge_pl_key, mass_pl_key, symmetry_pl_key;
	proplist_t prefactor_pl, prefactor_pl_key;
    char * description;
    /* setting the filename */
    calculation_dict = PLSetFilename (calculation_dict, calculation_filename);
    /* creating the key and obtaining the mc_list as a proplist */
    mc_pl_key = PLMakeString(dictionary_names[CALCULATION]);
    mc_pl = mc_list_to_pl (g_list_first(mc_list));
    /* inserting it into the main dictionary */
    PLInsertDictionaryEntry(calculation_dict, mc_pl_key, mc_pl);
    /* obtaining a proplist array for the charges and masses */
    charge_pl_key = PLMakeString(dictionary_names[PARTICLE_CHARGES]);
    mass_pl_key = PLMakeString(dictionary_names[PARTICLE_MASSES]);
	symmetry_pl_key = PLMakeString(dictionary_names[SYM_MATRIX]);
	prefactor_pl_key = PLMakeString(dictionary_names[CE_PREFACTORS]);
    mass_pl = double_array_to_pl (particles, masses);
    charge_pl = double_array_to_pl (particles, charges);
	symmetry_pl = int_matrix_to_pl (symmetry);
	prefactor_pl = float_matrix_to_pl (prefactors);
    PLInsertDictionaryEntry(calculation_dict, mass_pl_key, mass_pl);
    PLInsertDictionaryEntry(calculation_dict, charge_pl_key, charge_pl);
    PLInsertDictionaryEntry(calculation_dict, symmetry_pl_key, symmetry_pl);
	PLInsertDictionaryEntry(calculation_dict, prefactor_pl_key, prefactor_pl);
	
    /* and saving... */
    if (PLSave(calculation_dict, YES)) {
	  g_message("File Saved succesfully!");
	  saved = 1;
	}
    else {
	  g_warning("I could not save");
	  saved = 0;
	}
    description = PLGetDescriptionIndent(calculation_dict, 3);
    printf("File Saved:\n%s\n", description);
    free(description);
    PLRelease(mc_pl_key);
    PLRelease(charge_pl_key);
    PLRelease(mass_pl_key);
}

void gmoiss_save_call (GtkWidget *widget, gpointer data)
{
	PLRelease(calculation_filename);
	calculation_filename = PLMakeString(gtk_file_selection_get_filename(GTK_FILE_SELECTION(widget)));
	gtk_widget_destroy(widget);
	gmoiss_save();
}


int compare_stats_equal(Stat *stat1, Stat *stat2) {
	return (stat1->type == stat2->type);
}

int compare_stats(gconstpointer p1, gconstpointer p2) {
	Stat *stat1, *stat2;
	stat1 = (Stat*) p1;
	stat2 = (Stat*) p2;
	return (stat1->type - stat2->type);
}

void gmoiss_remove_stats_from_tree(GtkWidget *tree, Stat *stats)
{

	MonteCarlo *mc;
	GList *node, *startnode;
	Stat *tree_stats = NULL, *new_stats;
	/* first find it in our internal data structure ,
	   then in the real tree */
	mc = gtk_ctree_node_get_row_data(GTK_CTREE(gmoiss_tree), selected_node);
	startnode = mc->stats;

	for (node = startnode; node; node = node->next)
		{

			tree_stats = node->data;
			if (compare_stats_equal(tree_stats, stats))
				break;
		}

	g_assert (node != NULL); 
	if (!node)
		{
			/* not found */
			printf("Strange error, stats should be found!");
		}
	else
		{
			GtkCTreeNode *node_to_delete;
			/* found */

			/* do the g_new stuff tomorrow &stats should be
			   a new_stats pointer! FIXME */
			new_stats = g_new (Stat, 1);
			new_stats =  stats;

			/* now removing the stats from the tree itself! */
			node_to_delete = gtk_ctree_find_by_row_data_custom( 
									   GTK_CTREE(gmoiss_tree), selected_node, new_stats, 
									   compare_stats); 
			gtk_ctree_remove_node(GTK_CTREE(gmoiss_tree), node_to_delete);

			mc->stats = g_list_remove(mc->stats, node->data);

			if (tree_stats) 
				g_free (tree_stats);
		}
}

void gmoiss_init_pixes(void)
{
	mc_pix = pix_new(mc_xpm);
	stat_pix = pix_new(stat_xpm);
}

void gmoiss_start_calculation(GtkWidget *widget, gpointer data) {
  int i;
  char **argv;
  int argc;
  GtkWidget *w;
  if (saved == 1) {
	g_message("Changing directory to %s", my_pl_get_string(dictionary_names[OUTPUT_PATH]));
	if (chdir(my_pl_get_string(dictionary_names[OUTPUT_PATH])) != 0) {
	  w = gnome_message_box_new(_("There was an error changing directory! \n Please check the \"Output Path \" parameter in \"File Setup\" "),
								GNOME_MESSAGE_BOX_ERROR, 
								GNOME_STOCK_BUTTON_OK, NULL);
	  GTK_WINDOW(w)->position = GTK_WIN_POS_MOUSE;
	  gtk_widget_show(w);
	} //end if saved
	else {
	  g_message("Creating directory %s", my_pl_get_string( dictionary_names[JOB_NAME]));
	  if (mkdir(my_pl_get_string(dictionary_names[JOB_NAME]), 0000) != 0) {
		w = gnome_message_box_new(_("There was an error creating the directory for the jobname. \n Try changing the jobname for a different one. "),
								  GNOME_MESSAGE_BOX_ERROR, 
								  GNOME_STOCK_BUTTON_OK, NULL);
		GTK_WINDOW(w)->position = GTK_WIN_POS_MOUSE;
		gtk_widget_show(w);
	  } //end if mkdir
	  else {
		/* changing the permissions */
		chmod(my_pl_get_string(dictionary_names[JOB_NAME]), S_ISUID | S_ISGID
			  | S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP | S_IXGRP);
		/* we are *ALMOST* sure that this chdir will work */
		chdir(my_pl_get_string(dictionary_names[JOB_NAME]));

		argc = 3; /* numero de argumentos (incluyendo ejecutable) */
		
		argv = g_new(char *, argc);
		//argv[0] = strdup("nxterm");
		//argv[1] = strdup("-e");
		argv[0] = strdup(my_pl_get_string(dictionary_names[MOISS_PATH]));
		argv[1] = strdup("-g");
		argv[2] = strdup(PLGetString(calculation_filename));
		argv[3] = NULL;
		
		g_message("Running moiss with input file %s", argv[2]);
		//if (gnome_execute_async(NULL, argc, argv) == -1)
		//  gnome_app_error(GNOME_APP(gmoiss_window), 
		//_("Could not fork for executing moiss. Please report the bug to the authors"));
		
		gmoiss_moiss_in_notebook(argc, argv);
		
		/* FIXME, so... what are 40 bytes in memory if the program crashes?
		   anyway*/
		   //for (i = 0; i < argc; i++)
		   //  g_free(argv[i]);
	  } //end else if mkdir
  } //end if chdir
  } //end if saved
  else { /* the thing is not saved */
	w = gnome_message_box_new(_("You haven't saved any calculation file, please do so before trying to start a calculation"),
							  GNOME_MESSAGE_BOX_ERROR,
							  GNOME_STOCK_BUTTON_OK,
							  NULL);
	GTK_WINDOW(w)->position = GTK_WIN_POS_MOUSE;
	gtk_widget_show(w);
  } //end else saved 
} //end function



void gmoiss_quit(GtkWidget *widget, gpointer data)
{
	gtk_widget_destroy(gmoiss_window);
	gtk_main_quit();
}

void gmoiss_save_as(GtkWidget *widget, gpointer data)
{
	GtkWidget *fsel;

	fsel = gtk_file_selection_new(_("Save Calculation as..."));
	gtk_file_selection_hide_fileop_buttons(GTK_FILE_SELECTION(fsel));
	gtk_signal_connect_object(GTK_OBJECT(GTK_FILE_SELECTION(fsel)->ok_button),
				  "clicked", GTK_SIGNAL_FUNC(gmoiss_save_call),
				  GTK_OBJECT(fsel));
	gtk_signal_connect_object(GTK_OBJECT(GTK_FILE_SELECTION(fsel)->cancel_button),
				  "clicked", GTK_SIGNAL_FUNC(gmoiss_cancel),
				  GTK_OBJECT(fsel));
	gtk_widget_show(fsel);
}

static void
about_box_destroy_cb (void)
{
     about_box_visible = FALSE;
 }

void gmoiss_about(GtkWidget *widget, gpointer data)
{
	GtkWidget *about;
	const  gchar *authors[] = {
		"Al�n Aspuru Guzik (aspuru@eros.pquim.unam.mx)",
		"Ra�l Perusqu�a Flores (rulox@eros.pquim.unam.mx)",
		"Francisco Bustamante Hempe (pancho@nuclecu.unam.mx) ",
		"Carlos Amador Bedolla (amador@eros.pquim.unam.mx)",
	        "Dario Bressanini      (dario@fis.unico.it)",
		NULL
	};
	gchar * logo;
	
	if (about_box_visible)
	    return;
	else
	    about_box_visible = TRUE;
	
	logo=gnome_unconditional_pixmap_file("images/logo.gif");
	
	
	about = gnome_about_new (_("Monte Carlo Integration and Sampling Software"), GMOISS_VERSION_NUMBER,
				 "Copyright (C) 1997-1999. Under GNU/GPL License", authors,
				 _("Quantum Chemistry package"),
				 logo);
	gtk_signal_connect (GTK_OBJECT (about),
			                           "destroy",
			    (GtkSignalFunc) about_box_destroy_cb,
			    NULL);
	
	gtk_widget_show (about);
	
	g_free(logo);
}

GtkWidget*  my_new_hbox () {
	GtkWidget *box;
	box = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_container_border_width(GTK_CONTAINER(box), GNOME_PAD_SMALL);
	return box;
}


GtkEntry* my_new_float_entry ( char * label_name,
			       GtkWidget *box,
			       GtkSignalFunc signal_func,
			       float default_value,
			       int max_numbers){

	GtkWidget *label, *entry;
	gchar *text;

	label = gtk_label_new(label_name);
	gtk_box_pack_start(GTK_BOX(box), label, FALSE, FALSE, 0);
	entry = gtk_entry_new();
	text = g_malloc(max_numbers);
	snprintf(text, max_numbers, "%f", default_value);
	gtk_entry_set_text(GTK_ENTRY(entry), text);
	g_free (text);
	gtk_box_pack_start(GTK_BOX(box), entry, FALSE, FALSE, 0);

	gtk_signal_connect (GTK_OBJECT(entry), "activate",
			    GTK_SIGNAL_FUNC(signal_func),
			    entry);

	return GTK_ENTRY(entry);
}

GtkEntry* my_new_int_entry ( char * label_name,
			     GtkWidget *box,
			     GtkSignalFunc signal_func,
			     int default_value,
			     int max_numbers){

	GtkWidget *label, *entry;
	gchar *text;

	label = gtk_label_new(label_name);
	gtk_box_pack_start(GTK_BOX(box), label, FALSE, FALSE, 0);
	entry = gtk_entry_new();
	text = g_malloc(max_numbers);
	snprintf(text, max_numbers, "%i", default_value);
	gtk_entry_set_text(GTK_ENTRY(entry), text);
	g_free (text);
	gtk_box_pack_start(GTK_BOX(box), entry, FALSE, FALSE, 0);

	gtk_signal_connect (GTK_OBJECT(entry), "activate",
			    GTK_SIGNAL_FUNC(signal_func),
			    entry);

	return GTK_ENTRY(entry);
}



GtkSpinButton *  my_new_float_spinbutton (char * label_name,
					  GtkWidget *box,
					  GtkSignalFunc signal_func,
					  int spinner_size,
					  float min_value,
					  float max_value,
					  float default_value,
					  int num_digits,
					  float step_value ) {
	GtkWidget *label, *spin_button;
	GtkAdjustment *adj;

	label = gtk_label_new(label_name);
	gtk_box_pack_start(GTK_BOX(box), label, FALSE, TRUE, GNOME_PAD_SMALL);

	adj =(GtkAdjustment *)  gtk_adjustment_new (default_value,
						    min_value, max_value,
						    step_value, 5.0, 0.0);
	spin_button = gtk_spin_button_new(adj, 0, 0);

	gtk_spin_button_set_wrap (GTK_SPIN_BUTTON(spin_button), TRUE);
	gtk_spin_button_set_shadow_type( GTK_SPIN_BUTTON (spin_button),
					 GTK_SHADOW_OUT);
	gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(spin_button), TRUE);
	gtk_widget_set_usize(spin_button, spinner_size, 0);
	gtk_spin_button_set_digits (GTK_SPIN_BUTTON (spin_button), num_digits);

	gtk_box_pack_start (GTK_BOX(box), spin_button, FALSE, TRUE, GNOME_PAD_SMALL);

	gtk_signal_connect (GTK_OBJECT(adj), "value_changed",
			    GTK_SIGNAL_FUNC(signal_func),
			    spin_button);
	return GTK_SPIN_BUTTON(spin_button);
}


GtkSpinButton * my_new_int_spinbutton (char * label_name, GtkWidget *box,
				       GtkSignalFunc signal_func,
				       int spinner_size,
				       int min_value,
				       int max_value,
				       int default_value ) {
	GtkWidget *label, *spin_button;
	GtkAdjustment *adj;

	label = gtk_label_new(label_name);
	gtk_box_pack_start(GTK_BOX(box), label, FALSE, TRUE, GNOME_PAD_SMALL);

	adj =(GtkAdjustment *)  gtk_adjustment_new (default_value,
						    min_value, max_value,
						    1.0, 5.0, 0.0);
	spin_button = gtk_spin_button_new(adj, 0, 0);

	gtk_spin_button_set_wrap (GTK_SPIN_BUTTON(spin_button), TRUE);
	gtk_spin_button_set_shadow_type( GTK_SPIN_BUTTON (spin_button),
					 GTK_SHADOW_OUT);
	gtk_spin_button_set_numeric(GTK_SPIN_BUTTON(spin_button), TRUE);
	gtk_widget_set_usize(spin_button, spinner_size, 0);

	gtk_box_pack_start (GTK_BOX(box), spin_button, FALSE, TRUE, GNOME_PAD_SMALL);

	gtk_signal_connect (GTK_OBJECT(adj), "value_changed",
			    GTK_SIGNAL_FUNC(signal_func),
			    spin_button);

	return GTK_SPIN_BUTTON(spin_button);
}


void my_new_greek_choice ( GtkWidget *box , char * names[], 
			   GtkSignalFunc signal_func , int default_value ) 
{ 
	GtkWidget *option, *menu, *label; 
	int i; 


	option = gtk_option_menu_new();
	menu = gtk_menu_new (); 
	for (i = 0; *names[i]; i++)
		{ 
			GtkWidget *item; 

			item = gtk_menu_item_new_with_label (_(names[i])); 
			gtk_widget_set_style(item, greek_style);

			gtk_signal_connect (GTK_OBJECT(item), "activate", 
					    GTK_SIGNAL_FUNC(signal_func), 
					    GINT_TO_POINTER (i)); 

			gtk_menu_append (GTK_MENU (menu), item); 
			gtk_widget_show (item);}
			

	gtk_option_menu_set_menu(GTK_OPTION_MENU(option), menu); 
	gtk_option_menu_set_history(GTK_OPTION_MENU(option),  
				    default_value); 
	gtk_box_pack_start(GTK_BOX(box), option, FALSE, FALSE, 0); 
	gtk_widget_show(option); 
}

void my_new_choice ( char * label_name, GtkWidget *box , char * names[], 
		     GtkSignalFunc signal_func , int default_value ) 
{ 
	GtkWidget *option, *menu, *label; 
	int i; 

	label = gtk_label_new(label_name);
	gtk_box_pack_start(GTK_BOX(box), label, FALSE, FALSE, 0);

	option = gtk_option_menu_new();
	menu = gtk_menu_new (); 
	for (i = 0; *names[i]; i++)
		{ 
			GtkWidget *item; 

			item = gtk_menu_item_new_with_label (_(names[i])); 


			gtk_signal_connect (GTK_OBJECT(item), "activate", 
					    GTK_SIGNAL_FUNC(signal_func), 
					    GINT_TO_POINTER (i)); 

			gtk_menu_append (GTK_MENU (menu), item); 
			gtk_widget_show (item); 
		}

	gtk_option_menu_set_menu(GTK_OPTION_MENU(option), menu); 
	gtk_option_menu_set_history(GTK_OPTION_MENU(option),  
				    default_value); 
	gtk_box_pack_start(GTK_BOX(box), option, FALSE, FALSE, 0); 
	gtk_widget_show(option); 
}

void change_atomic_x (GtkWidget *widget, GtkWidget *entry) {
}
void change_atomic_y (GtkWidget *widget, GtkWidget *entry) {
}
void change_atomic_z (GtkWidget *widget, GtkWidget *entry) {
}
void change_atomic_Z (GtkWidget *widget, GtkWidget *entry) {
}

void change_dt (GtkWidget *widget, GtkWidget *entry)
{
	gchar *entry_text;


	entry_text = gtk_entry_get_text(GTK_ENTRY(entry));
	my_pl_add_float (dictionary_names[TIME_STEP], atof(entry_text));

	 
}


void change_gaussian_spread_range (GtkWidget *widget, GtkWidget *entry)
{
	gchar *entry_text;

	 
	entry_text = gtk_entry_get_text(GTK_ENTRY(entry));
	
	my_pl_add_float (dictionary_names[GAUSSIAN_SPREAD_RANGE], atof(entry_text));

}


void change_gaussian_tf_alpha (GtkWidget *widget, GtkWidget *entry)
{
	gchar *entry_text;


	entry_text = gtk_entry_get_text(GTK_ENTRY(entry));
	my_pl_add_float (dictionary_names[GAUSSIAN_TRIAL_FUNCTION_ALPHA], atof(entry_text));
}
void change_random_seed( GtkWidget *widget,GtkWidget *entry)
{

	gchar *entry_text;

	entry_text = gtk_entry_get_text(GTK_ENTRY(entry));
	my_pl_add_int(dictionary_names[ RANDOM_NUMBER_GENERATOR_SEED], atoi(entry_text));
};
void change_spin_parity( GtkWidget *widget,GtkWidget *entry)
{

	gchar *entry_text;

	entry_text = gtk_entry_get_text(GTK_ENTRY(entry));
	my_pl_add_int(dictionary_names[SPIN_PARITY_OPERATOR], atoi(entry_text));
};
void change_diffusion_constant (GtkWidget *widget, GtkWidget *entry)
{
	gchar *entry_text;


	entry_text = gtk_entry_get_text(GTK_ENTRY(entry));
	my_pl_add_float (dictionary_names[DIFFUSION_CONSTANT], atof(entry_text));
}
void change_min_histogram (GtkWidget *widget, GtkWidget *entry)
{
	gchar *entry_text;


	entry_text = gtk_entry_get_text(GTK_ENTRY(entry));
	my_pl_add_float (dictionary_names[GRAPH_MINIMUM], atof(entry_text));
}
void change_max_histogram (GtkWidget *widget, GtkWidget *entry)
{
	gchar *entry_text;


	entry_text = gtk_entry_get_text(GTK_ENTRY(entry));
	my_pl_add_float (dictionary_names[GRAPH_MAXIMUM], atof(entry_text));
}

void change_lj_sigma (GtkWidget *widget, GtkWidget *entry)
{
	gchar *entry_text;


	entry_text = gtk_entry_get_text(GTK_ENTRY(entry));
	my_pl_add_float (dictionary_names[LEHNARD_JONES_SIGMA], atof(entry_text));
}

void change_lj_epsilon (GtkWidget *widget, GtkWidget *entry)
{
	gchar *entry_text;


	entry_text = gtk_entry_get_text(GTK_ENTRY(entry));
	my_pl_add_float (dictionary_names[LEHNARD_JONES_EPSILON], atof(entry_text));
}
void change_electronic_repulsion_factor (GtkWidget *widget, GtkWidget *entry)
{
	gchar *entry_text;
	
	
	entry_text = gtk_entry_get_text(GTK_ENTRY(entry));
	my_pl_add_float(dictionary_names[ELECTRONIC_REPULSION], atof(entry_text));
}
void change_dt_convergence_factor (GtkWidget *widget, GtkWidget *entry)
{
	gchar *entry_text;
	 
  
	entry_text = gtk_entry_get_text(GTK_ENTRY(entry));
	my_pl_add_float(dictionary_names[DT_CONVERGENCE_FACTOR], atof(entry_text));
}

void change_sym_number_of_terms(GtkWidget *widget, GtkSpinButton *spinner)
{
	
	my_pl_add_int (dictionary_names[SYM_TERMS], gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner)));
	sym_terms = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner));

}

void change_ce_number_of_prefactor(GtkWidget *widget, GtkSpinButton *spinner)
{
	
	my_pl_add_int (dictionary_names[CE_PREFACTOR_TERMS], gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner)));
	prefactor_terms = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner));

}
void change_ce_number_of_e_n_params(GtkWidget *widget, GtkSpinButton *spinner)
{
	
	my_pl_add_int (dictionary_names[ELEC_NUCLEI_PARAMS], gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner)));

}
void change_ce_number_of_terms(GtkWidget *widget, GtkSpinButton *spinner)
{
	
	my_pl_add_int (dictionary_names[CE_TERMS], gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner)));

}

void change_walkers (GtkWidget *widget, GtkSpinButton *spinner)
{
	
	my_pl_add_int (dictionary_names[WALKERS], gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner)));

}

void change_max_branches (GtkWidget *widget, GtkSpinButton *spinner)
{
	my_pl_add_int(dictionary_names[MAXIMUM_BRANCHES],
		      gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner)));
}

void change_divs_per_dimension (GtkWidget *widget, GtkSpinButton *spinner)
{
	my_pl_add_int(dictionary_names[DIVS_PER_DIMENSION],
		      gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner)));
}
	

void change_potential_type (GtkWidget *widget, gpointer data)
{
   
	my_pl_add_string (dictionary_names[POTENTIAL], potential_names[GPOINTER_TO_INT(data)]);
	if (find_names_index_plus(potential_names, dictionary_names[TUNNEL_POTENTIAL]) == GPOINTER_TO_INT(data)) {
		/* haz tu caca */
		/* ver create_pages (5,6) gtk_widget_hide*/
		/* hacerle gtk_widget_show y listo paparrin */
		printf("Tunnel potential chosen!\n");
	}
}

void change_dimensions (GtkWidget *widget, gpointer data)
{
	 

	my_pl_add_string (dictionary_names[DIMENSIONS], dimension_names[GPOINTER_TO_INT(data)]);
}

void change_electron_tf (GtkWidget *widget, gpointer data)
{
	my_pl_add_string (dictionary_names[ELEC_ELEC_TF], boolean_names[GPOINTER_TO_INT(data)]);
}

void change_nuclei_tf (GtkWidget *widget, gpointer data)
{
	my_pl_add_string (dictionary_names[NUCLEI_ELEC_TF], boolean_names[GPOINTER_TO_INT(data)]);
}

void change_trial_function_type (GtkWidget *widget, gpointer data)
{
	my_pl_add_string (dictionary_names[TRIAL_FUNCTION], trial_function_names[GPOINTER_TO_INT(data)]);
	/* if (problem_data.trial_function_type == SLATER_TYPE_ORBITALS) {

	   }*/
}

void change_initial_position (GtkWidget *widget, gpointer data)
{

	my_pl_add_string (dictionary_names[INITIAL_POSITION], initial_position_names[GPOINTER_TO_INT(data)]);
}



void change_particles ( GtkWidget *widget, GtkSpinButton *spinner)
{
  
  int i;
  
  my_pl_add_int (dictionary_names[PARTICLES], gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner)));
    particles = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(spinner));
    g_free(charges);
    g_free(masses);
    charges = g_new(double, particles);
    masses = g_new(double, particles);
	/* setting the default values */
    for (i = 0; i < particles; i ++)
	  {
	    charges[i] = -1.0;
	    masses[i] = 1.0;
	}
	/* now we change the number of symmetry terms accordingly */
	
	  switch (particles) {
	  case 2:
		sym_terms = 2;
		break;
	  case 3:
		sym_terms = 4;
		break;
	  case 4:
		sym_terms = 16;
		break;
		
	}
	gtk_spin_button_set_value(terms_spinner, sym_terms);
}


void gmoiss_tree_selected(GtkCTree *tree, GtkCTreeNode *row, gint column)
{
	gchar *text[4];
	/* We always start with a root selected node, because we are in
	   browse mode */
	GtkCTreeNode *work; 
	GList *selection; 

	text[0] = malloc(11);

	selection = GTK_CLIST (gmoiss_tree)-> selection;

	gtk_clist_freeze(GTK_CLIST(gmoiss_tree));


	work = selection->data; 


	if(GTK_CTREE_ROW(work)->is_leaf){
		/* this is a leaf node */
		/* the "selected node must be the root of this thing */
		gtk_ctree_select(GTK_CTREE(gmoiss_tree), selected_node);
		printf("Selected CHILD node");

	}
	else {
		selected_node = row;

	}
	gtk_clist_thaw(GTK_CLIST(gmoiss_tree));

	gtk_ctree_get_node_info (GTK_CTREE(gmoiss_tree), selected_node,
				 text, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
/* 	gtk_menu_set_active (GTK_MENU(t_level.menu), get_menu_index(text[0])); */
	gtk_option_menu_set_history (GTK_OPTION_MENU(t_level.opt_menu), get_menu_index(text[0]));

	gtk_ctree_node_get_text (GTK_CTREE(gmoiss_tree), selected_node, 1, text);
	gtk_entry_set_text(t_level.block_entry, text[0]);
	gtk_ctree_node_get_text (GTK_CTREE(gmoiss_tree), selected_node, 2, text);
	gtk_entry_set_text(GTK_ENTRY(t_level.iter_entry), text[0]);

	t_level_set_toggles (gmoiss_tree, selected_node);
}


void change_mc_type (GtkWidget *widget, gpointer data)
{
	MonteCarlo *mc;
	char *text[4];
	int i;


	mc = gtk_ctree_node_get_row_data (GTK_CTREE(gmoiss_tree), selected_node);

	mc->type = GPOINTER_TO_INT (data);

	text[0] = _(mc_names[mc->type - MC_VARIATIONAL]);
	text[1] = g_malloc(11);
	text[2] = g_malloc(11);
	text[3] = g_malloc(11);


	snprintf(text[1], 11 , "%d", mc-> blocks);
	snprintf(text[2], 11 , "%d", mc-> iterations);
	snprintf(text[3], 11 , "%d", mc-> iterations * mc->blocks);
	/* FIXME PIXMAP STUFF 
	   gtk_ctree_set_node_info(GTK_CTREE(gmoiss_tree), selected_node,
	   text[0], TREE_SPACING, 
	   stat_pix->pixmap, stat_pix->mask, 
	   stat_pix->pixmap, stat_pix->mask, FALSE, FALSE);*/

	gtk_ctree_set_node_info(GTK_CTREE(gmoiss_tree), selected_node,
				text[0], TREE_SPACING, 
			        NULL, NULL, 
				NULL, NULL, FALSE, FALSE);
	for (i= 1; i < 4; i++)
		gtk_ctree_node_set_text(GTK_CTREE(gmoiss_tree), selected_node,
					i, text[i]);
	g_free(text[1]);
	g_free(text[2]);
	g_free(text[3]);
	
	
}

void change_mc_iterations(GtkWidget * widget, GtkWidget * entry)
{
	MonteCarlo *mc;
	char *text[4];

	gchar *entry_text;
	int i;

	entry_text = gtk_entry_get_text(GTK_ENTRY(entry));


	mc = gtk_ctree_node_get_row_data (GTK_CTREE(gmoiss_tree), selected_node);
	mc->iterations = atoi(entry_text);


	text[0] = _(mc_names[mc->type  - MC_VARIATIONAL]);
	text[1] = g_malloc(11);
	text[2] = g_malloc(11);
	text[3] = g_malloc(11);

	snprintf(text[1], 11 , "%d", mc->blocks);
	snprintf(text[2], 11, "%d", mc->iterations);
	snprintf(text[3], 11, "%d", mc->iterations * mc->blocks);
	/* FIXME
	   gtk_ctree_set_node_info(GTK_CTREE(gmoiss_tree), selected_node,
	   text[0], TREE_SPACING, 
	   stat_pix->pixmap, stat_pix->mask, 
	   stat_pix->pixmap, stat_pix->mask, FALSE, FALSE);*/
	gtk_ctree_set_node_info(GTK_CTREE(gmoiss_tree), selected_node,
				text[0], TREE_SPACING, 
				NULL, NULL, 
				NULL, NULL, FALSE, FALSE);
	for (i = 1; i < 4; i++)
		gtk_ctree_node_set_text(GTK_CTREE(gmoiss_tree), selected_node,
					i, text[i]);


}

void change_parameters_path (GtkWidget * widget, GtkWidget * entry) {
  my_pl_add_string (dictionary_names[PARAMETERS_PATH], gtk_entry_get_text(GTK_ENTRY(entry)));
}

void change_moiss_path (GtkWidget * widget, GtkWidget * entry) {
  my_pl_add_string (dictionary_names[MOISS_PATH], gtk_entry_get_text(GTK_ENTRY(entry)));
}

void change_output_path (GtkWidget * widget, GtkWidget * entry) {
  my_pl_add_string (dictionary_names[OUTPUT_PATH], gtk_entry_get_text(GTK_ENTRY(entry)));
}

void change_job_path (GtkWidget * widget, GtkWidget * entry) {
  my_pl_add_string (dictionary_names[JOB_NAME], gtk_entry_get_text(GTK_ENTRY(entry)));
}


void change_mc_blocks(GtkWidget * widget, GtkWidget * entry)
{
	MonteCarlo *mc;
	char *text[4];

	gchar *entry_text;
	int i;

	entry_text = gtk_entry_get_text(GTK_ENTRY(entry));


	mc = gtk_ctree_node_get_row_data (GTK_CTREE(gmoiss_tree), selected_node);
	mc->blocks = atoi(entry_text);


	text[0] = _(mc_names[mc->type  - MC_VARIATIONAL]);
	text[1] = g_malloc(11);
	text[2] = g_malloc(11);
	text[3] = g_malloc(11);

	snprintf(text[1], 11 , "%d", mc->blocks);
	snprintf(text[2], 11, "%d", mc->iterations);
	snprintf(text[3], 11, "%d", mc->iterations * mc->blocks);

	/* FIXME: Pixmap stuff
	   gtk_ctree_set_node_info(GTK_CTREE(gmoiss_tree), selected_node,
	   text[0], TREE_SPACING, 
	   stat_pix->pixmap, stat_pix->mask, 
	   stat_pix->pixmap, stat_pix->mask, FALSE, FALSE);*/

	gtk_ctree_set_node_info(GTK_CTREE(gmoiss_tree), selected_node,
				text[0], TREE_SPACING, 
				NULL, NULL, 
				NULL, NULL, FALSE, FALSE);
	for (i = 1; i < 4; i++)
		gtk_ctree_node_set_text(GTK_CTREE(gmoiss_tree), selected_node,
					i, text[i]);


}

void sym_elec_toggled(GtkWidget *widget, gpointer data) {
  if (GTK_TOGGLE_BUTTON(widget)->active)
	{
	  my_pl_add_string(dictionary_names[SYM_ELEC_ELEC], boolean_names[ON]);
	}
  else 
	{
	  my_pl_add_string(dictionary_names[SYM_ELEC_ELEC], boolean_names[OFF]);
	}
}

void sym_nuclei_toggled(GtkWidget *widget, gpointer data) {
   if (GTK_TOGGLE_BUTTON(widget)->active)
	{
	  my_pl_add_string(dictionary_names[SYM_ELEC_NUCLEI], boolean_names[ON]);
	}
  else 
	{
	  my_pl_add_string(dictionary_names[SYM_ELEC_NUCLEI], boolean_names[NO]);
	}
}

void stats_toggled (GtkWidget *widget, gpointer data) {
	Stat *stats;

	stats = g_new(Stat, 1);
	stats->type = GPOINTER_TO_INT (data);

	if (GTK_TOGGLE_BUTTON (widget)->active) 
		{
			/* If control reaches here, the toggle button is down, so add a child */
			if (lock)
				gmoiss_add_stats_to_tree(gmoiss_tree, stats);

		} else {

			/* If control reaches here, the toggle button is up,
			   so delete the child if
			   it exists */
			if (lock)
				gmoiss_remove_stats_from_tree(gmoiss_tree, stats);
		}
}

/*
   {
   GList *nodo;
   Stat *stats;

   for (nodo = mi_lista; nodo; nodo = nodo->next) {
   stats = nodo->data;
   if (stats->type == loquequiero)
   break;
   }

   if (!nodo)
   no lo encontre;
   else
   {
   }

   }
 */



int gmoiss_add_mc_to_tree(GtkWidget *tree, MonteCarlo *mc)
{
    
    
    char *text[4];
    GtkCTreeNode *mc_tree;
    GdkColor col1;
   
    
    text[0] = _(mc_names[mc->type  - MC_VARIATIONAL]);
    text[1] = g_malloc(11);
    text[2] = g_malloc(11);
    text[3] = g_malloc(11);
    
    snprintf(text[1], 11, "%d", mc->blocks);
    snprintf(text[2], 11, "%d", mc->iterations);
    snprintf(text[3], 11, "%d", mc->iterations * mc->blocks);
    
    
    col1.red = 50000;
    col1.green = 20000;
    col1.blue = 0;
    
    /*mc_tree = gtk_ctree_insert_node(GTK_CTREE(gmoiss_tree), 
      NULL, NULL, text, TREE_SPACING, 
      stat_pix->pixmap, stat_pix->mask, 
      stat_pix->pixmap, stat_pix->mask, FALSE,
				    TRUE);*/
    /* FIXME: Pixmap problem */
    mc_tree = gtk_ctree_insert_node(GTK_CTREE(gmoiss_tree), NULL, NULL,
				    text, TREE_SPACING,
				    NULL, NULL,
				    NULL, NULL, FALSE, TRUE);
    /* this will do the job while we fix it */
    
    gtk_ctree_node_set_background (GTK_CTREE(gmoiss_tree), mc_tree, &col1);
    
    gtk_ctree_node_set_row_data(GTK_CTREE(gmoiss_tree), mc_tree, mc);
    
    
    
	/* selecting for default */
    gtk_ctree_select(GTK_CTREE(gmoiss_tree), mc_tree);
    g_free(text[1]);
    g_free(text[2]);
    g_free(text[3]);
   
}

void gmoiss_add_stats_to_tree(GtkWidget *tree, Stat *stats)
{
    /* stats must come from stats->type which comes from the enum StatType
       STAT_BASIC=0, STAT_EXTENDED = 1 (see .h), and the
       char * stat_names[i] for the names of the same things...
    */
    MonteCarlo *mc;
    char *text[4];
    GtkCTreeNode *stats_tree;
    Stat *new_stats;
    GtkStyle *parent_style;
    
    text[0] = _(stat_names[stats->type]);
    text[1] = NULL;
    text[2] = NULL;
    text[3] = NULL;
    
    new_stats = g_new (Stat, 1); 
    new_stats = stats;
    /* FIXME: Pixmap problem
       stats_tree = gtk_ctree_insert_node(GTK_CTREE(gmoiss_tree), 
       selected_node, NULL, text, TREE_SPACING, 
       stat_pix->pixmap, stat_pix->mask, 
       stat_pix->pixmap, stat_pix->mask, TRUE, TRUE);
    */
    stats_tree = gtk_ctree_insert_node(GTK_CTREE(gmoiss_tree),
				       selected_node, NULL, text, TREE_SPACING,
				       NULL, NULL,
				       NULL, NULL, TRUE, TRUE);
    parent_style = gtk_ctree_node_get_row_style (GTK_CTREE(gmoiss_tree), selected_node);
    
    gtk_ctree_node_set_row_style (GTK_CTREE(gmoiss_tree), stats_tree, parent_style);
    
    /* Adding the personal data */
    gtk_ctree_node_set_row_data(GTK_CTREE(gmoiss_tree), stats_tree, new_stats);
 
    mc = gtk_ctree_node_get_row_data(GTK_CTREE(gmoiss_tree), selected_node);
    mc->stats = g_list_append (mc->stats, stats);
    
    
    gtk_ctree_expand(GTK_CTREE(gmoiss_tree), selected_node);
    
 /* g_free(text); */
    
}

static void
create_tree (void)
{
	GtkWidget *button;
	MonteCarlo *mc;
	int x = 0, y = 0;


	mc = g_malloc(sizeof(MonteCarlo));


	mc->type = def_mc.type;
	mc->blocks = def_mc.blocks;
	mc->iterations = def_mc.iterations;
	mc->stats = NULL;

	/* borrar la mc_list antigua */

	mc_list = g_list_append(mc_list, mc);

	if (!gmoiss_tree) {
		GtkWidget *hbox, *vbox, *hbox2;
		GtkWidget *option, *menu, *entry;
		GtkWidget *frame, *align;
		GtkWidget *label;
		GtkWidget *table;
		GtkWidget *scrollwin;
		char *text;
		char *titles[] = { N_("Theory Level"), N_("Blocks"),
				   N_("Iterations/Block"),
				   N_("Iterations")};
		int i;

		gtk_widget_push_visual (gdk_imlib_get_visual ());
		gtk_widget_push_colormap (gdk_imlib_get_colormap ());

		scrollwin = gtk_scrolled_window_new (NULL, NULL);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrollwin),
						GTK_POLICY_NEVER,
						GTK_POLICY_AUTOMATIC);
		gtk_box_pack_start(GTK_BOX(gmoiss_tree_vbox), scrollwin, 
				   TRUE, TRUE, 0);
		
		gmoiss_tree = gtk_ctree_new_with_titles(4, 0, titles);
		gtk_container_add (GTK_CONTAINER(scrollwin), gmoiss_tree);
		gtk_widget_pop_colormap ();
		gtk_widget_pop_visual ();
		gtk_clist_set_column_width(GTK_CLIST(gmoiss_tree), 0, COL_SPACING);
		for (i=1; i<4; i++)
			gtk_clist_set_column_width(GTK_CLIST(gmoiss_tree),
						   i, COL_SMALLSPACING);

		gtk_ctree_set_line_style (GTK_CTREE(gmoiss_tree), 
					  GTK_CTREE_LINES_TABBED);
/*  		gtk_ctree_set_reorderable (GTK_CTREE(gmoiss_tree), FALSE); */


		/* now making it work with BROWSE selection, we MUST go to
		   Multiple one day when we are famous, rich and with no more
		   critic things to do <g> */
		gtk_clist_set_selection_mode(GTK_CLIST(gmoiss_tree),
					     GTK_SELECTION_BROWSE);

		/* connecting */

		gtk_signal_connect (GTK_OBJECT (gmoiss_tree),
				    "tree_select_row",
				    GTK_SIGNAL_FUNC(gmoiss_tree_selected), NULL);

		hbox = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
		gtk_box_pack_start(GTK_BOX(gmoiss_tree_vbox), hbox, FALSE, FALSE, 0);

		align = gtk_alignment_new(0.0, 0.5, 0, 0);
		gtk_box_pack_start(GTK_BOX(hbox), align, FALSE, FALSE, 0);

		vbox = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);
		gtk_container_border_width(GTK_CONTAINER(vbox), 
					   GNOME_PAD_SMALL);
		gtk_container_add (GTK_CONTAINER (align), vbox);

		hbox2 = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
		gtk_box_pack_start(GTK_BOX(vbox), hbox2, FALSE, FALSE, 0);
		label = gtk_label_new(_("Theory level:   "));
		gtk_box_pack_start(GTK_BOX(hbox2), label, FALSE, FALSE, 0);
		t_level.opt_menu = option = gtk_option_menu_new();
		t_level.menu = menu = gtk_menu_new ();

		for (i = 0; *mc_names[i]; i++) {
			GtkWidget *item;

			item = gtk_menu_item_new_with_label (_(mc_names[i]));

			/* Connecting the signal */

			gtk_signal_connect (GTK_OBJECT(item), "activate",
					    GTK_SIGNAL_FUNC(change_mc_type),
					    GINT_TO_POINTER (i + MC_VARIATIONAL));

			gtk_menu_append (GTK_MENU (menu), item);
			gtk_widget_show (item);
		}
		gtk_option_menu_set_menu(GTK_OPTION_MENU(option), menu);
		gtk_option_menu_set_history(GTK_OPTION_MENU(option), 
					    def_mc.type);
		gtk_box_pack_start(GTK_BOX(hbox2), option, FALSE, FALSE, 0);
		gtk_widget_show(option);
		 
		hbox2 = my_new_hbox();
		 
		t_level.block_entry = my_new_int_entry(_("Blocks:           "),
							hbox2,
							change_mc_blocks,
						       /* my_pl_get_int(dictionary_names[ RANDOM_NUMBER_GENERATOR_SEED])
							  FIXME: right now it's a constant
							  �should be loaded from an array of
							  walk types!
							  def_mc should die one day 
							*/
							def_mc.blocks,  
							CONVERGENCE_DIGITS
							);
		gtk_box_pack_start(GTK_BOX(vbox), hbox2, FALSE, FALSE, 0);

		hbox2 = gtk_hbox_new(FALSE, GNOME_PAD_SMALL);
		gtk_box_pack_start(GTK_BOX(vbox), hbox2, FALSE, FALSE, 0);
		label = gtk_label_new(_("Iterations/Block:"));
		gtk_box_pack_start(GTK_BOX(hbox2), label, FALSE, FALSE, 0);
		t_level.iter_entry = entry = gtk_entry_new();

		/* connecting */

		gtk_signal_connect (GTK_OBJECT(entry), "activate",
				    GTK_SIGNAL_FUNC(change_mc_iterations),
				    entry);


		text = g_malloc(11);
		snprintf(text, 11, "%d", def_mc.iterations);
		gtk_entry_set_text(GTK_ENTRY(entry), text);
		g_free(text);
		gtk_box_pack_start(GTK_BOX(hbox2), entry, FALSE, FALSE, 0);
		 
		 
		  
		vbox = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);
		gtk_box_pack_start(GTK_BOX(hbox), vbox, FALSE, FALSE, 0);
		  
		frame = gtk_frame_new(_("Statistics"));
		gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 0);

		vbox = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);
		gtk_container_add(GTK_CONTAINER(frame), vbox);
		gtk_container_border_width(GTK_CONTAINER(vbox), 
					   GNOME_PAD_SMALL);

		
		table = gtk_table_new (3, 2, TRUE);
		
		gtk_box_pack_start (GTK_BOX(vbox), table, TRUE, TRUE, 0);
				    
		for (i = 0; *stat_names[i]; i++) {
			GtkWidget *check;
		    
			t_level.toggles[i] = check = gtk_check_button_new_with_label (stat_names[i]);
		    
			gtk_signal_connect (GTK_OBJECT (check),
					    "toggled",
					    GTK_SIGNAL_FUNC(stats_toggled),
					    GINT_TO_POINTER(i)
					    );
		    
			gtk_table_attach (GTK_TABLE(table), check, x, x + 1, y, y + 1, GTK_SHRINK, GTK_SHRINK, 2, 2);
			x++;
			if (x >= 2) {
				y++;
				x = 0;
			}
		}
		vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);

		gtk_box_pack_start (GTK_BOX(hbox), vbox, FALSE, FALSE, 0);

		frame = gtk_frame_new(_("Theory Level"));
		gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 0);

		vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
		gtk_container_add (GTK_CONTAINER(frame), vbox);
		gtk_container_border_width(GTK_CONTAINER(vbox), GNOME_PAD_SMALL);

		button = gtk_button_new_with_label(_("Add"));
		gtk_signal_connect (GTK_OBJECT(button), "clicked",
				    (GtkSignalFunc)mc_add_callback,
				    NULL);
		gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);

		button = gtk_button_new_with_label(_("Remove"));
		gtk_signal_connect (GTK_OBJECT(button), "clicked",
				    (GtkSignalFunc)mc_remove_callback,
				    NULL);
		gtk_box_pack_start(GTK_BOX(vbox), button, FALSE, FALSE, 0);

		gtk_widget_show_all(gmoiss_tree_vbox);
	}

	gtk_clist_clear(GTK_CLIST(gmoiss_tree));
	 
	/*	for (node = mc_list; node; node = node->next)*/
	gmoiss_add_mc_to_tree(gmoiss_tree, mc);
}


static void
create_path (void) {
  GtkWidget *vbox, *frame, *hbox, *table, *vbox2, *label;
  GtkWidget *button;
  

  vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_start(GTK_BOX(gmoiss_path_vbox), vbox, TRUE, TRUE, 0);
   
	frame = gtk_frame_new(_("Pathnames"));
	gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);
	
	table = gtk_table_new (3 , 2, TRUE);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);
	
	gtk_container_add(GTK_CONTAINER(frame), table);
	
	hbox = my_new_hbox();
	label = gtk_label_new (_("Parameters file path"));
	parameters_entry = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(parameters_entry), my_pl_get_string(dictionary_names[PARAMETERS_PATH]));
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), parameters_entry, TRUE, TRUE, 0);
	
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 0, 1, GTK_SHRINK, GTK_SHRINK, 0, 0);

	gtk_signal_connect (GTK_OBJECT(parameters_entry), "activate",
						GTK_SIGNAL_FUNC(change_parameters_path),
						parameters_entry);
	
	hbox = my_new_hbox();
	label = gtk_label_new (_("'moiss' binary location"));
	moiss_entry = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(moiss_entry), my_pl_get_string(dictionary_names[MOISS_PATH]));
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), moiss_entry, TRUE, TRUE, 0);
	
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 1, 2, GTK_SHRINK, GTK_SHRINK, 0, 0);

	gtk_signal_connect (GTK_OBJECT(moiss_entry), "activate",
						GTK_SIGNAL_FUNC(change_moiss_path),
						moiss_entry);
	
		hbox = my_new_hbox();
	label = gtk_label_new (_("Output base directory"));
	output_entry = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(output_entry), my_pl_get_string(dictionary_names[OUTPUT_PATH]));
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), output_entry, TRUE, TRUE, 0);
	
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 2, 3, GTK_SHRINK, GTK_SHRINK, 0, 0);

	gtk_signal_connect (GTK_OBJECT(output_entry), "activate",
						GTK_SIGNAL_FUNC(change_output_path),
						output_entry);

	/* and now the three buttons */
	button = gtk_button_new_with_label(_("Change..."));
	gtk_signal_connect (GTK_OBJECT(button), "clicked",
						(GtkSignalFunc)parameters_file_dialog,
						NULL);

	gtk_table_attach(GTK_TABLE(table), button, 1, 2 , 0 , 1, GTK_SHRINK, GTK_SHRINK, 0, 0);
	button = gtk_button_new_with_label(_("Change..."));
	gtk_signal_connect (GTK_OBJECT(button), "clicked",
						(GtkSignalFunc)moiss_file_dialog,
						NULL);

	gtk_table_attach(GTK_TABLE(table), button, 1, 2 , 1 , 2, GTK_SHRINK, GTK_SHRINK, 0, 0);
	
	button = gtk_button_new_with_label(_("Change..."));
	gtk_signal_connect (GTK_OBJECT(button), "clicked",
						(GtkSignalFunc)output_file_dialog,
						NULL);

	gtk_table_attach(GTK_TABLE(table), button, 1, 2 , 2 , 3, GTK_SHRINK, GTK_SHRINK, 0, 0);
	
	gtk_widget_show_all(gmoiss_path_vbox);
}	

static void
create_atomic (void) {
   
	proplist_t atomic_array, pl, key, value;
	GtkWidget *vbox, *clist, *frame, *hbox, *table, *button, *vbox2;
	GtkWidget *scrolled_window;
	GtkAdjustment *hadjustment, *vadjustment;
	GtkWidget *vbox3;
	int i, j;
	char *titles[] = {
		_("Atom #"),
		_("Z"),
		_("Symbol"),
		_("x"),
		_("y"),
		_("z")
	};
	char *my_string[ATOMIC_COORDINATES_COLUMNS];

   
	/* temporal */ 
	create_atomic_array();  
   
	vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_start(GTK_BOX(gmoiss_atomic_vbox), vbox, TRUE, TRUE, 0);
   
	frame = gtk_frame_new(_("Atomic Coordinates"));
	gtk_box_pack_start(GTK_BOX(vbox), frame, TRUE, TRUE, 0);

	hadjustment= (GtkAdjustment *) gtk_adjustment_new (0, 0, 1, 0.01, 0.2, 0.2);
	vadjustment= (GtkAdjustment *) gtk_adjustment_new (0, 0, 1, 0.01, 0.2, 0.2);

	scrolled_window = gtk_scrolled_window_new(GTK_ADJUSTMENT(hadjustment),
						  GTK_ADJUSTMENT(vadjustment));
	
	
	clist = gtk_clist_new_with_titles (ATOMIC_COORDINATES_COLUMNS, titles);
	gtk_clist_set_selection_mode (GTK_CLIST(clist), GTK_SELECTION_BROWSE);
	atomic_clist = GTK_CLIST(clist);
   
	gtk_clist_set_row_height (GTK_CLIST (clist), 18);
	gtk_widget_set_usize (clist, -1, ATOMIC_TABLE_HEIGHT);
   
	gtk_clist_set_column_width (GTK_CLIST (clist), 0, 100);
   
	for (i = 1; i < ATOMIC_COORDINATES_COLUMNS; i++)
		{
			gtk_clist_set_column_width (GTK_CLIST (clist), i, 80);
			gtk_clist_set_column_justification (GTK_CLIST (clist), i,
							    GTK_JUSTIFY_CENTER);
		}
   
	gtk_clist_set_selection_mode (GTK_CLIST (clist), GTK_SELECTION_BROWSE);
	gtk_clist_select_row (GTK_CLIST (clist), 0, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_window)
					, GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	
	
	gtk_container_border_width (GTK_CONTAINER(clist), GNOME_PAD_SMALL);
	gtk_container_add(GTK_CONTAINER(scrolled_window), clist);
	gtk_container_add(GTK_CONTAINER(frame), scrolled_window);
	
	/* see insert_row_clist from testgtk.c */
	/* reading the atomic coordinates from the proplist */
	atomic_array = my_pl_get_array(dictionary_names[ATOMIC_COORDINATES]);
	i=0;

   
   
	while ((pl = PLGetArrayElement(atomic_array, i)) != NULL) {
		unsigned char * debug_description;
     
		for (j=0; j < ATOMIC_COORDINATES_COLUMNS ; j++) {
			my_string[j] = g_malloc(DICTIONARY_STRING_SIZE);
		}
		sprintf(my_string[0], "%d", i);
     
		key = PLMakeString(atomic_coordinates[ATOMIC_X]);
		value = PLGetDictionaryEntry(pl, key);
		PLRelease(key);
		my_string[3] = strcpy(my_string[3], PLGetString(value));
	/* 	PLRelease(value); */

		key  = PLMakeString(atomic_coordinates[ATOMIC_Y]);
		value = PLGetDictionaryEntry(pl, key);
		PLRelease(key);
		my_string[4] = strcpy(my_string[4], PLGetString(value));
	/* 	PLRelease(value) */;
     
		key = PLMakeString(atomic_coordinates[ATOMIC_Z]);
		value = PLGetDictionaryEntry(pl, key);
		PLRelease(key);
		my_string[5] = strcpy(my_string[5], PLGetString(value));
	/* 	PLRelease(value); */

		key = PLMakeString(atomic_coordinates[ATOMIC_NUMBER]);
		value = PLGetDictionaryEntry(pl, key);
		PLRelease(key);
		my_string[1] = strcpy(my_string[1], PLGetString(value));
	/* 	PLRelease(value); */

		my_string[2] = strcpy(my_string[2], atomic_names[atoi(my_string[1])]);
     
		gtk_clist_insert (GTK_CLIST (clist), i, my_string);
     

		/*     PLRelease(pl); */
      
		for (j=0; j < ATOMIC_COORDINATES_COLUMNS ; j++) {
			g_free(my_string[j]);
		}
     
		i++;
	}
   
	frame = gtk_frame_new(_("Options"));

	gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 0);
   
   
   
	/* rows, columns, homogeneus */
   
	table = gtk_table_new (2 , 2, TRUE);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);
     
	gtk_container_add(GTK_CONTAINER(frame), table);

	vbox2 = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	
	button = gtk_button_new_with_label(_("Add"));
	gtk_signal_connect (GTK_OBJECT(button), "clicked",
			    (GtkSignalFunc)atomic_add_callback,
			    NULL);
	gtk_box_pack_start(GTK_BOX(vbox2), button, FALSE, FALSE, 0);
	
	button = gtk_button_new_with_label(_("Remove"));
	gtk_signal_connect (GTK_OBJECT(button), "clicked",
			    (GtkSignalFunc)atomic_remove_callback,
			    NULL);
	gtk_box_pack_start(GTK_BOX(vbox2), button, FALSE, FALSE, 0);
	
	gtk_table_attach (GTK_TABLE (table), vbox2, 0, 1, 0, 1,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);

	vbox3 = gtk_vbox_new(FALSE, GNOME_PAD_SMALL);

	hbox = my_new_hbox();
	my_new_int_entry(_("Spin Parity Operator"),
			 hbox,
			 change_spin_parity,
			 my_pl_get_int(dictionary_names[SPIN_PARITY_OPERATOR]),
			 CONVERGENCE_DIGITS
			 );
	gtk_box_pack_start(GTK_BOX(vbox3), hbox, FALSE, FALSE, 0);
	
	/* used bit's dialog, left if we want to return to the old
	   atomic entry way of doing things 
	hbox = my_new_hbox();
	my_new_float_entry(_("x"),
			   hbox,
			   change_atomic_x,
			   DEFAULT_ATOMIC_X,
			   DT_DIGITS		
			   );
	gtk_box_pack_start(GTK_BOX(vbox3), hbox, FALSE, FALSE, 0);

	hbox = my_new_hbox();
	
	my_new_float_entry(_("y"),
			   hbox,
			   change_atomic_y,
			   DEFAULT_ATOMIC_Y,
			   DT_DIGITS		
			   );
	gtk_box_pack_start(GTK_BOX(vbox3), hbox, FALSE, FALSE, 0);

	hbox = my_new_hbox();

	my_new_float_entry(_("z"),
			   hbox,
			   change_atomic_z,
			   DEFAULT_ATOMIC_Z,
			   DT_DIGITS		
			   );
	gtk_box_pack_start(GTK_BOX(vbox3), hbox, FALSE, FALSE, 0);
	
	hbox = my_new_hbox();
		
	my_new_float_entry(_("Z"),
			   hbox,
			   change_atomic_Z,
			   DEFAULT_ATOMIC_NUMBER,
			   DT_DIGITS		
			   );
	gtk_box_pack_start(GTK_BOX(vbox3), hbox, FALSE, FALSE, 0);
	*/
	gtk_table_attach (GTK_TABLE (table), vbox3, 1, 2, 0, 2,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);
	
			  
	/* showing everything */
	gtk_widget_show_all(gmoiss_atomic_vbox);
	PLRelease(atomic_array);
}

static void
create_initial_setup(void)
{

	GtkWidget *hbox, *frame, *hbox2, *table, *label;
	char * description_string;

	 
	hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_start(GTK_BOX(gmoiss_setup_vbox), hbox, FALSE, FALSE, 0);

	frame = gtk_frame_new(_("Problem"));

	/* rows, columns, homogeneus */
	table = gtk_table_new (3 , 2, TRUE);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);


	gtk_box_pack_start(GTK_BOX(hbox), frame, FALSE, FALSE, 0);
	gtk_container_add(GTK_CONTAINER(frame), table);


	hbox2 = my_new_hbox();

	description_string = my_pl_get_string(dictionary_names[POTENTIAL]);
	
	my_new_choice ( _("Potential type  "),
			hbox2,
			potential_names,
			change_potential_type ,                                
                        find_names_index(potential_names ,description_string ));
	g_free(description_string);                                             

	gtk_table_attach (GTK_TABLE (table), hbox2, 0, 1, 0, 1,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);

	hbox2 = my_new_hbox();
	 
	description_string = my_pl_get_string(dictionary_names[TRIAL_FUNCTION]);

	my_new_choice ( _("Trial Function type"),
			hbox2,
			trial_function_names,
			change_trial_function_type ,
			find_names_index(trial_function_names, description_string ));
	g_free(description_string);

	gtk_table_attach (GTK_TABLE (table), hbox2, 1, 2, 0, 1,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);

	hbox2 = my_new_hbox();
	 
	description_string = my_pl_get_string(dictionary_names[INITIAL_POSITION]);

	my_new_choice ( _("Initial Positions"), 
			hbox2, 
			initial_position_names, 
			change_initial_position, 
			find_names_index(initial_position_names, description_string ));
	g_free(description_string);

	gtk_table_attach (GTK_TABLE (table), hbox2, 0 , 1, 1, 2,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);

	hbox2 = my_new_hbox();

	description_string =  my_pl_get_string(dictionary_names[DIMENSIONS] );

	my_new_choice ( _("Dimensions         "), 
			hbox2, 
			dimension_names, 
			change_dimensions, 
			find_names_index(dimension_names, description_string));
	  
	g_free(description_string);

	gtk_table_attach (GTK_TABLE (table), hbox2, 1 , 2, 1, 2,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);

	hbox = my_new_hbox();
	label = gtk_label_new(_("Job name   "));
	job_entry =  gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(job_entry), my_pl_get_string(dictionary_names[JOB_NAME]));
	gtk_box_pack_start(GTK_BOX(hbox), label, TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), job_entry, TRUE, TRUE, 0);
	
	gtk_table_attach(GTK_TABLE(table), hbox, 0, 1, 2, 3, GTK_SHRINK, GTK_SHRINK, 0, 0);
	gtk_signal_connect (GTK_OBJECT(job_entry), "activate",
						GTK_SIGNAL_FUNC(change_job_path),
						job_entry);
	

	/* now the options box */

	hbox = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_start(GTK_BOX(gmoiss_setup_vbox), hbox, FALSE, FALSE, 0);

	frame = gtk_frame_new(_("Options"));
	gtk_box_pack_start(GTK_BOX(hbox), frame, FALSE, FALSE, 0);

	table = gtk_table_new (4 , 1, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);

	gtk_container_add(GTK_CONTAINER(frame), table);

	hbox2 = my_new_hbox();

	my_new_int_spinbutton(_("Particles"),
			      hbox2,
			      change_particles,
			      NORMAL_SPIN_SIZE,
			      1,
			      MAX_PARTICLES,
			      my_pl_get_int(dictionary_names[PARTICLES]));

	gtk_table_attach (GTK_TABLE (table), hbox2, 0, 1, 0, 1,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);

	hbox2 = my_new_hbox();

	my_new_int_spinbutton(_("Walkers"),
			      hbox2,
			      change_walkers,
			      BIG_SPIN_SIZE,
			      1,
			      MAX_WALKERS,
			      my_pl_get_int(dictionary_names[WALKERS]));

	gtk_table_attach (GTK_TABLE (table), hbox2, 1, 2, 0, 1,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);

	hbox2 = my_new_hbox();

	my_new_float_entry(_("Timestep (dt)"),
			   hbox2,
			   change_dt,
			   my_pl_get_float(dictionary_names[TIME_STEP]),
			   DT_DIGITS		
			   );

	gtk_table_attach (GTK_TABLE (table), hbox2, 2, 3, 0, 1,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);
	
	 
	/* showing everything */
	gtk_widget_show_all(gmoiss_setup_vbox);
}


/* Correlated Exponential Wavefunction options */
/* Chem Phys Lett 240 (1995) 566-570 Bressanini et al */

static void create_correlated_exponential(void)
{
       
    GtkWidget *vbox, *frame, *hbox2, *table, *button, *check1, *check2;
    char * description_string;
    vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
    gtk_box_pack_start(GTK_BOX(gmoiss_corr_vbox), vbox, FALSE, FALSE, 0);
    
    frame = gtk_frame_new(_("Correlated Exponential Trial Function Options"));
    gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 0);
    
    table = gtk_table_new (3 , 2, TRUE);
    gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
    gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);
    
    gtk_container_add(GTK_CONTAINER(frame), table);
    hbox2 = my_new_hbox();
    
    description_string = my_pl_get_string(dictionary_names[ELEC_ELEC_TF]);
    my_new_choice ( _("Electron-Electron TF "),
		    hbox2,
		    electron_tf_names,
		    change_electron_tf ,
		    find_names_index(electron_tf_names ,description_string ));
    g_free(description_string);
    
    gtk_table_attach (GTK_TABLE (table), hbox2,0 ,1, 0, 1,
		      GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
		      0 , 0);
    hbox2 = my_new_hbox();
    description_string = my_pl_get_string(dictionary_names[NUCLEI_ELEC_TF]);
    my_new_choice ( _("Electron-Nuclei TF "),
		    hbox2,
		     nuclei_tf_names,
		    change_nuclei_tf ,
		    find_names_index(nuclei_tf_names ,description_string ));
    g_free(description_string);
    gtk_table_attach (GTK_TABLE (table), hbox2,0 ,1, 1, 2,
					  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
				  0 , 0);
    
     
	hbox2 = my_new_hbox ();
	my_new_int_spinbutton(_("Electron-Nuclei parameters "),
						  hbox2,
						  change_ce_number_of_e_n_params,
					  NORMAL_SPIN_SIZE,
						  1,
						  MAX_CE_TERMS,
						  my_pl_get_int(dictionary_names[ELEC_NUCLEI_PARAMS]));

	gtk_table_attach (GTK_TABLE (table), hbox2,1 ,2, 0, 1,
					  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
				  0 , 0);

    hbox2 = my_new_hbox();
    
    my_new_int_spinbutton(_("Number of phi terms "),
			  hbox2,
			  change_ce_number_of_terms,
			  NORMAL_SPIN_SIZE,
			  1,
			  MAX_CE_TERMS,
			  my_pl_get_int(dictionary_names[CE_TERMS]));
    gtk_table_attach (GTK_TABLE (table), hbox2,1 ,2, 1, 2,
		      GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
		      0 , 0);
	hbox2 = my_new_hbox();
	my_new_int_spinbutton(_("Number of prefactor terms "),
						  hbox2,
						  change_ce_number_of_prefactor,
						  NORMAL_SPIN_SIZE,
						  /* FIXME, there should be a button for
							 not using instead of -1 */
						  -1,
						  MAX_CE_TERMS,
						  my_pl_get_int(dictionary_names[CE_PREFACTOR_TERMS]));
    gtk_table_attach (GTK_TABLE (table), hbox2,0 ,1, 2, 3,
					  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
		      0 , 0);
	button = gtk_button_new_with_label(_("Set prefactor terms..."));
	gtk_table_attach (GTK_TABLE (table), button, 1, 2, 2, 3,
					  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
					  0, 0);
	gtk_signal_connect(GTK_OBJECT(button), "clicked",
					   (GtkSignalFunc) set_prefactor_terms,
					   NULL);
	
    frame = gtk_frame_new(_("Symmetry options"));
    gtk_box_pack_start(GTK_BOX(vbox), frame, FALSE, FALSE, 0);

    table = gtk_table_new (2 , 2, TRUE);
    gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
    gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);
 
    gtk_container_add(GTK_CONTAINER(frame), table);

    hbox2 = my_new_hbox();
    terms_spinner = my_new_int_spinbutton(_("Number of terms "),
										  hbox2,
										  change_sym_number_of_terms,
										  NORMAL_SPIN_SIZE,
			  1,
										  MAX_SYM_TERMS,
										  sym_terms);
	gtk_table_attach (GTK_TABLE (table), hbox2,0 ,1, 0, 1,
					  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
					  0 , 0);

  button = gtk_button_new_with_label(_("Symmetry matrix..."));
  gtk_table_attach (GTK_TABLE (table), button, 1, 2, 0, 1,
		    GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
		    0, 0);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
		     (GtkSignalFunc) set_symmetry,
		       NULL);
  check1 = gtk_check_button_new_with_label(_("Electron-Electron Symmetry"));
  check2 = gtk_check_button_new_with_label(_("Electron-Nuclei Symmetry"));
  if (find_names_index(boolean_names,my_pl_get_string(dictionary_names[SYM_ELEC_ELEC])))
	{
	  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(check1), TRUE);
	}
  if (find_names_index(boolean_names,my_pl_get_string(dictionary_names[SYM_ELEC_NUCLEI])))
	{
	  gtk_toggle_button_set_state(GTK_TOGGLE_BUTTON(check2), TRUE);
	}

  gtk_signal_connect(GTK_OBJECT(check1),
					 "toggled",
					 GTK_SIGNAL_FUNC(sym_elec_toggled),
					 NULL);
  gtk_signal_connect(GTK_OBJECT(check2),
					  "toggled",
					 GTK_SIGNAL_FUNC(sym_nuclei_toggled),
					  NULL);
  
  hbox2=my_new_hbox();
  gtk_box_pack_start (GTK_BOX(hbox2), check1, FALSE, FALSE,0);
  
  gtk_table_attach (GTK_TABLE (table), hbox2, 0, 1, 1, 2,
					GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
					0, 0);
  
  hbox2=my_new_hbox();
  gtk_box_pack_start (GTK_BOX(hbox2), check2, FALSE, FALSE,0);
  
  gtk_table_attach (GTK_TABLE (table), hbox2, 1, 2, 1, 2,
					GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
					0, 0);

   gtk_widget_show_all(gmoiss_corr_vbox);
  
}

static void create_potential(void)
{
    GtkWidget *hbox, *frame, *hbox2, *table, *button;
    
    hbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
    gtk_box_pack_start(GTK_BOX(gmoiss_pot_vbox), hbox, FALSE, FALSE, 0);
    
    frame = gtk_frame_new(_("Potential Options"));
    gtk_box_pack_start(GTK_BOX(hbox), frame, FALSE, FALSE, 0);

    table = gtk_table_new (1 , 2, TRUE);
    gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
    gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
    gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);
    gtk_container_add(GTK_CONTAINER(frame), table);

    gtk_widget_show_all(gmoiss_pot_vbox);

    hbox2 = my_new_hbox();
    my_new_float_entry(_("Electronic Repulsion Factor"),
		       hbox2,
		       change_electronic_repulsion_factor,
		       my_pl_get_float(dictionary_names[ELECTRONIC_REPULSION]),
		       CONVERGENCE_DIGITS
		       );
    gtk_table_attach (GTK_TABLE (table), hbox2, 0, 1, 0, 1,
		      GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
		      0 , 0);
    button = gtk_button_new_with_label(_("Change mass & charges..."));
    gtk_table_attach (GTK_TABLE (table), button, 1, 2, 0, 1,
		      GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
		      0, 0);
    gtk_signal_connect(GTK_OBJECT(button), "clicked",
		       (GtkSignalFunc) set_mass_and_charges,
		       NULL);

    gtk_widget_show_all(table);
}

static void
create_other_options(void)
{
	GtkWidget *hbox, *frame, *hbox2, *table;

	hbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_box_pack_start(GTK_BOX(gmoiss_other_vbox), hbox, FALSE, FALSE, 0);

	frame = gtk_frame_new(_("Other Options"));
	gtk_box_pack_start(GTK_BOX(hbox), frame, FALSE, FALSE, 0);

	table = gtk_table_new (4 , 2, TRUE);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);

	gtk_container_add(GTK_CONTAINER(frame), table);

	gtk_widget_show_all(gmoiss_other_vbox);

	hbox2 = my_new_hbox();

	my_new_int_spinbutton(_("Max. branching "),
			      hbox2,
			      change_max_branches,
			      NORMAL_SPIN_SIZE,
			      1,
			      MAX_BRANCHING,
			      my_pl_get_int(dictionary_names[MAXIMUM_BRANCHES]));

	gtk_table_attach (GTK_TABLE (table), hbox2, 0, 1, 0, 1,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);

	hbox2 = my_new_hbox();

	my_new_float_entry(_("Timestep convergence"),
			   hbox2,
			   change_dt_convergence_factor,
			   my_pl_get_float(dictionary_names[DT_CONVERGENCE_FACTOR]),
			   CONVERGENCE_DIGITS
			   );

	gtk_table_attach (GTK_TABLE (table), hbox2, 1, 2, 0, 1,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);

	hbox2 = my_new_hbox();

	my_new_float_entry(_("Gaussian Spread  "),
			   hbox2,
			   change_gaussian_spread_range,
			   my_pl_get_int(dictionary_names[GAUSSIAN_SPREAD_RANGE]),
			   CONVERGENCE_DIGITS
			   );

	gtk_table_attach (GTK_TABLE (table), hbox2, 0, 1, 1, 2,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);

	hbox2 = my_new_hbox();

	my_new_float_entry(_("Gaussian TF alpha     "),
			   hbox2,
			   change_gaussian_tf_alpha,
			   problem_data.gaussian_tf_alpha,
			   CONVERGENCE_DIGITS
			   );
	gtk_table_attach (GTK_TABLE (table), hbox2, 1, 2, 1, 2,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);
		

	hbox2 = my_new_hbox();

	my_new_int_entry(_("Random Seed       "),
			 hbox2,
			 change_random_seed,
			 my_pl_get_int(dictionary_names[ RANDOM_NUMBER_GENERATOR_SEED]),  CONVERGENCE_DIGITS
			 );
	 
	gtk_table_attach (GTK_TABLE (table), hbox2, 0,1, 2, 3,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);

	hbox2 = my_new_hbox();
	
	my_new_float_entry(_("Diffusion Constant      "),
			   hbox2,
			   change_diffusion_constant,
			   my_pl_get_float(dictionary_names[DIFFUSION_CONSTANT ]),  CONVERGENCE_DIGITS
			   );
	
	gtk_table_attach (GTK_TABLE (table), hbox2,1,2, 2, 3,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);
	
	
	hbox2 = my_new_hbox();
	 
	my_new_float_entry(_("Lehnard-Jones Sigma "),
			   hbox2,
			   change_lj_sigma,
			   my_pl_get_float(dictionary_names[LEHNARD_JONES_SIGMA]),
			   CONVERGENCE_DIGITS
			   );
	 
	gtk_table_attach (GTK_TABLE (table), hbox2,0,1, 3, 4,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);
	 hbox2 = my_new_hbox();
	 
	my_new_float_entry(_("Lehnard-Jones Epsilon "),
			   hbox2,
			   change_lj_epsilon,
			   my_pl_get_float(dictionary_names[LEHNARD_JONES_EPSILON]),  CONVERGENCE_DIGITS);
	 
	gtk_table_attach (GTK_TABLE (table), hbox2,1,2, 3, 4,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);
	
	frame = gtk_frame_new(_("Graphics Options "));
	gtk_box_pack_start(GTK_BOX(hbox), frame, FALSE, FALSE, 0);

	table = gtk_table_new (2 , 2, TRUE);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD_SMALL);
	gtk_container_border_width (GTK_CONTAINER (table), GNOME_PAD_SMALL);

	gtk_container_add(GTK_CONTAINER(frame), table);

	gtk_widget_show_all(gmoiss_other_vbox);
	 
	hbox2 = my_new_hbox();
	
	my_new_int_spinbutton(_("Divisions / dimension "),
			      hbox2,
			      change_divs_per_dimension,
			      DEFAULT_DIVS_PER_DIMENSION,
			      1,
			      MAX_DIVS_PER_DIMENSION,
			      my_pl_get_int(dictionary_names[DIVS_PER_DIMENSION]));
	gtk_table_attach (GTK_TABLE (table), hbox2,0 ,1, 0, 1,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);

	hbox2 = my_new_hbox();

	my_new_float_entry(_("Minimum histogram value "),
			   hbox2,
			   change_min_histogram,
			   my_pl_get_float(dictionary_names[GRAPH_MINIMUM]),  CONVERGENCE_DIGITS);
	gtk_table_attach (GTK_TABLE (table), hbox2,1 ,2, 0, 1,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);
	
	hbox2 = my_new_hbox();
	
	my_new_float_entry(_("Maximum histogram value "),
			   hbox2,
			   change_max_histogram,
			   my_pl_get_float(dictionary_names[GRAPH_MAXIMUM]),  CONVERGENCE_DIGITS);
	gtk_table_attach (GTK_TABLE (table), hbox2,1 ,2, 1, 2,
			  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL,
			  0 , 0);
	
}


static void
create_pages (GtkNotebook *notebook, gint start, gint end)
{
	GtkWidget *child = NULL;
	GtkWidget *label;
	GtkWidget *label_box;
	GtkWidget *menu_box;


	gint i;
	char buffer[32];

	for (i = start; i <= end; i++)
		{
			switch (i) 
				{
				case 8:
				  sprintf(buffer, _("MOISS Output"));
				  child = gmoiss_output_vbox = 
					gtk_vbox_new(TRUE, GNOME_PAD_SMALL);
				  break;
				  
				case 7:
				  sprintf(buffer, _("Correlated Exponential"));
				  child = gmoiss_corr_vbox =
					gtk_vbox_new(TRUE,GNOME_PAD_SMALL);
				  create_correlated_exponential();
				  break;
				  
				case 6:
				  sprintf(buffer, _("Potential Options"));
				  child = gmoiss_pot_vbox =
					gtk_vbox_new(TRUE,GNOME_PAD_SMALL);
				  create_potential();
				  break;
				case 5:
				  sprintf(buffer, _("File Locations"));
				  child = gmoiss_path_vbox =
					gtk_vbox_new(TRUE,GNOME_PAD_SMALL);
				  create_path();
				  break;
				case 4: 
				  sprintf(buffer, _("Atomic Coordinates"));
				  child = gmoiss_atomic_vbox =
					gtk_vbox_new(FALSE, GNOME_PAD_SMALL);
				  create_atomic();
				  break;
				case 3:
				  sprintf (buffer, _("Other Options"));
				  child = gmoiss_other_vbox =
					gtk_vbox_new (TRUE, GNOME_PAD_SMALL);
				  create_other_options();
				  break;
				  
				case 2:
				  sprintf (buffer, _("Theory Levels"));
				  child = gmoiss_tree_vbox =
					gtk_vbox_new(FALSE,GNOME_PAD_SMALL);
				  create_tree(); /* working on create_tree again */
						break;
				case 1:
				  sprintf (buffer, _("Initial Setup"));
				  child = gmoiss_setup_vbox =
					gtk_vbox_new (TRUE, GNOME_PAD_SMALL);
				  create_initial_setup();
				  break;
				}
			gtk_widget_show_all(child);
			label_box = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
			/* insert pixmap here if wanted (see testgtk.c for how to do it) */
			label = gtk_label_new (buffer);
			gtk_box_pack_start (GTK_BOX (label_box), label, FALSE, TRUE, 0);
			gtk_widget_show_all (label_box);

			menu_box = gtk_hbox_new (FALSE, GNOME_PAD_SMALL);
			/* insert another pixmap if you want */
			label = gtk_label_new(buffer);
			gtk_box_pack_start (GTK_BOX (menu_box), label, FALSE, TRUE, 0);
			gtk_widget_show_all (menu_box);

		

			gtk_notebook_append_page_menu (notebook, child, label_box, menu_box);
		}
}


void gmoiss_calc_new(GtkWidget *widget, gpointer *data)
{

	if(!gmoiss_notebook) {
    
		gmoiss_notebook = gtk_notebook_new();
    
		gtk_notebook_set_tab_pos(GTK_NOTEBOOK(gmoiss_notebook), GTK_POS_TOP);
		gtk_notebook_set_scrollable(GTK_NOTEBOOK(gmoiss_notebook), YES);
		gtk_notebook_popup_enable(GTK_NOTEBOOK(gmoiss_notebook));
		gtk_box_pack_start(GTK_BOX(gmoiss_vbox), gmoiss_notebook, 
				   TRUE, TRUE, 0);
		gtk_container_border_width (GTK_CONTAINER (gmoiss_notebook), 10);
		gtk_widget_realize(gmoiss_notebook);
    
		create_pages(GTK_NOTEBOOK(gmoiss_notebook), 1, 8);
    
		gtk_widget_show_all(gmoiss_window);
	}
}

GnomeUIInfo calcmenu[] = {
	{GNOME_APP_UI_ITEM, N_("_New"), N_("Create new calculation."),
	 gmoiss_calc_new, NULL, NULL,
	 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_NEW, 0, 0, NULL},
	{GNOME_APP_UI_SEPARATOR},
	{GNOME_APP_UI_ITEM, N_("_Open..."), N_("Load Properties from a file..."), gmoiss_open, NULL, NULL,
	 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_OPEN, 0, 0, NULL},
	{GNOME_APP_UI_ITEM, N_("_Save..."), N_("Save the properties to current file..."), gmoiss_save, NULL, NULL,
	 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE, 0, 0, NULL},
	{GNOME_APP_UI_ITEM, N_("Save _as..."), N_("Save the properties to a new filename..."), gmoiss_save_as, NULL, NULL,
	 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_SAVE, 0, 0, NULL},
	{GNOME_APP_UI_ITEM, N_("Sta_rt..."), N_("Start a calculation with the specified data..."), gmoiss_start_calculation, NULL, NULL,
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXEC, 0, 0, NULL},
	{GNOME_APP_UI_ITEM, N_("E_xit"), N_("Close the program."), 
	 gmoiss_quit, NULL, NULL,
	 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_EXIT, 0, 0, NULL},

	{GNOME_APP_UI_ENDOFINFO}
};

GnomeUIInfo helpmenu[] = {
	{GNOME_APP_UI_HELP, NULL, NULL, "moiss", NULL, NULL,
	 GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},

	{GNOME_APP_UI_ITEM, N_("_About..."), N_("Version, credits, etc."), 
	 gmoiss_about, NULL, NULL,
	 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_MENU_ABOUT, 0, 0, NULL},

	{GNOME_APP_UI_ENDOFINFO}
};

GnomeUIInfo mainmenu[] = {

	{GNOME_APP_UI_SUBTREE, N_("_Calculation"), NULL, calcmenu, NULL, NULL,
	 GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},

	/* {GNOME_APP_UI_JUSTIFY_RIGHT}, */
	/* how it's done in the new 0.99? */
		
	{GNOME_APP_UI_SUBTREE, N_("_Help"), NULL, helpmenu, NULL, NULL,
	 GNOME_APP_PIXMAP_NONE, NULL, 0, 0, NULL},

	{GNOME_APP_UI_ENDOFINFO}
};

GnomeUIInfo toolbar[] = {

	{GNOME_APP_UI_ITEM, N_("New"), N_("New calculation."),
	 gmoiss_calc_new, NULL, NULL,
	 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_NEW, 0, 0, NULL},
	
	{GNOME_APP_UI_ITEM, N_("Open"), N_("Open a calculation."),
	 gmoiss_open, NULL, NULL,
	 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_OPEN, 0, 0, NULL},

	{GNOME_APP_UI_ITEM, N_("Save"), N_("Save Calculation"),
	 gmoiss_save, NULL, NULL,
	 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_SAVE, 0, 0, NULL},
	{GNOME_APP_UI_ITEM, N_("Save As..."), 
	 N_("Save Calculation as a new filename"),
	 gmoiss_save_as, NULL, NULL,
	 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_SAVE, 0, 0, NULL},
	{GNOME_APP_UI_ITEM, N_("Start..."), 
	 N_("Start a calculation with the specified data"),
	 gmoiss_start_calculation, NULL, NULL,
	 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_EXEC, 0, 0, NULL},
	{GNOME_APP_UI_ENDOFINFO}
};

int quit_callback (GtkWidget *widget, gpointer data) {
	gtk_main_quit ();
}

int main(int argc,char *argv[])
{
	GtkWidget *gpixmap;
	GtkWidget *statusbar;
	GdkWindow *gdkwindow;
	GdkWindowAttr attr;
	init_globals();
#ifdef HAVE_PVM
	printf ("Hola puta\n");
#endif
	/* Initializing Gnome */
	gnome_init ("gmoiss", GMOISS_VERSION_NUMBER, argc, argv);
	/* later change it to gmoiss_init_with_popt */
	gmoiss_init_pixes();
	gmoiss_window = gnome_app_new("gmoiss", "gmoiss: Main");
	gmoiss_init_greek(); 
	gtk_window_set_wmclass(GTK_WINDOW(gmoiss_window), "gmoiss",
			       "gmoiss");
	gtk_signal_connect(GTK_OBJECT(gmoiss_window), "delete_event",
			   GTK_SIGNAL_FUNC(quit_callback), NULL);

	statusbar = gtk_statusbar_new();
	
	gtk_widget_show_all (gmoiss_window);


	
	/* Parsing all other arguments here */
	
	/* (still to be done) */
	
	/* connecting the signal for closing the application */
	/*	gtk_signal_connect (GTK_OBJECT (gmoiss_window), "delete_event",
	 *			    GTK_SIGNAL_FUNC(),
	 NULL);*/
	
	/* vbox */
	
	gmoiss_vbox = gtk_vbox_new (FALSE, GNOME_PAD_SMALL);
	gtk_container_border_width (GTK_CONTAINER (gmoiss_vbox), 10);
	
	gpixmap = gnome_pixmap_new_from_xpm_d (weed_xpm);
	gtk_box_pack_start (GTK_BOX (gmoiss_vbox), gpixmap, FALSE, FALSE, 0);
	gtk_widget_show (gpixmap);

	/* now setting the application icon to the same image */
	/* we need to do more work on the icon, it doesn't work right now,
	   FIXME see Pavolv's code in BALSA */
	attr.window_type = GDK_WINDOW_TOPLEVEL;
	attr.wclass = GDK_INPUT_OUTPUT;
	attr.event_mask = GDK_STRUCTURE_MASK;
	attr.x = 0;
	attr.y = 0;
	attr.visual = gdk_imlib_get_visual();
	attr.colormap = gdk_imlib_get_colormap();
	attr.width = 32;
	attr.height = 24;
	
	gdkwindow = gdk_window_new(NULL, &attr, 0);
	gdk_window_set_icon(gmoiss_window->window, gdkwindow, GNOME_PIXMAP(gpixmap)->pixmap, GNOME_PIXMAP(gpixmap)->mask);
	gdk_window_set_icon_name(gmoiss_window->window, _("gmoiss"));

	gnome_app_set_contents(GNOME_APP(gmoiss_window), gmoiss_vbox);
	gnome_app_create_menus(GNOME_APP(gmoiss_window), mainmenu);
	gnome_app_create_toolbar(GNOME_APP(gmoiss_window), toolbar);
	gnome_app_set_statusbar(GNOME_APP(gmoiss_window), statusbar);
	
	gtk_main();
	
	return 0;
}

