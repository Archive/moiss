/* This file is part of MOISS
   
   Copyright (C) 1998-99 Alan Aspuru Guzik
			 Raul Perusquia Flores
			 Carlos Amador Bedolla
			 Francisco Bustamante
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
               
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
                           
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
                                    
*/

#ifndef __PL_H__
#define __PL_H__

#include <proplist.h>
#include <gsl/gsl_matrix.h>
/* returns a double array from a PL Array */
double * pl_array_to_double(proplist_t pl);
/* returns a proplist array made from a double array */
proplist_t double_array_to_pl(int size, double * array);
/* adds a key-value pair to the calculation-dict proplist */
void my_pl_add_string( char * key_string, char * key_value);
/* gets a value string from the calculation-dict proplist */
char * my_pl_get_string( char * key_string);
/* adds a key-value pair of string-int to a calculation-dict proplist */
void my_pl_add_int(char * key_string, int value);
/* obtains the int value for a string key in the calculation-dict proplist */
int my_pl_get_int(char * key_string);
/* obtains the float value for a string key in the calculation-dict proplist */
float my_pl_get_float(char * key_string);
/* adds a key-value pair of string-float to a calculation-dict proplist */
void my_pl_add_float(char * key_string, float value);
/* obtains an array of for a key in the calculation_dict proplist
   PLRelease() the array upon use */
proplist_t my_pl_get_array (char * key_string);
/* opens a proplist and puts it in the opendict pointer. You need to specify
   a path string. it returns error codes defined in gmoiss.h */
int my_pl_open_call (proplist_t *opendict, char* _path);
/* finds the index of a string in a string array
   Defaults to zero if no string matches. Gives a g-warning
*/
   
int find_names_index (char** names, char * string  );
/* the same as above but returns -1 when the string does not
   match */
int find_names_index_plus (char ** names, char * string );
/* returns a proplist_t from a gsl_matrix_int, self descriptive */
proplist_t int_matrix_to_pl (gsl_matrix_int * m);
proplist_t float_matrix_to_pl( gsl_matrix_float *m);
/* the inverse operation of above */
gsl_matrix_int * my_pl_get_matrix_int(char * key_string);
gsl_matrix_float * my_pl_get_matrix_float(char * key_string);
#endif /* __PL_H__ */

