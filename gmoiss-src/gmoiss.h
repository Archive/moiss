/* This file is part of MOISS
   
   Copyright (C) 1998-99 Alan Aspuru Guzik
			 Raul Perusquia Flores
			 Carlos Amador Bedolla
			 Francisco Bustamante
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
               
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
                           
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
                                    
*/

#ifndef __GMOISS_H__
#define __GMOISS_H__


/* enums */

typedef enum 
{
	
	  OPEN_NOT_VALID=1,
	  OPEN_NOT_GMOISS=2,
	  OPEN_NOT_CORRECT_VERSION=3,
	  OPEN_OK=0
	  
} LoadReturnType;

typedef enum {
    IMPORTANCE_SAMPLING =0,
    MOVE_ELECTRONS = 1,
    GROWTH_ENERGY = 2,
    INTERPARTICLE_DISTANCE =3,
    ENERGY_PLOT=4,
    WAVEFUNCTION_PLOT=5,
    DUMP_WALKERS=6
} StatType;

typedef enum {
    EDU_SIM =0,
    BTF_SIM = 1,
    STO_SIM = 2,
    CLUSTER_SIM =3,

    PATH_SIM =4,
} SystemType;





typedef enum {
	MC_VARIATIONAL = 65, /* we use a big number to avoid StatType and MonteCarloType being confused by */
	MC_VARIATIONAL_FP = 66,   /* compare_stats_equal and causing the whole tree to disappear */
	MC_OPTIMIZATION = 67,
	MC_DIFFUSION = 68,
	MC_PURE_DIFFUSION = 69,
	MC_PATH = 70
} MonteCarloType;

typedef enum {
  NORMAL=0,
  EXCENTRIC
} MoleculeType;

typedef enum {
	HARMONIC_OSCILLATOR_POTENTIAL = 0,
	WELL_POTENTIAL = 1,
	HYDROGEN_POTENTIAL = 2,
	TUNNEL_POTENTIAL =3,
	BUMP_POTENTIAL = 4,
	HELIUM_POTENTIAL,
	VDW_POTENTIAL
	
} PotentialType;


typedef enum {
  GMOISS_VERSION,
  SYSTEM,
  MOLECULE,
  MOL_CHARGE,
  POTENTIAL ,
  TRIAL_FUNCTION ,
  DIMENSIONS ,
  PARTICLES ,
  WALKERS ,
  DIFFUSION_CONSTANT ,
  TIME_STEP ,
  MAXIMUM_BRANCHES ,
  DT_CONVERGENCE_FACTOR ,
  INITIAL_POSITION ,
  ATOMIC_COORDINATES,
  GAUSSIAN_SPREAD_RANGE ,
  GAUSSIAN_TRIAL_FUNCTION_ALPHA ,
  RANDOM_NUMBER_GENERATOR_SEED ,
  POTENTIAL_BOX_X ,
  POTENTIAL_BOX_Y ,
  POTENTIAL_BOX_Z ,
  TUNNEL_POTENTIAL_A ,
  TUNNEL_POTENTIAL_B ,
  TUNNEL_POTENTIAL_C ,
  CALCULATION,
  LEHNARD_JONES_SIGMA,
  LEHNARD_JONES_EPSILON,
  DIVS_PER_DIMENSION,
  GRAPH_MINIMUM  ,
  GRAPH_MAXIMUM,
  ELECTRONIC_REPULSION,
  SPIN_PARITY_OPERATOR,
  PARTICLE_CHARGES,
  PARTICLE_MASSES,
  CE_TERMS,
  ELEC_ELEC_TF,
  NUCLEI_ELEC_TF,
  SYM_TERMS,
  SYM_MATRIX,
  SYM_ELEC_ELEC,
  SYM_ELEC_NUCLEI,
  ELEC_NUCLEI_PARAMS,
  CE_PREFACTOR_TERMS,
  CE_PREFACTORS,
  PARAMETERS_PATH,
  OUTPUT_PATH,
  MOISS_PATH,
  JOB_NAME,
  O_REF_ENERGY,
  O_COST_FUN,
  O_WEIGHT_AVG,
  O_CLIP_ENERGY,
  O_MIN_EL,
  O_MAX_EL,
  O_MIN_W,
  O_MAX_W,
  O_TOLERANCE,
  O_SCALE,
  O_MAX_STEP
} DictionaryEntryType;

typedef enum {
	GAUSSIAN = 0,
	HYDROGEN = 1,
	HELIUM_KELLNER = 2,
	HELIUM_HILLERAS = 3,
	SLATER_TYPE_ORBITALS =4,
	CORRELATED_EXPONENTIAL =5

} TrialFunctionType;

typedef enum {
	ONED = 1,
	TWOD = 2,
	THREED = 3
} DimensionType;

typedef enum {
	GAUSSIAN_INITIAL = 0,
	FROM_COORDINATES =1,
	FROM_FILE = 2
} InitialPositionType;

typedef enum {
    ATOMIC_X =0,
    ATOMIC_Y =1,
    ATOMIC_Z =2,
    ATOMIC_NUMBER=3
} AtomicType;

typedef enum {
    CALCULATION_TYPE =0,
    BLOCKS=1,
    STEPS=2,
    STATISTICS=3
} CalcType;

typedef enum {
    ON=1,
    OFF=0
} BooleanType;

typedef enum {
    NUCLEI_CORRELATED_ELECTRON,
    NUCLEI_JASTROW

} NucleiTrialFunctionType;

typedef enum {
  ELECTRON_CORRELATED_EXPONENTIAL =0,
  ELECTRON_JASTROW =1,
  ELECTRON_GENERALIZED_JASTROW=2,
  NONE=3
    
} ElectronTrialFunctionType;

typedef enum {
    ALPHA,
    BETA
} SpinType;

/* structs */


typedef struct {
  StatType type;
  void (*function) ();
} Stat;

typedef struct {
	MonteCarloType type;
    int blocks;           /* blocks in which stats will be performed */
    int iterations;       /* iterations_per_block */
    GList *stats;         /*a list of all the Stats to perform  */
    GList *block_stats;  /*a list of the stats to be done in a block */
    float dt_step_value;
} MonteCarlo;

       

typedef struct {
	PotentialType potential_type;
	TrialFunctionType trial_function_type;
	DimensionType dimensions;
	int particles;
	int walkers;
	float D;
	float dt;
	int max_branches;
	float dt_convergence_factor;
	InitialPositionType initial_position;
	float gaussian_spread_range;
	float gaussian_tf_alpha;
	int   rng_seed;
	float box_x;
	float box_y;
	float box_z;
	float tunnel_a;
	float tunnel_b;
	float tunnel_h;
	
} ProblemData;


typedef struct {
	float dt_stepvalue;
} GMoissData;








/* Names */

extern char *tmolecule_names[];
extern char *stat_names[];

extern char *spin_names[];

extern char *mc_names[];
extern char *system_names[];
extern char *potential_names[];

extern char *trial_function_names[];

extern char *dimension_names[];

extern char *initial_position_names[];

extern char *dictionary_names[];

extern char * atomic_names [];

extern char * atomic_coordinates [];

extern char * calculation_names [];

extern char * electron_tf_names [];

extern char * nuclei_tf_names [];

extern char * boolean_names [];
/* Define sections */

#define NUMPROPS 20 /* number of properties to save in the dictionary */
#define INT_STRING_SIZE 45 /* allocation bytes for a char string */
#define FLOAT_STRING_SIZE 45 /* allocation bytes for a float string */
#define DICTIONARY_STRING_SIZE 500 /* Maximum string size for the dictionary */
#define TREE_SPACING 20
#define COL_SPACING 192
#define COL_SMALLSPACING 90
#define NORMAL_SPIN_SIZE 60
#define BIG_SPIN_SIZE 100
#define SMALL_SPIN_SIZE 40
#define MAX_PARTICLES 999
#define MAX_WALKERS 9999999
#define MAX_DIVS_PER_DIMENSION 9999
#define DEFAULT_DIVS_PER_DIMENSION 200
#define DT_DIGITS 9
#define DT_MIN 0.0000000000001
#define DT_MAX 1
#define MAX_BRANCHING 100
#define CONVERGENCE_DIGITS 6
#define DEFAULT_FILE "/tmp/moiss-input"
#define GMOISS_VERSION_NUMBER "0.5"
#define ATOMIC_COORDINATES_COLUMNS 6 /* the column to display in the atomic dlg */
#define ATOMIC_TABLE_HEIGHT 100   /* the height of the atomic dialog */
#define DEFAULT_ATOMIC_X 0.0      /* default coord for atomic dialog */
#define DEFAULT_ATOMIC_Y 0.0      /* default coord for atomic dialog */
#define DEFAULT_ATOMIC_Z 0.0      /* default coord for atomic dialog */
#define DEFAULT_ATOMIC_NUMBER 1.0 /* default atomic number for the same dlg */
#define DEFAULT_CE_TERMS 1        /* default correlated exponential tf number
				     of terms */
#define MAX_CE_TERMS 9999         /* Maximum number of correlated Exponential */
#define MAX_SYM_TERMS 9999        /* Correlated exponential sym dialog columns */
#define GREEK_FONT "-*-symbol-medium-*-normal-*-14-*-*-*-*-*-*-*"
extern proplist_t calculation_filename; /* calculation filename */
extern proplist_t calculation_dict;     /* main calculation dictionary */

#endif /* __GMOISS_H__ */

