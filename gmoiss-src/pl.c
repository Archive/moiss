#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gsl/gsl_matrix.h>
#include <stdlib.h>
#include <malloc.h>
#include <proplist.h>
#include <string.h>
#include <glib.h>
#include <stdio.h>
#include "gmoiss.h" 
 


/* Names */

char *stat_names[] = {
    "Importance Sampling",
    "Move one / all electrons",
    "Local Energy",
    "Interparticle distance",
    "Average Energy",
    "Wavefunction plot",
    "Write walkers",
    ""
};

char *mc_names[] = {
		"Variational",
		"Variational FP",
		"Variational Optimization",
		"Branching Diffusion",
		"Pure Diffusion",
		"Path Integral Monte Carlo",
		""
};

char *system_names[] = {
  "Educational",
  "Molecular (BTF)",
  "Molecular (STO)",
  "Clusers",
  "Path",
  ""
};

char *tmolecule_names[]={
  "Normal",
  "Excentric",
  ""
};




char *potential_names[] = {
    "Harmonic Oscillator",
    "Potential Well",
    "Hydrogen atom",
    "Tunnel",
    "Bump",
    "Helium",
    "Van der Waals cluster",
    ""
};

char *trial_function_names[] = {
	"Gaussian",
	"Hydrogen (Slater)",
	"Helium (Kellner)",
	"Helium (Eckart & Hilleraas)",
	"Slater Type Orbitals",
	"Corr. Exponential/Jastrow",
	""
};

char *dimension_names[] = {
	"1",
	"2",
	"3",
	""
};

char *initial_position_names[] = {
  "Random",
  "Coordinates",
  "File",
  ""
};

char *dictionary_names[] ={
  "GMoiss Version",
  "Simulation",
  "Molecule",
  "Molecule Charge",
  "Potential",
  "Trial Function",
  "Dimensions",
  "Particles",
  "Walkers",
  "Diffusion Constant",
  "Time step",
  "Maximum Branches",
  "dt convergence factor",
  "Initial Position",
  "Atomic Coordinates",
  "Gaussian Spread Range",
  "Gaussian Trial Function Alpha",
  "Random Number Generator Seed",
  "Potential Box X",
  "Potential Box Y",
  "Potential Box Z",
  "Tunnel Potential A",
  "Tunnel Potential B",
  "Tunnel Potential H",
  "Calculation",
  "Lehnard-Jones Sigma",
  "Lehnard-Jones Epsilon",
  "Graph: Divisions per Dimension",
  "Graph: Minimum",
  "Graph: Maximum",
  "Electronic Repulsion Scale",
  "Spin Parity Operator",
  "Particle Charges",
  "Particle Masses",
  "CE Number of Terms",
  "Electron-Electron TF",
  "Electron-Nuclei TF",
  "CE Sym: Number of Terms",
  "CE Sym: Matrix",
  "CE Electron-Electron Symmetry",
  "CE Electron-Nuclei Symmetry",
  "CE Electron-Nuclei Parameters",
  "CE Prefactor Terms",
  "CE Prefactors",
  "Parameters File Path",
  "Output Files Path",
  "Moiss Binary Path",
  "Job Name" ,
  "Reference Energy",
  "Cost Function",
  "Weight Averages",
  "Clip Energy ",
  "Min Eloc",
  "Max ELoc",
  "Min Weight",
  "Max Weight",
  "Tolerance",
  "OPt Scale",
  "Max opt iter",
   ""
  
};

char * atomic_coordinates[] = {
    "x",
    "y",
    "z",
    "Atomic Number",
    "" 
};

char * calculation_names[] = {
    "Calculation Type",
    "Blocks",
    "Steps",
    "Statistics",
    ""
};

char * atomic_names [] = {
  "Dummy Atom",
  "H",
  "He",
  "Li",
  "Be",
  "B",
  "C",
  "N",
  "O",
  "F",
  "Ne",
  "Na",
  "Mg",
  "Al",
  "Si",
  "P",
  "S",
  "Cl",
  "Ar",
  "K",
  "Ca",
  "Sc",
  "Ti",
  "V",
  "Cr",
  "Mn",
  "Fe",
  "Co",
  "Ni",
  "Cu",
  "Zn",
  "Ga",
  "Ge",
  "As",
  "Se",
  "Br",
  "Kr",
  "Rb",
  "Sr",
  "Y",
  "Zr",
  "Nb",
  "Mo",
  "Tc",
  "Ru",
  "Rh",
  "Pd",
  "Ag",
  "Cd",
  "In",
  "Sn",
  "Sb",
  "Te",
  "I",
  "Xe",
  "Cs",
  "Ba",
  "La",
  "Ce",
  "Pr",
  "Nd",
  "Pm",
  "Sm",
  "Eu",
  "Gd",
  "Tb",
  "Dy",
  "Ho",
  "Er",
  "Tm",
  "Yb",
  "Lu",
  "Hf",
  "Ta",
  "W",
  "Re",
  "Os",
  "Ir",
  "Pt",
  "Au",
  "Hg",
  "Tl",
  "Pb",
  "Bi",
  "Po",
  "At",
  "Rn",
  "Fr",
  "Ra",
  "Ac",
  "Th",
  "Pa",
  "U",
  "Np",
  "Pu",
  "Am",
  "Cm",
  "Bk",
  "Cf",
  "Es",
  "Fm",
  "Md",
  "No",
  "Lr",
  "Rf",
  "Db",
  "Sg",
  "Bh",
  "Hs",
  "Mt",
  "Uun",
  "Uuu",
  "Uub",
  NULL
};

char * nuclei_tf_names [] = {
    "Correlated Exponential",
    "Jastrow",
    ""
};

char * electron_tf_names [] = {
  "Correlated Exponential",
  "Jastrow",
  "Generalized Jastrow",
  "None",
  ""
};

char * spin_names [] = {
    "a",
    "b",
    ""
};

char * boolean_names [] = {
  "No",
  "Yes",
  ""
};

#define PL_INT_STRING_SIZE 45 /* allocation bytes for a char string */
#define PL_FLOAT_STRING_SIZE 45 /* allocation bytes for a float string */
#define PL_DICTIONARY_STRING_SIZE 500 /* Maximum string size for the dict */

proplist_t calculation_dict;
proplist_t calculation_filename;

proplist_t int_matrix_to_pl (gsl_matrix_int * m) {
  int i, j;
  proplist_t pl_row_matrix;
  proplist_t pl_column_matrix;
  proplist_t pl;
  char * text;
  
  pl_row_matrix = PLMakeArrayFromElements(NULL);
  // g_message("The matrix is of size %d , %d ", (int)  m->size1, (int)  m->size2);
  for (i = 0; i < (int) m->size1; i++)
	{
	  pl_column_matrix = PLMakeArrayFromElements(NULL);
	  PLAppendArrayElement(pl_row_matrix, pl_column_matrix);
	  for (j = 0; j < (int) m->size2; j++)
		{
		  //printf("eech: %d %d\n", i, j);
		  text = g_malloc(PL_INT_STRING_SIZE);
		  snprintf(text, PL_INT_STRING_SIZE, "%d",gsl_matrix_int_get(m, i, j));
		  pl = PLMakeString(text);
		  PLAppendArrayElement(pl_column_matrix, pl);
		  g_free(text);
		}
	}
  return pl_row_matrix;
}
proplist_t float_matrix_to_pl (gsl_matrix_float * m) {
  int i, j;
  proplist_t pl_row_matrix;
  proplist_t pl_column_matrix;
  proplist_t pl;
  char * text;
  
  pl_row_matrix = PLMakeArrayFromElements(NULL);
  //g_message("The matrix is of size %d , %d ", (int) m->size1, (int)  m->size2);
  for (i = 0; i < (int) m->size1; i++)
	{
	  pl_column_matrix = PLMakeArrayFromElements(NULL);
	  PLAppendArrayElement(pl_row_matrix, pl_column_matrix);
	  for (j = 0; j < (int) m->size2; j++)
		{
		  
		  text = g_malloc(PL_INT_STRING_SIZE);
		  snprintf(text, PL_INT_STRING_SIZE, "%f",gsl_matrix_float_get(m, i, j));
		  pl = PLMakeString(text);
		  PLAppendArrayElement(pl_column_matrix, pl);
		  g_free(text);
		}
	}
  return pl_row_matrix;
}


proplist_t double_array_to_pl( int size, double * array) {
    proplist_t pl_array, pl;
    int i;
    char * text;

    pl_array = PLMakeArrayFromElements(NULL);
    for (i = 0; i < size; i++)
	{
	    text = g_malloc(PL_FLOAT_STRING_SIZE);
	    snprintf(text, PL_FLOAT_STRING_SIZE, "%f", array[i]);
	    pl = PLMakeString(text);
	    PLAppendArrayElement(pl_array, pl);
	    g_free(text);
	}
    
    return pl_array;
}

double * pl_array_to_double(proplist_t pl) {
  double * array;
    int elements, i;
    char * text;
    
    g_assert(PLIsArray(pl));
    elements = PLGetNumberOfElements(pl);
    array = g_new(double, elements);
    for (i = 0; i < elements; i++) {
	  text = PLGetString(PLGetArrayElement(pl, i));
	array[i] = atof(text);
	g_free(text);
    }
	return array;
}

int find_names_index (char** names, char * string  )
{  
	int i;
	i = -1;
	for (i = 0; *names[i]; i++)
		{
		    if (strcmp(names[i], string) == 0) {
			return i;}
		}
	g_warning("Index for %s not found, so I assumed %s; Maybe an input file has typos.", string, names[0]);
	return 0;
}

int find_names_index_plus (char ** names, char * string )
{  
	int i;
	for (i = 0; *names[i]; i++)
		{
			if (strcmp(names[i], string) == 0) return i;
		}
	return -1;
}


void pl_fatal_error (char *error_message)
{
  printf ("Fatal error!\n");
  printf (error_message);
  exit (1);
}



void my_pl_add_string( char * key_string, char * key_value) {
  proplist_t key, value;
  
   key = PLMakeString(key_string);
   value = PLMakeString(key_value);
   PLInsertDictionaryEntry(calculation_dict , key, value);
   PLRelease(key);
   PLRelease(value);
} 
  
gsl_matrix_int * my_pl_get_matrix_int(char * key_string) {
  gsl_matrix_int * matrix;
  proplist_t key, value, row, col;
  char * my_string;
  unsigned int i, j, rows, cols;

  my_string = (char *) malloc (PL_DICTIONARY_STRING_SIZE);

  if (my_string == NULL)
	g_error("No memory left!");
  
  key = PLMakeString(key_string);
  value = PLGetDictionaryEntry(calculation_dict, key);
  if (value == NULL) {
	g_error("Couldn't find %s in the proplist. Probably you edited a gmoiss-input wrong or gmoiss-input is missing.", key_string);
  }
  
  /* quick initialization of the size of the matrix */
  /* FIXME: we have to check if the thing is indeed an array */
  

  rows = PLGetNumberOfElements(value);
  row = PLGetArrayElement(value, 0);
  cols = PLGetNumberOfElements(row);
  
  matrix = gsl_matrix_int_calloc(rows, cols);
  
  /* now we build the matrix */

  for (i = 0; i < rows; i++) {
	row = PLGetArrayElement(value, i);
	for (j = 0; j < cols; j++) {
	  col = PLGetArrayElement(row, j);
	  gsl_matrix_int_set(matrix, i, j, atoi(PLGetString(col)));
	 
	} 
  }
 
  return matrix;
}

gsl_matrix_float * my_pl_get_matrix_float(char * key_string) {
  gsl_matrix_float * matrix;
  proplist_t key, value, row, col;
  char * my_string;
  unsigned int i, j, rows, cols;

  my_string = (char *) malloc (PL_DICTIONARY_STRING_SIZE);

  if (my_string == NULL)
	g_error("No memory left!");
  
  key = PLMakeString(key_string);
  value = PLGetDictionaryEntry(calculation_dict, key);
  if (value == NULL) {
	g_error("Couldn't find %s in the proplist. Probably you edited a gmoiss-input wrong or gmoiss-input is missing.", key_string);
  }
  
  /* quick initialization of the size of the matrix */
  /* FIXME: we have to check if the thing is indeed an array */
  

  rows = PLGetNumberOfElements(value);
  row = PLGetArrayElement(value, 0);
  cols = PLGetNumberOfElements(row);
  
  matrix = gsl_matrix_float_calloc(rows, cols);
  
  /* now we build the matrix */

  for (i = 0; i < rows; i++) {
	row = PLGetArrayElement(value, i);
	for (j = 0; j < cols; j++) {
	  col = PLGetArrayElement(row, j);
	  gsl_matrix_float_set(matrix, i, j, atof(PLGetString(col)));
	} 
  }

  return matrix;
}


char * my_pl_get_string( char * key_string) {
   
   char * my_string;
   proplist_t pl, pl1;
   proplist_t pl2;
   
   /* malloc for the string, it's needed b/c proplist returns a reference
    and not a copy of the original string (see libPropList's readme) */
   my_string = (char *) malloc(PL_DICTIONARY_STRING_SIZE);
   if (my_string == NULL)
     g_error("No memory left!");
   
   pl = PLMakeString(key_string);
   pl1 = PLGetDictionaryEntry(calculation_dict, pl);
   if (pl1 == NULL) {
      g_error("Couldn't find %s in the proplist. Probably you edited a gmoiss-input wrong or gmoiss-input is missing.", key_string);
  }
   pl2 = PLGetString(pl1);

   
   
   my_string = strcpy(my_string,pl2);
   
   PLRelease(pl);
   return my_string;
 }

 void my_pl_add_int(char * key_string, int value)
 {
   char * my_string;
   my_string = (char *) malloc(PL_INT_STRING_SIZE);
   if (my_string == NULL)
     pl_fatal_error ("No memory for my_string!\n");

   snprintf(my_string, PL_INT_STRING_SIZE  ,  "%d", value);
   my_pl_add_string(key_string, my_string);
   free(my_string);
 }

 void my_pl_add_float(char * key_string, float value)
 {
   char * my_string;
   my_string = (char *) malloc(PL_FLOAT_STRING_SIZE);
   if (my_string == NULL)
     pl_fatal_error ("No memory for my_string!\n");
   
   snprintf(my_string, PL_FLOAT_STRING_SIZE , "%f", value);
   my_pl_add_string(key_string, my_string);
    free(my_string);
 }

 int my_pl_get_int(char * key_string){
   int i;
   char * my_string;
   my_string = my_pl_get_string(key_string); 
   i = atoi (my_string);
   free(my_string);
   return i;
 }

float my_pl_get_float(char * key_string) {
  float f;
  char * my_string;
  my_string = my_pl_get_string(key_string);
  f = atof ( my_string);
  free(my_string);
  return f;
 }

/* caution, free the returned array, it's a copy of the original array */

proplist_t my_pl_get_array (char * key_string) {
  proplist_t array, pl;
  char * my_string;
  my_string = malloc(PL_DICTIONARY_STRING_SIZE);
  if (my_string == NULL)
    pl_fatal_error("No memory for my_string!\n");
  
  pl = PLMakeString(key_string);
  array= PLDeepCopy(PLGetDictionaryEntry(calculation_dict, pl));
  PLRelease(pl);
  free(my_string);
  return array;
}

int my_pl_open_call (proplist_t  *opendict, char* _path) {

  opendict = malloc(sizeof(proplist_t));
  
  *opendict = PLGetProplistWithPath( _path);
  
  
  if (*opendict == NULL || !PLIsDictionary(*opendict)) return OPEN_NOT_VALID;
  else {
    proplist_t key, value;
    key = PLMakeString(dictionary_names[GMOISS_VERSION]);
    value = PLGetDictionaryEntry(*opendict, key);
/*     PLRelease(key); */
    if (value == NULL)
      {
	return OPEN_NOT_GMOISS;
      }
    else if (strcasecmp(GMOISS_VERSION_NUMBER, PLGetString(value)) == 0) 
      {
	

#if 0	  
	  charalito = PLGetDescriptionIndent(*opendict, 4);
	  printf("File loaded in pl.c: \n %s ", charalito);
/* 	  free(charalito); */
	     
/*  	  *opendict = PLDeepCopy(dict);  */

	  charalito = PLGetDescriptionIndent(*opendict, 4);
	  printf("Opendict es: \n %s ", charalito);
/* 	  free(charalito); */

	  /* debug 
	     charalito = PLGetDescription(opendict);
	     printf("File loaded: \n %s ", charalito);
	     free(charalito);
	  debug */
#endif
	  if (!opendict) printf("que pedo...\n");
	return OPEN_OK;
      }
    else {
      return OPEN_NOT_CORRECT_VERSION;
	}
  }
}
