# Note that this is NOT a relocatable package
%define ver      0.1
%define rel      SNAP
%define prefix   /opt/moiss

Summary:   Molecular Integration and Sampling Software
Name:      moiss
Version:   %ver
Release:   %rel
Copyright: GPL
Group:     Scientific
Source0:   moiss.tar.gz
URL:       http://moiss.pquim.unam.mx/moiss/
BuildRoot: /tmp/moiss-%{PACKAGE_VERSION}-root
Packager: Alan Aspuru and Daniel Manrique  <aspuru@eros.pquim.unam.mx>
# Requires: gtk+ >= 1.1.1
# Requires: gnome-libs
Docdir: %{prefix}/doc

%description
This is a n-particle n-dimensions Diffusion Monte Carlo (DMC) Schroedinger
equation integrator. It is very easy to implement new potentials and theory 
levels, because modularity is one of our highest priorities.  

Our goal is to provide a freely available general purpose Quantum Monte Carlo 
(QMC) program released under the GNU/GPL license.  
		        
						       
%prep
%setup

%build
CFLAGS="$RPM_OPT_FLAGS -static" LDFLAGS="-s -static" ./autogen.sh \
	--prefix=%{prefix} 

if [ "$SMP" != "" ]; then
  (make "MAKE=make -k -j $SMP"; exit 0)
  make
else
  make
fi

%install
rm -rf $RPM_BUILD_ROOT
#install -d $RPM_BUILD_ROOT/etc/{rc.d/init.d,pam.d,profile.d,X11/wmconfig}

make prefix=$RPM_BUILD_ROOT%{prefix} install

cp -R $RPM_BUILD_ROOT/input-files %{prefix}/input-files

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%doc README COPYING ChangeLog NEWS TODO AUTHORS ABOUT-NLS IDEAS INSTALL
%{prefix}/bin/moiss
%{prefix}/bin/gmoiss
%{prefix}/input-files/*

%changelog
* Thu Oct 15 1998 Alan Aspuru <aspuru@eros.pquim.unam.mx>

- First RPM built ever


