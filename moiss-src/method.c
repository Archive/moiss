#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gsl/gsl_rng.h>
#include <math.h>
#include "moiss.h"
#include "potential.h"
#include "method.h"
#include "gui.h"
#include "init.h"
#include "bto.h"

/*
The following code is obsolete

*********************************************************
ATENCION codigo obsoleto
*********************************************************
method.c esta a p�nto de desaparecer
se mantiene aqui temporalmente solo por razones nostalgicas
 */
/*
void G_branch_DMC(walker * w)
{
    GB = exp(-(spotential - e->ref_energy) * teff);

    num_branches = (GB + gsl_rng_uniform(rng));
}

void G_branch_bessel(walker * w)
{
    potential = (*E_T) (w);
    GB = (potential - Vmax_bessel) / (e->ref_energy - Vmax_bessel);
    num_branches = (GB + gsl_rng_uniform(rng));
}

void G_diff_all(walker * w)
{
    register int i, j;
    for (i = 0; i < num_pars; i++)
	for (j = 0; j < dimensions; j++)
	    w->pars[i].R[j] += normal_dist() * diff_sigma;
    potential = (*E_T) (w);
    spotential = 0.5 * (potential + oldpotential);

}

void G_diff_all_IS(walker * w)
{
    register int i, j;
    (*Force) (w);
    for (i = 0; i < num_pars; i++)
	for (j = 0; j < dimensions; j++)
	    w->pars[i].R[j] += normal_dist() * diff_sigma + w->pars[i].Force[j] * Ddt;
    potential = E_T(w);
    spotential = 0.5 * (potential + oldpotential);
}
*/
/*****************************************************/
/* esta es la funcion   que no sirve  ************** */


void G_diff_all_IS_Metro_1(walker * w)
{
    register int i, j, k;
    double q, halfDdt, invnum_pars;
    double desp, dr, dsq = 0, dsqa = 0;
    halfDdt = Ddt / 2.0;
    invnum_pars = 1.0 / num_pars;

    for (i = 0; i < num_pars; i++) {
	(*PsiT) (w);
	flush_walker(w);
	dr = 0;
	for (j = 0; j < dimensions; j++) {
	    desp = normal_dist() * diff_sigma +
		w->pars[i].pastForce[j] * Ddt;

	    w->pars[i].R[j] += desp;
	    dr += desp * desp;
	}
	dsq += dr;
	(*Force) (w);

	q = 0;
	for (k = 0; k < num_pars; k++)
	    for (j = 0; j < dimensions; j++) {
		q += 0.5 * (w->pars[k].pastForce[j] + w->pars[k].Force[j]) *
		    (D * dt / 2
		     * (w->pars[k].pastForce[j] - w->pars[k].Force[j])
		     - (w->pars[k].R[j] - w->pars[k].pastR[j]));
	    }
	P_Metropolis = min(exp(q) * (w->Psi * w->Psi)
			   / (w->pastPsi * w->pastPsi), 1.0);
	if (P_Metropolis > gsl_rng_uniform(rng)) {
	    /*accept+=invnum_pars; */
	    dsqa += dr;
	} else
	    reincarn_particle(w, i);
    }


    potential = (*E_T) (w);
    spotential = 0.5 * (potential + oldpotential);
    /*teff=dt*accept; */
    teff = dt * dsqa / dsq;
}


/*****************************************************/
/* esta es la funcion   que no sirve  ************** */

void G_diff_all_IS_Metro(walker * w)
{
    register int i, j, k = 0;
    int vstp, Nw;
    double vDdt, vsgm;


    double vEl, bkvEl, Accept = 0, qado_avg_bkvEl = 0, A, vnew;
    double Q, fqf[ncoord], temp1, vvnew[ncoord], del2, grad[ncoord];
    int vblk;
    double Evar_mean, Evar_sigma;
    double avg_bkvEl, avg_wEL, psi1, psi2;
    double engy1;
    temp1 = D * dt / 2.0;
    vEl = 0;
    /* FIXME: fill_walkers() should work only with Bressanini's get_del */
    /*  fill_walkers(); */

    vDdt = dt * D;
    vsgm = sqrt(2 * D * dt);
    avg_bkvEl = 0;


    psi1 = walkers[Nw].Psi;
    engy1 = walkers[Nw].Egy_Lcl;
    for (i = 0, k = 0; i < num_pars; i++)
	for (j = 0; j < dimensions; j++, k++) {
	    vvnew[k] =
		normal_dist() * vsgm
		+ w->pars[i].R[j]
		+ w->pars[i].Force[j] * Ddt;

	}
    /* FIXME see bto.h and fix accordingly */
	vnew = pot(vvnew);
    get_del(vvnew, &psi2, &del2, grad);
    for (k = 0; k < ncoord; k++)
	fqf[k] = 2 * grad[k] / psi2;
    Q = 0.0;
    for (i = 0, k = 0; i < num_pars; i++)
	for (j = 0; j < dimensions; j++, k++)
	    Q += (fqf[k] + w->pars[i].Force[j]) *
		(temp1 * (-fqf[k] + w->pars[i].Force[j]) -
		 (-w->pars[i].R[j] + vvnew[k]));
    A = ((psi2 * psi2) / (w->Psi * w->Psi)) * exp(0.5 * Q);
    if (A > gsl_rng_uniform(rng)) {
	Accept++;
	for (i = 0, k = 0; i < num_pars; i++)
	    for (j = 0; j < dimensions; j++, k++) {
		w->pars[i].Force[j] = fqf[k];
		w->pars[i].R[j] = vvnew[k];
	    }
	w->Psi = psi2;
	w->Egy_Lcl = -0.5 * del2 / psi2 + vnew;
	teff++;
    }
    // printf("Eng= %f \n",w->Egy_Lcl);
    /* effective time step is time step times ratio of average
       accepted displacements over average of all displacements
       (Umrigar, Nightingale and Runge (1993)) */


}
void G_diff_all_IS_Metro_2(walker * w)
{
    int i, j, numa = 0;
    double accept, Q, A, temp1;
    double v1[3], v2[3], psi_1, psi_2, E_local_1, E_local_2;
    double Y[3];
    double *force_1;
    double *force_2;
    double desp, dr, dsq = 0, dsqa = 0;

    force_1 = v1;
    force_2 = v2;

    temp1 = D * dt / 2.0;
    accept = 0.0;


    for (i = 0; i < num_pars; i++) {
	dr = 0;

	(*Trialfun) (w, i, &psi_1, force_1, &E_local_1);
	for (j = 0; j < dimensions; j++) {
	    Y[j] = w->pars[i].R[j];
	    desp = normal_dist() * diff_sigma + force_1[j] * Ddt;
	    /*printf("ojo     %f %f %f %d   \n",dsq,desp,force_1[j],num_pars); */
	    w->pars[i].R[j] += desp;
	    dr += P2(desp);
	}
	(*Trialfun) (w, i, &psi_2, force_2, &E_local_2);
	/* sum of all displacements squared */
	dsq += dr;

	Q = 0.0;
	for (j = 0; j < dimensions; j++)
	    Q += 0.5 * (force_1[j] + force_2[j]) *
		((temp1 * (force_1[j] - force_2[j])) - (-Y[j] + w->pars[i].R[j]));

	A = min(1.0, P2(psi_2 / psi_1) * exp(Q));

	if (A > gsl_rng_uniform(rng)) {
	    /*accept+=INV(num_pars); */
	    numa++;
	    dsqa += dr;
	} else
	    for (j = 0; j < dimensions; j++)
		w->pars[i].R[j] = Y[j];
    }
    potential = (*E_T) (w);

    spotential = 0.5 * (potential + oldpotential);

    /* effective time step is time step times ratio of average
       accepted displacements over average of all displacements
       (Umrigar, Nightingale and Runge (1993)) */
    teff = dt * dsqa * num_pars / (dsq * numa);
    /*  printf("      %f %f %f    \n",dsq,dsqa,teff); */
    /*teff=dt*accept; */

    /*spotential=0.5*(E_local_2+E_local_1); */

    /*  printf("%f \n",spotential); */


}

void inv_matrix(double **A, int ncol, double *d, double **y)
{

    int i, ii, m, imax = 0, j, k, ip;
    double big, dum, sum = 0, temp;
    double vv[ncol];
    double B[ncol];
    double *b;
    double *v;
    int *indx;
    int ind[ncol];

    b = B;
    v = vv;
    indx = ind;


    *d = 1.0;
    for (i = 0; i < ncol; i++) {
	big = 0.0;
	for (j = 0; j < ncol; j++)
	    if ((temp = abs(A[i][j])) > big)
		big = temp;
	if (big == 0)
	    *(v + i) = 1.0 / big;
    }

    for (j = 0; j < ncol; j++) {
	for (i = 0; i < j; i++) {
	    sum = A[i][j];
	    for (k = 0; k < i; k++)
		sum -= A[i][k] * A[k][j];
	    A[i][j] = sum;
	}
	big = 0.0;
	for (i = j; i < ncol; i++) {
	    sum = A[i][j];
	    for (k = 0; k < j; k++)
		sum -= A[i][k] * A[k][j];
	    A[i][j] = sum;
	    if ((dum = (*(v + i)) * abs(sum)) >= big) {
		big = dum;
		imax = i;
	    }
	}
	if (j != imax) {
	    for (k = 0; k < ncol; k++) {
		dum = A[imax][k];
		A[imax][k] = A[j][k];
		A[j][k] = dum;
	    }
	    *d = -(*d);
	    *(v + imax) = *(v + j);

	}
	*(indx + j) = imax;
	/*if (A[ndet][j][j]==0) A[ndet][j][j]=1.0e-20; */
	if (j != ncol - 1) {
	    dum = 1.0 / A[j][j];
	    for (i = j + 1; i < ncol; i++)
		A[i][j] *= dum;
	}
    }
    for (m = 0; m < ncol; m++) {
	for (i = 0; i < ncol; i++)
	    *(b + i) = 0.0;
	*(b + m) = 1.0;

	ii = 0;
	for (i = 0; i < ncol; i++) {
	    ip = *(indx + i);
	    sum = B[ip];
	    B[ip] = B[i];
	    if (ii)
		for (j = ii - 1; j <= i; j++)
		    sum -= A[i][j] * (*(b + j));
	    else if (sum)
		ii = i + 1;
	    B[i] = sum;
	}
	for (i = ncol - 1; i >= 0; i--) {
	    sum = *(b + i);
	    for (j = i + 1; j < ncol; j++)
		sum -= A[i][j] * (*(b + j));
	    *(b + i) = sum / A[i][i];
	}
	for (i = 0; i < ncol; i++)
	    y[i][m] = B[i];

    }
}
void G_diff_bessel(walker * w)
{
    int i, j;
    double Ylength = 0, u = 1.0, vexp;
    double Y[num_pars][dimensions];
    double v, r, A;

    vexp = (double) (dimensions * num_pars - 1.0);
    flush_walker(w);
    for (i = 0; i < num_pars; i++)
	for (j = 0; j < dimensions; j++) {
	    Y[i][j] = normal_dist() * diff_sigma;
	    Ylength += Y[i][j] * Y[i][j];
	    u = u * gsl_rng_uniform(rng);
	}
    Ylength = sqrt(Ylength);
    v = sqrt(1.0 - (pow(gsl_rng_uniform(rng), vexp)));
    r = -log(u) * v;
    for (i = 0; i < num_pars; i++)
	for (j = 0; j < dimensions; j++)
	    w->pars[i].R[j] += Y[i][j] * r / Ylength;
    (*PsiT) (w);
    A = min((w->Psi / w->pastPsi), 1.0);
    if (A < gsl_rng_uniform(rng))
	reincarn_particle(w, 1);
}
