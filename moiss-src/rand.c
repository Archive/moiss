/* MOISS (C) Under the GPL. Read *.h for legal info */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glib.h>
#include <stdio.h>
#include "math.h"
#include "rand.h"
#include "moiss.h"

double exp_dist(void)
{
    int i;
    double U, Umin, Ustar, K = 0;

    double *q1 = vector1;

    U = gsl_rng_uniform(rng);
    U += U;
    while (U < 1) {
	U += U;
	K += *q1;
    }
    U -= 1.0;
    if (U < *q1)
	return K + U;
    else {
	i = 1;

	Umin = gsl_rng_uniform(rng);
	do {
	    Ustar = gsl_rng_uniform(rng);
	    if (Ustar < Umin)
		Umin = Ustar;
	    i++;
	} while (U > *(vector1 + i - 1));
	return K + Umin ** q1;
    }
}



double cauchy_dist(void)
  {
    double U, UU, T, S, Z;

    U = gsl_rng_uniform(rng);
    T = U - 0.5;
    S = W_cauchy - T * T;
    if (S > 0)
	return T * (A_cauchy / S + B_cauchy);
    else
	do {
	    UU = gsl_rng_uniform(rng);
	    T = U - 0.5;
	    S = 0.25 - T * T;
	    Z = T * (a_cauchy / S + b_cauchy);
	} while ((S * S * ((1.0 + Z * Z) * (H_cauchy * UU + P_cauchy) - q_cauchy)) <= 0.5);
    return Z;
}




double normal_dist(void)
{

  int B;
  double U, S, W, Z, V;
  
  F_normal = -F_normal;
  if (F_normal > 0)
    {
      
      return Y_normal;
      
    }   
  else {
    U = gsl_rng_uniform(rng);
    if (U < 0.5)
      B = 0;
    else
      B = 1;
    /*V=exp_dist(); */
    V = -log(gsl_rng_uniform(rng));
    S = V + V;
    W = cauchy_dist();
    Z = sqrt(S / (1 + W * W));
    Y_normal = W * Z;
    
	
    if (B == 0)
      return (Z);
    else
      return (-Z);
  }
  

}

