#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gsl/gsl_rng.h>
#include <stdio.h>
#include <math.h>
#include <glib.h>
#include "moiss.h"
#include "output.h"
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include "bto.h"
#include <errno.h>
#include <string.h>
#include "proto.h"
#include "bto.h"
#include "init.h"
#include "rand.h"

#include "stats.h"

void variational_M(ensemble * e, GList * stats, int num_steps, double *estep, double *e2step)
{
  
  int i;
  int vstp, Nw;
  double dsigma;
    double  A;
    double del1;
    double psi1;
    double vnew;
    double vvold[ncoord];
    double iestep,ie2step;
    int bol_wf = 0; /* do normalize?*/
    bol_wf = stats_check_wavef(stats);
    dsigma=sqrt(dt);
    del1=0.0;
    
    for (vstp = 0; vstp < num_steps; vstp++) 
      {
	iestep=ie2step=0;
	for (Nw = 0; Nw < e->num_walkers; Nw++) 
	  {
	    psi1=e->walkers[Nw].Psi;
	    for (i = 0; i < ncoord; i++) 
	      {
		vvold[i] = e->walkers[Nw].R[i];
		e->walkers[Nw].R[i]+= normal_dist() *dsigma;
	      }
         
	    vnew = (*V2)(&e->walkers[Nw]);  
	    
	    (*Derivs)(&e->walkers[Nw]);
	    A =(e->walkers[Nw].Psi * e->walkers[Nw].Psi)/(psi1 * psi1);
	    if(A>1.0)
	      A=1.0;
	    e->Paccept+=A;
	    if (A >gsl_rng_uniform(rng)) 

	      {
		
		e->walkers[Nw].Egy_Lcl = 
		  -0.5 * e->walkers[Nw].del2 / e->walkers[Nw].Psi + vnew;
		
	      }
	    else
	      {
		for (i = 0; i < ncoord; i++) 
		  e->walkers[Nw].R[i] = vvold[i];
		e->walkers[Nw].Psi = psi1;
	     
	      }
	    //     printf("%f %d  \n",e->walkers[Nw].Egy_Lcl,Nw);
	    iestep+=  e->walkers[Nw].Egy_Lcl;
	    ie2step+= e->walkers[Nw].Egy_Lcl* e->walkers[Nw].Egy_Lcl;
	 	  
	  }
	iestep /=(double)e->num_walkers;
	ie2step /=(double)e->num_walkers;
	
	*estep+=iestep;
	*e2step+=ie2step;
	if(bol_wf)
	  stats_histogram(e);
      }

    *estep /=(double)num_steps;
    *e2step /=(double)num_steps;

    
  
}

void variational_FPF(ensemble * e, GList * stats, int num_steps,double *estep, double *e2step)
{
    /*  Variational Monte Carlo based in the Fokker-Planck formalism */


    register int i;
    int vstp, Nw;
    double A, vnew;
    double Q, oldfqf[ncoord], temp1, vvold[ncoord];
    double psi1,dsigma;
    double engy1;
    double iestep,ie2step;
     int bol_wf = 0; /* do normalize?*/
    bol_wf = stats_check_wavef(stats);
    temp1 = 0.25*dt ;
    dsigma=sqrt(dt);

    for (vstp = 0; vstp < num_steps; vstp++) {
      iestep=ie2step=0;
      for (Nw = 0; Nw < e->num_walkers; Nw++) {
	
	
	psi1 = e->walkers[Nw].Psi;
	engy1 = e->walkers[Nw].Egy_Lcl;
	for (i = 0; i < ncoord; i++) 
	  {
	    oldfqf[i]=e->walkers[Nw].Force[i];
	    vvold[i]=e->walkers[Nw].R[i];
	    e->walkers[Nw].R[i] += normal_dist()* dsigma  + e->walkers[Nw].Force[i] * Ddt;
	   

	    // printf("%f %f %f \n", oldfqf[i],vvold[i],e->walkers[Nw].R[i] );
	   
	  } //exit(0);
	vnew = (*V2)(&e->walkers[Nw]);
	(*Derivs)(&e->walkers[Nw]);
	for (i = 0; i < ncoord; i++)
	e->walkers[Nw].Force[i]   = 
	  2 * e->walkers[Nw].grad[i] / e->walkers[Nw].Psi;
	Q = 0.0;
	for (i = 0; i < ncoord; i++)
	  
	  Q += (oldfqf[i] + e->walkers[Nw].Force[i]) *
	    (temp1 * (oldfqf[i] - e->walkers[Nw].Force[i]) -
	     ((e->walkers[Nw].R[i]) - vvold[i]));
	
	
	A = ((e->walkers[Nw].Psi * e->walkers[Nw].Psi) / 
	     (psi1 * psi1)) *exp(0.5 * Q);
	
	//	printf("%d %f %f %f %f\n" ,Nw,e->walkers[Nw].Psi,psi1,Q,A);
	


	   if(A>1.0)
	      A=1.0;
	    e->Paccept+=A;
	
	    if (A >=gsl_rng_uniform(rng)) 
	  {
	    
	    e->walkers[Nw].Egy_Lcl = -0.5 * e->walkers[Nw].del2 / e->walkers[Nw].Psi + vnew;
	  }
	else
	  {
	    for (i = 0; i < ncoord ; i++) 
	      {
		  e->walkers[Nw].R[i] = vvold[i];
		  e->walkers[Nw].Force[i] = oldfqf[i];
	      }
	    e->walkers[Nw].Psi = psi1;
	  

	  }
	    //  printf("%d %f %f  %f %f %f \n",
	    //  Nw,e->walkers[Nw].Egy_Lcl,e->walkers[Nw].Psi, A,e->walkers[Nw].Force[5] ,e->walkers[Nw].R[5]);
	iestep+=  e->walkers[Nw].Egy_Lcl ;
	ie2step+= e->walkers[Nw].Egy_Lcl* e->walkers[Nw].Egy_Lcl;
	
	
      }//printf("+++ %d %f %d  \n",vstp,iestep,e->num_walkers);
      
  iestep /=(double)e->num_walkers;
  ie2step /=(double)e->num_walkers;
  // printf("+++ %d %f %d  \n",vstp,iestep,e->num_walkers);
  *estep+=iestep;
  *e2step+=ie2step;
  
  if(bol_wf)
    stats_histogram(e);
    }

    *estep /=(double)num_steps;
    *e2step /=(double)num_steps;


}




