/*
   ** misc.c - miscellaneus functions that do not belong to anywhere else
   **
   **   (C) Copyright Dario Bressanini
   **   Dipartimento di Scienze Matematiche Fisiche e Chimiche 
   **   Universita' dell'Insubria, sede di Como
   **   E-mail: dario@fis.unico.it
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <math.h>

#define MAXLEN          512	/* max lenght of lines or words */
#define LABEL	'A'
#define NUMBER	'0'


/*
   ** NAME
   **      FILE *efopen(file, mode)
   **
   ** DESCRIPTION
   **      Try to open a file; if an error occour, print a message end exit. 
   **
   ** RETURN
   **      a valid FILE pointer.
 */

FILE *efopen(char *file, char *mode)
{
    FILE *fp;
    char *alert = "*** ERROR: efopen: can't open file %s mode %s";

    if ((fp = fopen(file, mode)) != NULL)
	return fp;
    printf(alert, file, mode);
    printf(": %s\n", strerror(errno));
    exit(1);
}


/*
   ** NAME
   **      void *emalloc(size_t amount)
   **
   ** DESCRIPTION
   **      Try to allocate the requested memory amount. If fails complain
   **      and exit.
   **
   ** RETURN
   **      a valid pointer.
 */

void *emalloc(size_t amount)
{
    void *ptr;
    char *alert = "*** ERROR: emalloc: can't allocate %lu words";

    if ((ptr = malloc(amount)) != NULL)
	return ptr;
    printf(alert, amount);
    printf(": %s\n", strerror(errno));
    exit(1);
}


void error(char *message)
{
    printf("*** ERROR: %s\n", message);
    exit(1);
}


/* return the dot product of two vectors */

double dot(double a[], double b[], int n)
{
    int i;
    double s = 0;

    for (i = 0; i < n; i++)
	s += a[i] * b[i];

    return s;
}


/* x^n  n integer */

double ipow(double x, int n)
{
    register double s;

    s = 1.0;
    while (n-- > 0)
	s *= x;
    return s;
}

/*--------------------------------------------------------------------------*/

/*
   ** DESCRIPTION
   **      Works like strcmp, but is case insensitive;
   **      return 0 : s == t, <0 if s<t  and  >0 if s>t.
 */

int stracmp(const char *s, const char *t)
{
    for (; toupper(*s) == toupper(*t); s++, t++)
	if (!*s)
	    return 0;
    return (toupper(*s) - toupper(*t));
}

/*--------------------------------------------------------------------------*/

/*
   ** NAME
   **      int fget_word(char *word, int lim, FILE *fp)
   **
   ** DESCRIPTION
   **      Read the next word in input from file fp up to 'lim-1' character.
   **      A word is:
   **              LABEL:  [a-zA-Z][a-zA-Z0-9_]*
   **              NUMBER: [0-9][0-9]*  any valid integer number or
   **                      any decimal number (not exponential)
   **              any other character (including newlines).
   **              spaces and tabs don't count.
   **      return the type of word.
   **      The caller must allocate  space for lim characters, included '\0'
 */

int fget_word(char *word, int lim, FILE * fp)
{
    int c;
    char *w = word;

    while ((c = getc(fp)) == ' ' || c == '\t');

    if (c == EOF) {
	*w = '\0';
	return EOF;
    } else if (isalpha(c)) {
	do {
	    *w++ = c;
	    c = getc(fp);
	} while (((w - word + 1 < lim) && isalnum(c)) || c == '_');
	ungetc(c, fp);		/* put back char into stream */
	*w = '\0';
	return LABEL;
    } else if (isdigit(c)) {
	do {
	    *w++ = c;
	    c = getc(fp);
	} while ((w - word + 1) < lim && isdigit(c));
	ungetc(c, fp);		/* put back char into stream */
	*w = '\0';
	return NUMBER;
    } else {
	*w++ = c;
	*w = '\0';
	return c;
    }
}

int get_word(char *word, int lim)
{
    return fget_word(word, lim, stdin);
}


/*
   ** Find the token 'target' in file *fp
   ** If not found, complain and exit 
   ** if found the next fgets() call will read the next line.
 */

void find_target(char *target, FILE * fp)
{
    char s[MAXLEN];

    rewind(fp);
    while (fget_word(s, MAXLEN, fp) != EOF)
	if (stracmp(s, target) == 0) {
	    fgets(s, MAXLEN, fp);	/* read rest of line */
	    return;
	}
    printf("*** ERROR: target <%s> not found\n", target);
    exit(1);
}


/*
   ** NAME
   **      double fassign(char *var, FILE *fp)
   **
   ** DESCRIPTION
   **      Search in file *fp for the assignment 'var = number'
   **      If not found, complain and exit
 */

double fassign(char *var, FILE * fp)
{
    char s[MAXLEN];

    rewind(fp);
    while (fget_word(s, MAXLEN, fp) != EOF)	/* get word */
	if (stracmp(var, s) == 0)	/* found var */
	    if (fget_word(s, MAXLEN, fp) == '=') {	/* found = */
		fgets(s, MAXLEN, fp);	/* read rest of line */
		return atof(s);
	    }
    printf("*** ERROR: variable <%s> not found\n", var);
    exit(1);
}


double assign(char *var)
{
    return fassign(var, stdin);
}
