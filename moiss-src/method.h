/* This file is part of MOISS
   
   Copyright (C) 1998-99 Alan Aspuru Guzik
			 Raul Perusquia Flores
			 Carlos Amador Bedolla
			 Francisco Bustamante
			 Dario Bressanini
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
               
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
                           
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
                                    
*/

#ifndef __METHOD_H__
#define __METHOD_H__

void G_branch_DMC (walker * w);
void G_branch_bessel (walker * w);
void G_diff_all (walker * w);
void G_diff_all_IS (walker * w);
void G_diff_all_IS_Metro_1 (walker * w);
void G_diff_all_IS_Metro_2 (walker * w);
void G_diff_all_IS_Metro (walker * w);
void G_diff_bessel(walker * w);
void G_diff_one_IS_Metro (walker * w);
void inv_matrix(double **A, int ncol, double *d, double **y);


#endif /* __METHOD_H__ */

