/* This file is part of MOISS
   
   Copyright (C) 1998-99 Alan Aspuru Guzik
			 Raul Perusquia Flores
			 Carlos Amador Bedolla
			 Francisco Bustamante
			 Dario Bressanini
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
               
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
                           
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
                                    
*/

#ifndef __SLATER_H__
#define __SLATER_H__

void slater_phi_deriv(walker *w, double **psi,double **dpsix,double **dpsiy
		     ,double **dpsiz,double **ddpsi);
void slater_force(walker * w);

#endif /* __SLATER_H__ */

