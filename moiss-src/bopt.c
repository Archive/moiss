
/*
   ** opt.c
   **
   **   (C) Copyright Dario Bressanini
   **   Dipartimento di Scienze Matematiche Fisiche e Chimiche 
   **   Universita' dell'Insubria, sede di Como
   **   E-mail: dario@fis.unico.it
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <math.h>
#include <glib.h>
#include "moiss.h"
#include "mymath.h"
#include "proto.h"
#include "bto.h"
#include "../gmoiss-src/pl.h"
#include "stats.h"

#include "init.h"
#include "powell.h"
#include <errno.h>
#include <string.h>

#define MAXDIM 3
#define MAXPAR	100
#define MAXELE 5
#define MAXWALKS 20000


static double vv[MAXCOORD * MAXWALKS];
static double psi[MAXWALKS];
static double epot[MAXWALKS];
static double eloc[MAXWALKS];


static int nwalks;


/*--- Optimization section ---*/

static int iopt;		/* cost function */
static int iweight;		/* weight averages */
static int eclip;		/* clip energy ? */
static double eref;		/* reference energy */
static double emin, emax;	/* max and min eloc allowed */
static double wmin, wmax;	/* max and min weight allowed */
static int maxiter;		/* max iterations */
static double ftol;		/* tolerance in the minimum */
static double scale;		/* optimization scale */

static double e0, s20, sm20, weff0;

/*---------------------------------------------------------------------*/
/* write current parameters on file 'fname' */

void write_psi(char *fname)
{
    int i, j, ien, ipr, iee, isym;
    FILE *fp;

    fp = efopen(fname, "w");

    ien = iee = isym = ipr = 0;
    for (i = 0; i < nterm; i++) {

	fprintf(fp, "%f\n", plin[i]);
	for (j = 0; j < num_deN; j++)
	    fprintf(fp, "%f\n", parater_eN[ien++]);
	for (j = 0; j < numpr; j++)
	    fprintf(fp, "%f\n", ppr[ipr++]);
	for (j = 0; j < num_dee; j++)
	    fprintf(fp, "%f\n", parater_ee[iee++]);
	for (j = 0; j < numsym; j++)
	    fprintf(fp, "%f\n", psym[isym++]);
    }
    fclose(fp);
}



/* change the parameters of the current psi */

void change_psi(double p[])
{
    int i, j, k, ien, ipr, iee, isym;

    ien = iee = isym = ipr = 0;
    k = 0;
    for (i = 0; i < nterm; i++) {
	plin[i] = p[k++];
	for (j = 0; j < num_deN; j++)
	    parater_eN[ien++] = p[k++];		/* -abs() is enforced later */
	for (j = 0; j < numpr; j++)
	    ppr[ipr++] = p[k++];
	for (j = 0; j < num_dee; j++)
	    parater_ee[iee++] = p[k++];
	for (j = 0; j < numsym; j++)
	    psym[isym++] = p[k++];
    }
}



int get_psi(double p[])
{
    int i, j, k, ien, ipr, iee, isym;

    ien = iee = isym = ipr = 0;
    k = 0;
    for (i = 0; i < nterm; i++) {
	p[k++] = plin[i];
	for (j = 0; j < num_pars; j++)
	    p[k++] = parater_eN[ien++];
	for (j = 0; j < numpr; j++)
	    p[k++] = ppr[ipr++];
	for (j = 0; j < num_dee; j++)
	    p[k++] = parater_ee[iee++];
	for (j = 0; j < numsym; j++)
	    p[k++] = psym[isym++];

    }
    return npar * nterm;
}








void read_walkers(void)
{
    FILE *fp;
    double el, del2, grad[MAXCOORD];
    int i, j, k, n;
    unsigned long int seed;

    fp = fopen("file1", "r");

    fscanf(fp, "%d", &nwalks);
    fscanf(fp, "%ld", &seed);
    printf("nwalks: %d\n", nwalks);

    n = 0;
    for (i = 0; i < nwalks; i++, n++) {
	j = n * ncoord;
	for (k = 0; k < ncoord; k++)
	    fscanf(fp, "%lf", &vv[j + k]);
// ******************************AAAAAAAAAAAAAAAAAAHHHHHHHHHH
//	epot[n] = pot(&vv[j]);

//	get_del(&vv[j], &psi[n], &del2, grad);
	el = -0.5 * del2 / psi[n] + epot[n];
	eloc[n] = el;

#if 0
	printf("[%d] psi=%.8f del2=%.8f  v=%.8f  e=%.8f\n",
	       n, psi[n], del2, epot[n], eloc[n]);
#endif

	if (el < emin || el > emax) {
	    printf("[%d] psi %g  eloc %g -> %f\n", n, psi[n], el, vv[j]);
	    --n;
	}
    }
    nwalks = n;
    printf("nwalks: %d\n", nwalks);
}


/*
   ** this function evaluate the 'cost' of the set p[] of wavefunction
   ** parameters
   ** The 'cost' can be <H>, S2(H), Mu2(H) ....
 */

double cost(double p[])
{
    int i, j;
    double grad[MAXCOORD];
    double w, wtot, el, val, del2, e, e2;
    double w2;

    change_psi(p);		/* tell psi.c to use a new function */


    e = e2 = wtot = w2 = 0.0;
    for (i = 0; i < nwalks; i++) {
	j = i * ncoord;
// ********************AaaaaaaaaAAAAAAAAAAAAAAAAAAAAAAAHHHHHHHHHHHHHHHHHHHHH
//	get_del(&vv[j], &val, &del2, grad);	/* pass address of i-th walker */
	el = -0.5 * del2 / val + epot[i];

	if (iweight == 0)
	    w = 1.0;
	else
	    w = (val * val) / (psi[i] * psi[i]);

	if (w > wmax)
	    w = wmax;
	else if (w < wmin)
	    w = wmin;

#if 0
	if (el < emin || el > emax)
	    w = 0.0;
	if (eclip > 0)
	    if (el > emax)
		el = emax;
	    else if (el < emin)
		el = emin;
#endif

	wtot += w;
	w2 += w * w;
	e += w * el;
	e2 += w * el * el;
    }
    e /= wtot;
    e2 /= wtot;
    wtot /= nwalks;
    w2 /= nwalks;

    e0 = e;
    s20 = e2 - e * e;
    sm20 = s20 + (e - eref) * (e - eref);
    weff0 = wtot * wtot / w2;	/* this measure the efficiency */

#if 0
    printf("C %.8f %f %f \n", sm20, wtot, (wtot * wtot) / w2);
#endif

    if (iopt == 0)
	return e0;
    else if (iopt == 1)
	return s20;
    else
	return sm20;
}


/* transmit to caller the 'status' of the optimization */

void weight(double *e, double *s2, double *sm2, double *weff)
{
    *e = e0;
    *s2 = s20;
    *sm2 = sm20;
    *weff = weff0;
}


void opt(void)
{
    double p[MAXPAR];
    int i, npar;
    double fmin;

    npar = get_psi(p);		/* Ok, Ok,  no error check. So what? */

    printf("npar = %d\n", npar);
    for (i = 0; i < npar; i++)
	printf("p[%d]=%f\n", i, p[i]);
    powell(cost, p, npar, ftol, scale, maxiter, &fmin);
    for (i = 0; i < npar; i++)
	printf("p[%d]=%f\n", i, p[i]);

}



void optimize_b(simulation *s)
{
    FILE *fp;
    printf("1\n");
    fp = efopen("opt.dat", "r");
    iopt = fassign("iopt", fp);

    eref = fassign("eref", fp);


    iweight = fassign("iweight", fp);

    eclip = fassign("eclip", fp);
    emin = fassign("emin", fp);
    emax = fassign("emax", fp);
    wmin = fassign("wmin", fp);
    wmax = fassign("wmax", fp);
    ftol = fassign("ftol", fp);
    scale = fassign("scale", fp);
    maxiter = fassign("maxiter", fp);
    fclose(fp);

    printf("eref %f\n", eref);
    printf("iopt %d\n", iopt);
    printf("iweight %d\n", iweight);
    printf("emin %f   emax %f\n", emin, emax);
    printf("wmin %f   wmax %f\n", wmin, wmax);

    //bto_init(s);
	inter_nuclear_potential_init(s->e);
    read_walkers();

    opt();
	/* FIXME change read_par and write_psi so they receive a file * instead
	   of a char * :) */
    write_psi(s->parameters->path);
    read_par(s->parameters->path);


}

