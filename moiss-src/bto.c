#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gsl/gsl_rng.h>
#include <math.h>
#include <glib.h>
#include <stdlib.h>
#include <stdio.h>
#include "moiss.h"
#include "output.h"

/*
 ** compute and store all the distances of the walker, all the gradients
 ** and the laplacian
 */

#define MAXDIM 3		/* max dimensions */
#define MAXELE 6		/* max electrons */
#define MAXNUC 5		/* max nuclei */
#define MAXDEE ((MAXELE*(MAXELE-1))/2)	/* max dist. e-e */
#define MAXDEN (MAXELE*MAXNUC)	/* max dist. e-n */
#define MAXEE (MAXDEE*2)	/* max Parameters e-e */
#define MAXEN (MAXDEN*2)	/* max parameter e-n */
#define MAXSYM 4		/* max symmetric param. */

#define MAXPAR	100

#define MAXTERM 10
#define MAXPERM 100



static double vw[MAXCOORD];	/* save the walker */



static double lapee[MAXDEE];	/* laplacian dist. e-e */
static double lapen[MAXDEN];	/* laplacian dist. e-n */
static double gradee[MAXDEE][MAXCOORD];
static double graden[MAXDEN][MAXCOORD];




static double vnuc;		/* nuclear potential */


void store(walker * w)
{
    int i, j, k, n;
    double t, s;

    for (i = 0; i < ncoord; i++)	/* save the walker */
	vw[i] = w->R[i];

    for (i = 0; i < ncoord; i++) {
	for (j = 0; j < num_dee; j++)
	    gradee[j][i] = 0.0;
	for (j = 0; j < num_deN; j++)
	    graden[j][i] = 0.0;
    }
    //printf("\n");
    /* store e-n distances */
    for (i = k = 0; i < num_pars * dimensions; i += dimensions)
	for (j = 0; j < num_nucleos; j++, k++) {
	    s = 0.0;
	    for (n = 0; n < dimensions; n++) {
		t = (w->R[i + n] - w->nuc_R[n+j]);
		s += t * t;
	    }
	    t = sqrt(s);
	    disteN[k] = t;
	    lapen[k] = (dimensions - 1) / t;
	    for (n = 0; n < dimensions; n++) {
		graden[k][i + n] = (w->R[i + n] - w->nuc_R[n+j]) / t;
		// printf("xxx %d %d %f \n",k,i+n,graden[k][i+n]);

	    }
	}

    /* store e-e distances */
    for (i = k = 0; i < (num_pars - 1) * dimensions; i += dimensions)
	for (j = i + dimensions; j < num_pars * dimensions; j += dimensions, k++) {
	    s = 0.0;
	    for (n = 0; n < dimensions; n++) {
		t = (w->R[i + n] - w->R[j + n]);
		s += t * t;
	    }
	    t = sqrt(s);
	    distee[k] = t;
	    lapee[k] = 2 * (dimensions - 1) / t;
	    for (n = 0; n < dimensions; n++) {
		gradee[k][i + n] = (w->R[i + n] - w->R[j + n]) / t;
		gradee[k][j + n] = -gradee[k][i + n];
		//printf("HHH %d %d %f \n",k,i+n,gradee[k][i+n]);
	    }
	}


}






/* potential energy */

double pot(walker * w)
{
    int i, j, n;
    double t, s, v, vee;

    v = vee = 0.0;

    /* e-n interactions */

    for (j = 0; j < num_nucleos; j++)
	for (i = 0; i < w->num_pars; i++) {
	    s = 0.0;
	    for (n = 0; n < 3; n++) {
		t = (w->R[3 * i + n] - w->nuc_R[n+j]);
		s += t * t;
	    }
	    v += echarge[i] * w->nuc_Z[j] / sqrt(s);
	}

    /* e-e interactions */

    for (i = 0; i < (num_pars - 1); i++)
	for (j = i + 1; j < num_pars; j++) {
	    s = 0.0;
	    for (n = 0; n < dimensions; n++) {
		t = (w->R[dimensions * i + n] - w->R[dimensions * j + n]);
		s += t * t;
	    }
	    vee += eescale * echarge[i] * echarge[j] / sqrt(s);
	}
    // printf(".*.*.*.* %f %f %f \n",v,vee,e->interN_pot );
    return v + vee + w->interNpot;	/* add the constant nuclei-nuclei potential */
}


/*-----------------------------------------------------------------------*/


/*
   ** compute the exponent of the e-e part, for the it-th term and
   ** the ip-th permutation
 */

void expee(int it, int ip, double *val, double *del2, double grad[])
{
    int i, j, n, ipt;
    double p;

    for (i = 0; i < ncoord; i++)
	grad[i] = 0.0;

    *val = *del2 = 0.0;

    ipt = it * eeparam;
    for (j = 0; j < num_dee; j++) {
	p = parater_ee[ipt + j];
	n = permee[ip][j];
	*val += p * distee[n];
	*del2 += p * lapee[n];
	for (i = 0; i < ncoord; i++)
	    grad[i] += p * gradee[n][i];
    }
}


void expjee(int it, int ip, double *val, double *del2, double grad[])
{
    int i, j, n, ik;
    double a, b, c, r;
    double t, t2, t3;

    for (i = 0; i < ncoord; i++)
	grad[i] = 0.0;

    *val = *del2 = 0.0;

    ik = it * eeparam;
    for (j = 0; j < num_dee; j++) {
	a = parater_ee[ik++];
	b = 0.0;
	c = fabs(parater_ee[ik++]);

	n = permee[ip][j];
	r = distee[n];
	t = 1 + c * r;
	t2 = t * t;
	t3 = t * t2;

	*val += (a * r + b * r * r) / (1 + c * r);
	for (i = 0; i < ncoord; i++)
	    grad[i] += (a + 2 * b * r + b * c * r * r) / t2 * gradee[n][i];
	*del2 += 2 * (a * (dimensions - 1) / r + 2 * (b - a * c) / t3 + (dimensions - 1) * (b - a * c) * (1 / t2 + 1 / t));
    }
}


void expjee3(int it, int ip, double *val, double *del2, double grad[])
{
    int i, j, n, ik;
    double a, b, c, r;
    double t, t2, t3;

    for (i = 0; i < ncoord; i++)
	grad[i] = 0.0;

    *val = *del2 = 0.0;

    ik = it * eeparam;
    for (j = 0; j < num_dee; j++) {
	a = parater_ee[ik++];
	b = parater_ee[ik++];
	c = fabs(parater_ee[ik++]);

	n = permee[ip][j];
	r = distee[n];
	t = 1 + c * r;
	t2 = t * t;
	t3 = t * t2;

	*val += (a * r + b * r * r) / (1 + c * r);
	for (i = 0; i < ncoord; i++)
	    grad[i] += (a + 2 * b * r + b * c * r * r) / t2 * gradee[n][i];
	*del2 += 2 * (a * (dimensions - 1) / r + 2 * (b - a * c) / t3 + (dimensions - 1) * (b - a * c) * (1 / t2 + 1 / t));
    }
}


void expen(int it, int ip, double *val, double *del2, double grad[])
{
    int i, j, n, ipt;
    double p;

    for (i = 0; i < ncoord; i++)
	grad[i] = 0.0;

    *val = *del2 = 0.0;

    ipt = it * eNparam;
    for (j = 0; j < num_deN; j++) {
	p = -fabs(parater_eN[ipt + j]);
	n = permen[ip][j];
	*val += p * disteN[n];
	*del2 += p * lapen[n];
	for (i = 0; i < ncoord; i++)
	    grad[i] += p * graden[n][i];
    }
}


void expjen(int it, int ip, double *val, double *del2, double grad[])
{
    int i, j, n, ik;
    double v, d2, a, b, c, r;
    double t, t2, t3;

    for (i = 0; i < ncoord; i++)
	grad[i] = 0.0;

    v = d2 = 0.0;

    ik = it * eNparam;
    for (j = 0; j < num_deN; j++) {
	a = parater_eN[ik++];
	b = -fabs(parater_eN[ik++]);
	if (jen == 1)
	    c = 1;
	else
	    c = fabs(parater_eN[ik++]);

	n = permen[ip][j];
	r = disteN[n];
	t = 1 + c * r;
	t2 = t * t;
	t3 = t * t2;

	v += (a * r + b * r * r) / (1 + c * r);
	for (i = 0; i < ncoord; i++)
	    grad[i] += (a + 2 * b * r + b * c * r * r) / t2 * graden[n][i];
	d2 += a * (dimensions - 1) / r + 2 * (b - a * c) / t3 + (dimensions - 1) * (b - a * c) * (1 / t2 + 1 / t);
    }
    *val = v;
    *del2 = d2;
}



/* totalsymmetric factor */

void symfactor(int it, double *val, double *del2, double grad[])
{
    int i, j, isym;
    double s, t, t2, t3;
    double a, b;
    double r, v, d2;

    isym = it * numsym;
    a = psym[isym];
    b = fabs(psym[isym + 1]);

    /* first compute the exponent. No need to consider permutations,
       ** since it is totally symmetric. Cycle on e-e distances
     */

    v = d2 = 0.0;
    for (i = 0; i < ncoord; i++)
	grad[i] = 0.0;

    for (j = 0; j < num_dee; j++) {

	r = distee[j];
	t = 1 + b * r;
	t2 = t * t;
	t3 = t * t2;

	v += a * r / t;
	d2 += 2 * a * ((dimensions - 1) / (r * t2) - 2 * b / t3);
	for (i = 0; i < ncoord; i++)
	    grad[i] += a * gradee[j][i] / t2;
    }

    /* now compute the exponential */

    v = exp(v);
    s = 0.0;
    for (i = 0; i < ncoord; i++) {
	s += grad[i] * grad[i];
	grad[i] *= v;
    }

    *val = v;
    *del2 = v * (d2 + s);
}


/* compute the prefactor */

void prefactor(int it, int ip, double *val, double *del2, double grad[])
{
    int i, n, ipt, k, ik1, ik2, ik3;
    double a0;
    double x1, y1, z1;
    double x2, y2, z2;
    double x3, y3, z3;

    for (i = 0; i < ncoord; i++)
	grad[i] = 0.0;

    *val = 1.0;
    *del2 = 0.0;

    /* WE NEED A SMART WAY TO SELECT THE PREFACTOR */


    ipt = it * numpr;
    switch (k = type_pr[it]) {	/* CHANGE */

    case 0:			/* r_i , i = 0..9 */
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
	n = permen[ip][k];
	*val = disteN[n];
	*del2 = lapen[n];
	for (i = 0; i < ncoord; i++)
	    grad[i] = graden[n][i];
	break;

    case 10:			/* r_i + a0 */
    case 11:
    case 12:
    case 13:
    case 14:
    case 15:
    case 16:
    case 17:
    case 18:
    case 19:
	k -= 10;
	a0 = ppr[ipt];		/* select the parameter */
	n = permen[ip][k];
	*val = disteN[n] + a0;
	*del2 = lapen[n];
	for (i = 0; i < ncoord; i++)
	    grad[i] = graden[n][i];
	break;

    case 20:			/* a0 + Sum[a_i r_i] */
	a0 = ppr[ipt++];
	*val = a0;
	for (k = 0; k < num_deN; k++) {
	    n = permen[ip][k];
	    a0 = ppr[ipt++];
	    *val += a0 * disteN[n];
	    *del2 += a0 * lapen[n];
	    for (i = 0; i < ncoord; i++)
		grad[i] += a0 * graden[n][i];
	}
	break;


    case 21:			/* abs(a0) + Sum[abs(a_i) r_i] */
	a0 = fabs(ppr[ipt++]);
	*val = a0;
	for (k = 0; k < num_deN; k++) {
	    n = permen[ip][k];
	    a0 = fabs(ppr[ipt++]);
	    *val += a0 * disteN[n];
	    *del2 += a0 * lapen[n];
	    for (i = 0; i < ncoord; i++)
		grad[i] += a0 * graden[n][i];
	}
	break;


    case 100:			/* x1 y2 z3 */
	  //ik1 = perm[ip][1];
	  //ik2 = perm[ip][2];
	  //ik3 = perm[ip][3];
	  ik1 = gsl_matrix_int_get(symmetry, ip, 1);
	  ik2 = gsl_matrix_int_get(symmetry, ip, 2);
	  ik3 = gsl_matrix_int_get(symmetry, ip, 3);

	x1 = vw[ik1 * dimensions];
	y1 = vw[ik1 * dimensions + 1];
	z1 = vw[ik1 * dimensions + 2];
	x2 = vw[ik2 * dimensions];
	y2 = vw[ik2 * dimensions + 1];
	z2 = vw[ik2 * dimensions + 2];
	x3 = vw[ik3 * dimensions];
	y3 = vw[ik3 * dimensions + 1];
	z3 = vw[ik3 * dimensions + 2];

	*val = x1 * y2 * z3 - x3 * y2 * z1 + x2 * y3 * z1 + x3 * y1 * z2 - x1 * y3 * z2 - x2 * y1 * z3;
	*del2 = 0.0;
	grad[ik1 * dimensions + 0] = y2 * z3 - y3 * z2;
	grad[ik1 * dimensions + 1] = x3 * z2 - z3 * x2;
	grad[ik1 * dimensions + 2] = x2 * y3 - x3 * y2;

	grad[ik2 * dimensions + 0] = y3 * z1 - z3 * y1;
	grad[ik2 * dimensions + 1] = x1 * z3 - z1 * x3;
	grad[ik2 * dimensions + 2] = x3 * y1 - x1 * y3;

	grad[ik3 * dimensions + 0] = y1 * z2 - z1 * y2;
	grad[ik3 * dimensions + 1] = x2 * z1 - z2 * x1;
	grad[ik3 * dimensions + 2] = x1 * y2 - x2 * y1;

	break;

    case 111:			/* x2 */
	  //ik1 = perm[ip][1];
	  //ik2 = perm[ip][2];
	  //ik3 = perm[ip][3];
	  ik1 = gsl_matrix_int_get(symmetry, ip, 1);
	  ik2 = gsl_matrix_int_get(symmetry, ip, 2);
	  ik3 = gsl_matrix_int_get(symmetry, ip, 3);
	x1 = vw[ik1 * dimensions];
	y1 = vw[ik1 * dimensions + 1];
	z1 = vw[ik1 * dimensions + 2];
	x2 = vw[ik2 * dimensions];
	y2 = vw[ik2 * dimensions + 1];
	z2 = vw[ik2 * dimensions + 2];
	x3 = vw[ik3 * dimensions];
	y3 = vw[ik3 * dimensions + 1];
	z3 = vw[ik3 * dimensions + 2];

	*val = x2;
	*del2 = 0.0;

	grad[ik2 * dimensions + 0] = 1.0;
	grad[ik2 * dimensions + 1] = 0.0;
	grad[ik2 * dimensions + 2] = 0.0;

	break;

    default:
	break;
    }

}

//AGGREGLANDO, bueno para arreglarlo chido tambien necesito que le mandes
// el ensemble para los signos FIXME
void get_del(walker *w)
{
    int i, ip, it;
    double gradab[MAXCOORD];
    double gree[MAXCOORD], gren[MAXCOORD], grpr[MAXCOORD];
    double del2en, del2ee, del2pr, ee, en, pr;
    double sym, del2sym, grsym[MAXCOORD];
    double vt, d2t, grt[MAXCOORD];
    double c, espo, s, del2ab, ab_dot_ab, ab_dot_pr;

    /* we need to clear gree[] and grsym[] in case they are not called */

    for (i = 0; i < ncoord; i++)
	w->grad[i] = gree[i] = grsym[i] = 0.0;

    w->Psi = w->del2 = 0.0;

    del2ab = del2ee = del2sym = 0.0;
    ee = sym = 0.0;

    store(w);

    for (it = 0; it < nterm; it++) {	/* term cycle */

	vt = d2t = 0.0;
	for (i = 0; i < ncoord; i++)
	    grt[i] = 0.0;

	for (ip = 0; ip < nperm; ip++) {	/* permutation cycle */
	  c = plin[it] * gsl_vector_int_get(symmetry_signs, ip);
	  
	  if (jen == 0)
	    expen(it, ip, &en, &del2en, gren);
	  else
	    expjen(it, ip, &en, &del2en, gren);
	  
	  if (jee == 0)
	    expee(it, ip, &ee, &del2ee, gree);
	  else if (jee == 1)
	    expjee(it, ip, &ee, &del2ee, gree);
	  else if (jee == 2)
	    expjee3(it, ip, &ee, &del2ee, gree);
	  
	  prefactor(it, ip, &pr, &del2pr, grpr);
	  
	    espo = exp(ee + en);
	    //      printf(".. %d %d  %f %f \n",it,ip,ee,en);
	    //   for (i = 0; i < ncoord; i++)
	    //   printf("..           %f %f \n",gree[i],gren[i]);
	    

	    
	    ab_dot_ab = ab_dot_pr = 0.0;
	    for (i = 0; i < ncoord; i++) {
	      gradab[i] = gree[i] + gren[i];
	      ab_dot_ab += gradab[i] * gradab[i];
		ab_dot_pr += gradab[i] * grpr[i];
		grt[i] += c * espo * (grpr[i] + pr * gradab[i]);
		//      printf("### %f \n",ab_dot_ab);
	    }
	    
	    //printf("xx %d %d %f  %f %f \n",it,ip,c,pr,espo);
	    //  printf("XX %d %d %f  %f  \n",it,ip,del2ee,del2en);
	    del2ab = del2ee + del2en;
	    
	    vt += c * pr * espo;
	    d2t += c * espo *
	      (del2pr + 2.0 * ab_dot_pr + pr * (del2ab + ab_dot_ab));
	    // printf("**** %f %f %f %f %f %f \n",d2t,del2pr,ab_dot_pr,pr,del2ab,ab_dot_ab);
	}
	
	if (numsym > 0) {	/* use symmetric factor */
	    symfactor(it, &sym, &del2sym, grsym);
	    s = 0.0;
	    for (i = 0; i < ncoord; i++) {
	      s += grt[i] * grsym[i];
		grt[i] = vt * grsym[i] + sym * grt[i];
	    }
	    d2t = vt * del2sym + sym * d2t + 2 * s;
	    vt *= sym;
	}
	//printf("HH %f %f  \n",vt,d2t);
	w->Psi += vt;
	w->del2 += d2t;
	for (i = 0; i < ncoord; i++)
	    w->grad[i] += grt[i];
    }
    //printf("ww %f %f  \n",*psi,*del2);
    
}
