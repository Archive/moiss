#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <glib.h>
#include "moiss.h"
#include "init.h"
#include "path.h"
#include <math.h>
#include "rand.h"

#define num_anillo 50


int norm_bin,iout;
int nout=100;
int nequil=20;
double aceptados;
double apath[num_anillo+2];
double propt[1000];

double ecum=0;
double V_energy(double a)
{
  return 0.5*a*a;
}

double V_energye(double a)
{
  return a*a;
}

void 
estadisticas(double prop[],int estadist, int num_steps)
{
  int i;
  
  double anorm,accept,venerg=0,x,eave=0;
  
  
 
  norm_bin=graph_bins;
 
  venerg=0.0;
  anorm=1.0/(double)(num_anillo*num_steps);
  accept=aceptados*anorm;

  if(iout==1)
    {
     for (i = 0; i < norm_bin; i++)
       propt[i]=0.0;
   
    }
  for (i = 0; i < norm_bin; i++)
    {
	
	  prop[i]=prop[i]*anorm;
	  x=graph_low+delta_norm/2+(i*delta_norm);
	  venerg+=prop[i]*V_energye(x);
	
    }
  if(estadist==1)
    {
      ecum+=venerg;
      for (i = 0; i < norm_bin; i++)
	propt[i]=prop[i];
      
      eave=ecum/(iout-nequil);
      
    }
  for (i = 0; i <norm_bin; i++)
    prop[i]=0.0;
   printf("%d %f %f %f \n",iout,venerg,accept,eave);
}

void walk_path(double prop[], int num_steps)
{
  int i, elem;
  double change;
  double deltaE,newelem;
  double delta = 1.5;
  int dumb;
  double t9,t10,t11,t12,cte_path;
  double ep=0.1;

  cte_path=0.5/(ep*ep);
  aceptados=0;
      
  for (i = 0; i < num_steps*num_anillo; i++) 
    {
      elem = 1+ (int)(gsl_rng_uniform(rng) *num_anillo) ;
      change = ( gsl_rng_uniform(rng) -0.5)*delta ;
      newelem = apath[elem] + change;
      
      t12=apath[elem+1]-newelem;
      t11=apath[elem-1]-newelem;
      t10=apath[elem+1]-apath[elem];
      t9=apath[elem-1]-apath[elem];
      
      deltaE= V_energy(newelem)-V_energy(apath[elem])
	+cte_path* (P2(t12)+P2(t11)-P2(t10)-P2(t9));
      
      if(exp(-ep*deltaE) > gsl_rng_uniform(rng))
	{	
	  apath[elem]=newelem;
	  aceptados++;
	  if(elem==1)
	    apath[num_anillo+1]=newelem;
	  if(elem==num_anillo)
	    apath[0]=newelem;
	  }
      
	if(apath[elem]>graph_low && apath[elem]<graph_high )
	  {  
	    dumb = (( apath[elem]- graph_low) / delta_norm);
	    prop[dumb]++;
	  }
      }
}

void
path(int bloques, int estad, int num_steps)
{
  double vec[graph_bins];
  double *prop;
  int i;
  FILE *file_path;
  prop=vec;
  
  for (i = 1; i < num_anillo+1; i++) 
    apath[i] = gsl_rng_uniform(rng)-0.5;
  
  apath[num_anillo+1]=apath[1];
  
  apath[0]=apath[num_anillo];
  printf("%d \n",graph_bins);
  for (i = 0; i < graph_bins; i++) 
    {  
      prop[i] = 0.0;
    }

  for(iout=1;iout<=bloques;iout++)
    {
      walk_path(prop, num_steps);
      estadisticas(prop,estad, num_steps);
    }
  
  if ((file_path = fopen ("res", "wb")) == NULL)
    {
      printf ("I can't create the file res  \n");
      exit (1);
    }
  for (i = 0; i < graph_bins; i++)
      fprintf (file_path, "%f %f\n",graph_low+delta_norm/2+(i*delta_norm)
	       ,propt[i]);
  fclose (file_path);
}
















