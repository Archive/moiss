//debug
double interN_pot;


/* This file is part of MOISS
   
   Copyright (C) 1998-99 Alan Aspuru Guzik
			 Raul Perusquia Flores
			 Carlos Amador Bedolla
			 Francisco Bustamante
			 Dario Bressanini
 
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
               
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
                           
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
                                    
*/

// pgcc option = -mpentiumpro

#ifndef __MOISS_H__
#define __MOISS_H__

/* Other *.h's included.  */
#include "../gmoiss-src/pl.h"
//#include "rand.h"
#include <gsl/gsl_rng.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <signal.h>
int restart; /* FIXME: do we need this? */

#define HO_const 1.0

/************************ Bressanini TRIAL FUNCTIONS ***************/
#define MAX_BASIS 30
#define MAX_DET    8
#define MAX_ELEC  40
#define MAX_NUC   10
#define MAX_TERM  20
#define MAX_PERM  20
#define MAX_DEE ((MAX_ELEC*(MAX_ELEC-1))/2)		/* max dist. e-e */
#define MAX_DEN (MAX_ELEC*MAX_NUC)			/* max dist. e-n */
#define MAX_EE (MAX_DEE*2)			/* max Parameters e-e */
#define MAX_EN (MAX_DEN*2)			/* max parameter e-n */
#define MAX_PAR	100
#define MAX_SYM 4				/* max symmetric param. */
#define MAXCOORD (MAX_ELEC*3)
#define MAX_NUM 100000000 /* maximum number in the program */
/*************************/

gsl_rng * rng;



int parity; /* bressanini's parity operator */
//double eescale;			/* to scale V(e,e) */
// eliminated may give compilation troubles, but now is read where it should 
double emass[MAX_ELEC];
double echarge[MAX_ELEC];	
int nterm;			/* terms */
int jee;				/* use ee ? */
int jen;				/* which en ? */
int symee;			/* symmetric ee */
int symen;			/* symmetric en */
int numpr;			/* parameters in the prefactor */
int npar;			/* parameters in a term */
int type_pr[MAX_TERM];		/* prefactor type */
int nperm;			/* permutations */
//int sign[MAX_PERM];
//int perm[MAX_PERM][MAX_ELEC];	/* permutation vector */
int permee[MAX_PERM][MAX_DEE];		/* e-e permutations */
int permen[MAX_PERM][MAX_DEN];		/* e-n permutations */
double plin[MAX_TERM];			/* linear parameters */
double parater_ee[MAX_EE*MAX_TERM];		/* e-e parameters */
double parater_eN[MAX_EN*MAX_TERM];		/* e-n parameters */
double ppr[MAX_PAR*MAX_TERM];		/* prefactor parameters */
double psym[MAX_SYM*MAX_TERM];		/* prefactor parameters */
gsl_rng * rng;                      /* Random Number Generator Instance */
int ncoord;
			/* parameters e-e */
int numsym;			/* parameters in symmetric factor */

#define INPUT_FILENAME "input"
#define HISTOGRAM_FILENAME "psi"
#define ENERGY_FILENAME "energy_vs_t"
#define RESTART_FILENAME "restart"
#define GMOISS_FILENAME "gmoiss-input"
#define WALKERS_FILENAME "walk"
#define PARAMETERS_FILENAME "./psi.inp"

double disteN[MAX_DEN];
double distee[MAX_DEE];
int eNparam;
int eeparam;

int block_count;

/*********** PROPLISTS ************/
proplist_t calculation_dict;

char *nom_gmoiss_input;

/*********** STRUCTS   ************/

typedef struct file {
    char * path; /* string that holds the filename */
    FILE * fp;   /* file handler to that filename */
} file;
/*
typedef struct nucleus {
  int n1s;
  int n2s;
  int n2p[3];
  int n3s;
  int n3p[3];
  int n3dzz;
  int n3dx2_y2;
  int n3dxy;
  int n3dxz;
  int n3dyz;
  int n4s;
  int n4p[3];
  int *Z;
  double *R;
} nucleus;
*/

/* Dario Bresannini's proposed structure, it proposes eliminating
   struct par at all and passing *r and returning *gr as a whole
   to functions. Vector supercomputers can work with the r in a cycle
   instead of going through each particle.

   Disadvantages: the for loops in the code become a little more complex
   We would have to recode a lot... The change is left for later.
   Also, Dario proposes us to store the potential energy instead of all
   Egy_Local, that makes sense if we want to divide the energy between
   kinetic and potential energy. As soon as Rulox arrives we'll fix this
   FIXME
   typedef struct {
   int sign;                        sign of the psip 
   double w;                        weight of the psip 
   double psi_g;                    psi guide 
   double psi_t;                    psi trial 
   double del2;                     laplacian of psi_t 
   double pot;                      potential energy 
   double *r;                       pointer to coordinates 
   double *gr;                      pointer to gradient 
   } WALKER;
*/
double eescale;
typedef struct walker {
  int    dim; /* dimension */
  int    num_pars;     /*number of particles */
  int num_nucleos;       /* number of nuclei */
  double del2;           /* delta squared of psi */
  double pot;            /* potential energy, implement later */
  double *grad;          /* gradient of psi */
  double *Force;
  double psi_g; 
  double psi_t;     
  double weight;         /* weight associated to the walker */
  double Psi;           
  double Egy_Lcl;        /* local energy */
  double *R;             /* walker position */
  double *nuc_R;         /* nuclei positions */
  double *nuc_Z;         /* nuclei atomic number */
  double interNpot;

  float  Z; /* Atomic number (or charge) in case of boson */
} walker;
/* replace in bto.c ASAP!!! the same is needed for
   'parity' I will do it at the end with all the variables
   eescale, parity */

typedef struct ensemble {
  double Paccept;         /*  Aceppt promedio    */
  int num_walkers;             /* actual number of walkers */
  int min_walkers;             /* minimum number of walkers */
  int ini_walkers;             /* initial number of walkers */
  int max_walkers;             /* max. number of walkers */
  double total_weight;         /* total weight */
  int num_pars;                /* # of particles in the Sim. (electrons) */
  int dim;                 /* physical space dimensions */
  double ref_energy;           /* reference energy (E_Ref, e->ref_energy) */
  double energy;               /* average energy (ELocalAvg)*/
  walker * walkers;            /* pointer to the WALKER array */
  double ee_scale;             /* electron-electron repulsion scale */
  double interN_pot;            /* internuclear potential energy */
                                 /* used for potential calculations */
 
} ensemble;
gsl_matrix_int * symmetry;   /* symmetry permutations either for
				correlated exponential or for
				STO's eventually */
gsl_vector_int * symmetry_signs; /* the sign for each term of the symmetry
		      	    matrix */
typedef struct opto {
int iopt;			/* cost function */
int iweight;			/* weight averages */
int eclip;			/* clip energy ? */
double eref;			/* reference energy */
double emin, emax;		/* max and min eloc allowed */
double wmin, wmax;		/* max and min weight allowed */
int maxiter;			/* max iterations */
double ftol;			/* tolerance in the minimum */
double scale;			/* optimization scale */
}opto;

int iopt;			/* cost function */
int iweight;			/* weight averages */
int eclip;			/* clip energy ? */
double eref;			/* reference energy */
double emin, emax;		/* max and min eloc allowed */
double wmin, wmax;		/* max and min weight allowed */
int maxiter;			/* max iterations */
double ftol;			/* tolerance in the minimum */
double scale;			/* optimization scale */


typedef struct simulation {
  ensemble* e;          /* Ensemble associated to the simulation */
  //    nucleus *nuclei;
  GList *mc_list;            /* List of Monte Carlos to perform */
  file * input; /* non gui input file, must dissapear eventually */
  file * restart;
  file * wavefun;   /* Wave function for plotting (histogram) */
  file * energy;
  file * walkers;
  file * ginput; /* gmoiss input file */
  file * parameters; /* psi parameters file */
  int hola;
} simulation;



/************* VMC ****************/




double E_mix_PI;
double E_mix_b;
double P_Metropolis;
/*double Q_Metropolis;*/



/* Variables for STOs  */
/*
double *C1s;
double *C2s;
double *C2px;
double *C2py;
double *C2pz;
double *C3s;
double *C3px;
double *C3py;
double *C3pz;
double *C3dzz;
double *C3dx2_y2;
double *C3dxy;
double *C3dxz;
double *C3dyz;
double *C4s;
double *C4px;
double *C4py;
double *C4pz;
*/
double *zetaqw; /* DESCRIBE */
double zeta[MAX_BASIS]; /* DESCRIBE */
int num_dNN; /* number of distances Nuclei-Nuclei */
int num_dee; /* number of distances electron-electron */
int num_deN; /* number of distances electron-nuclei */
double vecjas1[MAX_ELEC]; /* DESCRIBE */
double vecjas2[3][MAX_ELEC]; /* DESCRIBE */
double *laplc_jastrw; /* DESCRIBE */
double **gradjstrw; /* DESCRIBE */

double jastrow; /* DESCRIBE */
double jstrw_b; /* DESCRIBE */


double hamiltonian_V; /* DESCRIBE */

/*

�����QUE ES ESTO???????
��� LE DAMOS KRANKY????
double **slater_psi;
double **dslater_psix;
double **dslater_psiy;
double **dslater_psiz;
double **ddslater_psi;


double vec_slater1[MAX_ELEC][MAX_BASIS];
double vec_slater2[MAX_ELEC][MAX_BASIS];
double vec_slater3[MAX_ELEC][MAX_BASIS];
double vec_slater4[MAX_ELEC][MAX_BASIS];
double vec_slater5[MAX_ELEC][MAX_BASIS];
*/

int num_up; /* DESCRIBE */
int num_down; /* DESCRIBE */
int num_up2; /* DESCRIBE */

/* Global variables */

/* Local energy for the simulation */
double E_local; 

double pi; /* DESCRIBE */
int num_nucleos; /* DESCRIBE */

//walker *walkers;		/* Vector of Walkers                               */
double *energy_vs_time;		/* This is for the plot Energy = f(T)              */
double *eblk; /* DESCRIBE */


float const_norm[MAX_DET*MAX_ELEC][MAX_BASIS];

double cte_jas_1; /* DESCRIBE (Jastrow Constant 1?)*/
double cte_jas_2; /* DESCRIBE (Jastrow Constant 2?)*/



// int num_steps;                  /* Number of simulation iterations                 */

int num_coef; /* DESCRIBE */
int num_basis; /* DESCRIBE */
int num_det; /* DESCRIBE */
int num_pars_up; /* DESCRIBE */


int initial_spread;		/* Initial random spread distribution of walkers   */
int graph_bins;			/* Number of bins for plotting                     */
double graph_low;		/* Lower limit for the graph                       */
double graph_high;		/* Higher limit for the graph                      */
double dt;			/* imaginary time differential                     */

int *entes; /* DESCRIBE */
double delta_norm; /* DESCRIBE */

/*constantes de normalizacion para las funciones de prueba*/
double trialfun_cte_He_1; 
double trialfun_cte_H;     



double trialfun_cte;      /*constantes de normalizacion para las funciones de prueba*/
double Ddt;                     

double vector1[15];    /*para calcular la distribucinon exponencial*/
int seed_fija;                  /*if seed_fija ==1 then you always have the same
				  random numbers                                   */
/*Bessel*/
double IS_factor;
float Vmax_bessel;
//FILE *file_restart;             
//FILE *file_input;		/* Input file for the calculation           */
//FILE *file_energy;		/* Output file for energy_vs_t              */
double teff;                    /* effective time step                      */ 
double diff_sigma;		/* 2 * D * t , For Box Muller's std deviv   */
int num_pars;			/* # de particulas                          */
int max_branch;			/* Control Natal                            */
int dimensions;			/* Numero de dimensiones del problema       */
int mem_extra;                   /* Buffer for the array, make it a config option! */
double half_box[100];           /* temporal */

double dt_convergence_factor;    /* convergence of the dt see the docs or walk.c */

int molecule_type;
int system_type;
int potential_type ;         /* Potential type  */
int branching_type ;         /* Branching type  */
int trial_function_type ;    /* Self commenting */


double tunnel_a ;    /* Starting point for the tunnel block, < half-box */
double tunnel_b ;    /* Ending point for the tunnel block, < half-box */
double tunnel_h ;    /* Height in angstrons of the tunnel block */

double trial_alpha ; /* Gaussian Trial Function Alpha value */
double trial_alpha2; 

double lehnard_jones_sigma; /* Sigma parameter for Lehnard Jones potential
			       Please read potential.c for details */
double lehnard_jones_epsilon; /* Well depth for Lehnard Jones */

/*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
/* Declaring the function pointers */

void (* Derivs) ();
//double (* V) ();                /* Function pointer to the potential energy */
double (* V2) (); 
double ( * E_T ) ();
double ( * E_Var ) ();
//double (* HPsioPsi) ();      /* HPsi / Phi term (not a very cool name) */
void (* G_Branch) ();           /* FP to Branching Green's Function */
void (* G_Diff) ();             /* FP to Diffusion Green's Function */
void (* Force) ();         /* Quantum Force*/
//void (* PsiT) ();            /* Trial function */
void (* Walk) ();
//void (* Trialfun) ();
//void (* TrialFun) ();


walker new_walker (int dimensions, int num_pars);
double gauss_std (void);
void copy_walker (walker * w1, walker * w2);
void flush_walker(walker * w );
void print_walker(walker * w );
void print_r(double  * r[] );
void reincarn_particle(walker * w, int i);
void randhora ();
walker new_walker_from (walker * w1);
void initialize_walkers(walker * w1);
//void cerrar (void);
//void flush_Force_par (par * pars);
void print_time (GTimer * stopwatch);
/* Calculate the distance between two particles in a walker,
   i and j are the indices of the particles */
double distance (walker *  w, int i, int j);
# define INV(a) 1.0/a
//# define min(a,b) (((a)<(b)) ? (a) : (b))
# define Q_Metropolis 1.0-P_Metropolis
# define P2(a) a*a
# define P3(a) a*a*a
# define P4(a) a*a*a*a
# define P5(a) a*a*a*a*a
# define vabs(a)   (((a)>0) ? (a) : -(a))
# define sign(a)  (((a)>=0) ? (1) : (0))







#endif /* __MOISS_H__ */








