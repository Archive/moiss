/* This file is part of MOISS
   
   Copyright (C) 1998-99 Alan Aspuru Guzik
			 Raul Perusquia Flores
			 Carlos Amador Bedolla
			 Francisco Bustamante
			 Dario Bressanini
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
               
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
                           
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
                                    
*/

#ifndef __POTENTIAL_H__
#define __POTENTIAL_H__

double V_HOs(walker *w);
void GD_HOs(walker *w);
double V_hydrogen (walker *w);
void GD_hydrogen(walker *w);
double V_well (walker  *w);
double V_tunnel_open (walker *w);
double V_bump (walker *w);
double V_annular_well (walker * w);
double V_circular_well (walker * w);
double V_Lehnard_Jones (walker * w);
double V_He (walker *w);
void GD_HE_Hilleras(walker *w);
void GD_HE_Kellner(walker *w);

#endif /* __POTENTIAL_H__ */

