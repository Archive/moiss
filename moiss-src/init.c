#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gsl/gsl_rng.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include "moiss.h"
#include "../gmoiss-src/gmoiss.h"
#include "method.h"
#include "walk.h"
#include "potential.h"
#include "init.h"
#include "gui.h"
#include <errno.h>
#include <string.h>
#include "proto.h"
#include <proplist.h>
#include "bto.h"
#include "rand.h"

#define PL_INDENT_LEVEL 2

float find_float (char * name)
{
    float k;
    k = my_pl_get_float (name);
    // printf("Initializing %s to: %f\n", name, k);
    return k;
}

int find_int (char * name)
{
    int k;
    k = my_pl_get_int (name);
    //printf("Initializing %s to: %d\n", name, k);
    return k;
}

int find_string_to_int ( char ** dictionary, char *name)
{
    char * temp_string;
    int k;
    
    temp_string = my_pl_get_string(name);
    k = find_names_index_plus(dictionary, temp_string);
    
    if (k < 0)  {
	g_error("Index not found in: %s", temp_string);
    }
    
    
    //printf("Initializing %s to: %s, index = %d\n", name, temp_string, k);
    free(temp_string);
    return k;
}



void dynamic_init(simulation * s)
{
  switch (potential_type) {
  case WELL_POTENTIAL:
    V2 = V_well;
    break;
  case HYDROGEN_POTENTIAL:
    V2 = V_hydrogen;
    break;
  case TUNNEL_POTENTIAL:
    V2 = V_tunnel_open;
    break;
  case BUMP_POTENTIAL:
    V2 = V_bump;
    break;
	//case ANNULAR_WELL_POTENTIAL:
    //V2 = V_annular_well;
    //break;
	//case CIRCULAR_WELL_POTENTIAL:
    //V2 = V_circular_well;
    //break;
  case HELIUM_POTENTIAL:
    V2 = V_He;
    s->e->dim=3;
    num_pars=2;
    ncoord=6;
    break;
  case VDW_POTENTIAL:
    V2 = V_Lehnard_Jones;
    break;
  default:
    V2 = V_HOs;
    break;
  }
  printf("Potential used : %s \n", potential_names[potential_type]);
  
  
  switch (trial_function_type) 
    {
    case CORRELATED_EXPONENTIAL :
      bto_init(s);
      Derivs = get_del;
      V2 = pot;
      break;

    case HELIUM_HILLERAS:
      Derivs = GD_HE_Hilleras;
    
      break;
    case HELIUM_KELLNER:
      Derivs = GD_HE_Kellner;

    
    break;
  case HYDROGEN:
    Derivs = GD_hydrogen;
    dimensions=3;
    s->e->dim=3;
    num_pars=1;
    break;
  default:
    Derivs = GD_HOs ;
    num_pars=1;
    break;
  }
  
  printf("Trial Function: %s \n", trial_function_names[trial_function_type]);
  
}

/*As per the gsl, init now takes a int  value to initialize its
RNG. So either send it a fixed number or have the caller get time()
and send that as a seed. Hackish, huh?*/
 void init(simulation * s, int randseed)     
{
	/* finish x, y, z, Z */
  int i, e, elements;
  float x,y,z,Z;
  int checa,j,k,l;
  float fdumb;
  /* FIXME: Hey... what's the deal with this charalito?? please rename the variable :)*/
  char * charalito;
  proplist_t key, array, key_x, key_y, key_z, key_Z;
  proplist_t value_x, value_y, value_z, value_Z, element;
    
  /* initializing the random number generator */
  rng = gsl_rng_alloc(gsl_rng_taus);
  gsl_rng_set(rng,randseed);
  


  teff = dt;
  pi = 4 * atan(1);
  Ddt = 0.5 * dt;
  // correct:
  // http://www.pitt.edu/~wpilib/statfaq/gaussfaq.html
  diff_sigma = sqrt(dt);
  ncoord= s->e->dim * s->e->num_pars;
  /****  normalization factos  ************************/
  
  trialfun_cte_H = 1;
  trialfun_cte_He_1 = 1;
  /* printf("%f \n",trialfun_cte_H); */
  /*********************************************************/
  
  
  E_mix_PI = 1;
  E_mix_b = 0.0;
  /*bessel */
  Vmax_bessel = 0;
  
  s->e->ref_energy = 0.0;
  mem_extra = 3;

  
  s->e->walkers = (walker *) malloc((s->e->ini_walkers * mem_extra) * sizeof(walker));
  if (s->e->walkers == NULL)
    g_error("No memory for walkers!");

  
  

  /* Initialzing the walker positions  */
 
  switch (restart) {
  case GAUSSIAN_INITIAL:
  for (i = 0; i < (s->e->ini_walkers * mem_extra); i++) 
    {
      s->e->walkers[i] = new_walker(s->e->dim, s->e->num_pars);
      
      if(system_type == BTF_SIM )
	read_nucleos(&s->e->walkers[i]);

      // printf("1: %f %d \n" ,s->e->walkers[i].pars[0].R[0],i );
    }
   for (i = 0; i < (s->e->ini_walkers); i++) 
     initialize_walkers(&s->e->walkers[i]);
  
  



    break;
    case FROM_FILE:
      
      for (i = 0; i < (s->e->ini_walkers * mem_extra); i++) 
	{
	  s->e->walkers[i] = new_walker(s->e->dim, s->e->num_pars);
	  if(system_type == BTF_SIM )
	    read_nucleos(&s->e->walkers[i]);
	  // printf("1: %f %d \n" ,s->e->walkers[i].pars[0].R[0],i );
	}
  


  if ((s->restart->fp = fopen(s->restart->path, "r")) == NULL) 
    {
      printf("I can't open the file %s \n", s->restart->path);
    exit(1);
    }
  fscanf(s->restart->fp,"%d",&checa);
  if(checa!=s->e->ini_walkers){
    if(checa>(int)(s->e->ini_walkers * mem_extra*0.85))
      {
	printf("Too many walkers in the file\n");
      }
    else{
	  g_warning("The number of walkers (%d) in file %s isn't the same as the walkers line in the input file (%d) ", checa, s->restart->path, s->e->ini_walkers);
	}
  }
  printf("Reading the walkers \n" );
  for (i = 0; i < checa; i++) {
    for (j = 0,l=0; j < s->e->num_pars; j++) 
	  for (k = 0; k < s->e->dim; k++,l++)
		{
		  fscanf(s->restart->fp,"%f",&fdumb);
		  s->e->walkers[i].R[l]=(double)fdumb; 
		  //	printf("qw %f %d \n",s->e->walkers[i].R[0],i);
		  s->e->num_walkers = checa;
		}
	}
  fclose(s->restart->fp);
  
  
      break;
  case FROM_COORDINATES:
    //printf("Initializing particles from coordinates:\n");
    key = PLMakeString(dictionary_names[ATOMIC_COORDINATES]);
    
      array = PLGetDictionaryEntry(calculation_dict, key);
      charalito = PLGetDescriptionIndent(array, 4);
      //PLRelease(key);
      elements = PLGetNumberOfElements(array);
      printf("Atomic coordinates are: %s\n", charalito);
      printf("Number of particles: %d\n", elements);
      
      if (elements != s->e->num_pars) 
	g_error("The number of particles specified is different to the number of particles\n in the coordinate section of the  input file. Please make them agree\n ");
      
      free(charalito);

      for (i = 0; i < (s->e->ini_walkers * mem_extra); i++) {
	s->e->walkers[i] = new_walker(s->e->dim, s->e->num_pars);
	//print_walker(&walkers[i]);
      }
	    
      for (e = 0; e < elements; e++) {
	
	element = PLGetArrayElement(array, e);
	key_x = PLMakeString(atomic_coordinates[ATOMIC_X]);
	key_y = PLMakeString(atomic_coordinates[ATOMIC_Y]);
	key_z = PLMakeString(atomic_coordinates[ATOMIC_Z]);
	key_Z = PLMakeString(atomic_coordinates[ATOMIC_NUMBER]);
	
	value_x = PLGetDictionaryEntry(element, key_x);
	value_y = PLGetDictionaryEntry(element, key_y);
	value_z = PLGetDictionaryEntry(element, key_z);
	value_Z = PLGetDictionaryEntry(element, key_Z);
	
	x= atof(PLGetString(value_x));
	y= atof(PLGetString(value_y));
	z= atof(PLGetString(value_z));
	Z= atof(PLGetString(value_Z));
	g_message("Placing  %s in (%f, %f, %f) ", atomic_names[(int)Z], x, y, z);
	
	for (i = 0; i < (s->e->ini_walkers * mem_extra); i++) {
	  
	  
	  s->e->walkers[i].R[0+e]= x;
	  s->e->walkers[i].R[1+e]= y;
	  s->e->walkers[i].R[2+e]= z;
	  s->e->walkers[i].Z = Z;
	  
	}
	
	PLRelease(key_x);
	PLRelease(key_y);
	PLRelease(key_z);
	PLRelease(key_Z);
		
	
      }
      
      for (i = 0; i < (s->e->ini_walkers * mem_extra); i++) {
	//		 (*TrialFun) (&(s->e->walkers[i]));
      }
      
      
      break;
  }
  /*dumb= new_walker(s->e->dim, s->e.num_pars); */
  
  /***************************************************************************************/

 
  
  /* set up gsl to deal with different rng's FIXME 
     this means setting up a GUI parameter for choosing the different
     RNG optons*/
	
  
  /* All these are parameters defined in rand.h, so we reckon they
     must be related to the random number generator; taken from
       fishman, Montecarlo concepts, algorithms and applications,
       Springer, 1995 */
  b_exp = 1.4142135623730950;
  q2_exp = 0.6931471805599453;
  p_exp = 0.9802581434685472;
  a_exp = 5.7133631526454228;
  c_exp = -1.6734053240284925;
  B_exp = 3.3468106480569850;
  A_exp = 5.6005707569738080;
  H_exp = 0.0026106723602095;
  
  a_cauchy = 0.6380631366077803;
  b_cauchy = 0.5959486060529070;
  q_cauchy = 0.9339962957603656;
  W_cauchy = 0.2488702280083841;
  A_cauchy = 0.6366197723675813;
  B_cauchy = 0.5972997593539963;
  H_cauchy = 0.0214949004570452;
  P_cauchy = 4.9125013953033204;
  /* OK rand number generator related values should end here. */
  
  
  F_normal = 1;
  vector1[0] = .693147180559945;
  vector1[1] = .933373687519046;
  vector1[2] = .988877796183868;
  vector1[3] = .998495925291496;
  vector1[4] = .999829281106139;
  vector1[5] = .999983316410073;
  vector1[6] = .999998569143877;
  vector1[7] = .999999890692556;
  vector1[8] = .999999992473416;
  vector1[9] = .999999999528328;
  vector1[10] = .999999999972882;
  vector1[11] = .999999999998560;
  vector1[12] = .999999999999929;
  vector1[13] = .999999999999997;
  vector1[14] = 1.0;
  
  
  /* Init ends here*/
    
  
}

/* Receives a mc_pl and transforms it into a MonteCarlo GList */


GList * mc_pl_to_list(proplist_t mc_pl)
{
    MonteCarlo * mc;
    GList * mc_list = NULL;
    int montecarlos, i, keys, j, found, meta_found;
	unsigned int k;
    proplist_t monte_pl, key_array, key, value;
    char * key_string, * napkin_string;
    Stat * stats;
					

    /* creating the list */
    //mc_list = g_list_alloc();    
    /* Checking if the thing is an array... it must be!*/
    g_assert(PLIsArray(mc_pl));

    /* checking the number of elements */
    
    montecarlos = PLGetNumberOfElements(mc_pl);
	//printf("Saque %d MonteCarlos\n", montecarlos);
    for (i=0; i < montecarlos; i++) {
	  /* getting memory for each Monte Carlo */
	  mc = g_malloc(sizeof(MonteCarlo));
	  mc->stats = NULL;
	  /* getting the Monte Carlo pl */
	  monte_pl = PLGetArrayElement(mc_pl, i);
	  
	  /* it must be a dictionary */
	  g_assert(PLIsDictionary(monte_pl));
	  
	  /* now descending into the dictionary's contents */
	  key_array = PLGetAllDictionaryKeys(monte_pl);
	  
	  keys = PLGetNumberOfElements(monte_pl);
	  
	  for (j =0; j < keys; j++){
	    /* finally getting the key and selecting it's destiny.
	       First we'll need to find the key index */
	    key = PLGetArrayElement(key_array, j);
	    value = PLGetDictionaryEntry(monte_pl, key);
	    key_string = PLGetString(key);
	    found = find_names_index(calculation_names, key_string);
		
		
	    switch (found)
		  {
		  case CALCULATION_TYPE:
			/* first assert we got a string */
			g_assert(PLIsString(value));
			/* gee, now I have to find again */
			napkin_string = PLGetString(value);
			meta_found = find_names_index(mc_names, napkin_string);
			/* shifting the index, the way of MC TYPES see
			   gmoiss.h for enlightenment */
			meta_found = meta_found + MC_VARIATIONAL;
			mc->type = meta_found;
			//g_message("Setting MC Type to %s", mc_names[mc->type - MC_VARIATIONAL]);
			/* getting rid of napkin_string */
			g_free(napkin_string);
			break;
		    case BLOCKS:
			/* first assert we got a string */
			g_assert(PLIsString(value));
			/* now we get the int */
			mc->blocks = atoi ( PLGetString(value) );
			//printf("Setting the Blocks to %d\n", mc->blocks);
			break;
		    case STEPS:
			/* first assert we got a string */
			g_assert(PLIsString(value));
			/*now we get the int */
			mc->iterations = atoi( PLGetString(value));
			break;
		    case STATISTICS:
			/* now we need to assure it's an array */
			g_assert(PLIsArray(value));
			/* and now, allocate the stats */
			//mc->stats = g_list_alloc();
			for (k= 0; k < PLGetNumberOfElements(value); k++) {
			  //printf("Viendo el elemento %d \n", k);
			  // stats = g_new(Stat,1);
			  stats = malloc(sizeof(Stat));
			    napkin_string = PLGetString(PLGetArrayElement(value, k));
			    stats->type = find_names_index(stat_names, napkin_string);
			    mc->stats = g_list_append (mc->stats, stats);
			}
			/* removing null elements */
			g_list_remove(mc->stats, NULL);
			break;
		}
	}
	/* Appending each Monte Carlo to the List */
	mc_list = g_list_append(mc_list, mc);    
	//printf("La lista mc_list es de %d nodos, estamos en el %d nodo\n", g_list_length(mc_list), g_list_index( mc_list, mc));
    }
	/* removing all NULL elements */
	g_list_remove( mc_list, NULL);
    return g_list_first(mc_list);
    
}

void read_config_opt(opto *o)
{
  
  eref=o->eref= find_float(dictionary_names[O_REF_ENERGY]);
   
  iopt=o->iopt=find_int (dictionary_names[O_COST_FUN]);
  iweight=o->iweight=find_int (dictionary_names[O_WEIGHT_AVG]);
  eclip=o->eclip=find_int (dictionary_names[O_CLIP_ENERGY]);
  emin=o->emin= find_float(dictionary_names[O_MIN_EL]);
  emax=o->emax= find_float(dictionary_names[O_MAX_EL]);
  wmin=o->wmin= find_float(dictionary_names[O_MIN_W]); 
  wmax=o->wmax= find_float(dictionary_names[O_MAX_W]); 
  ftol=o->ftol= find_float(dictionary_names[O_TOLERANCE]); 
  scale=o->scale= find_float(dictionary_names[O_SCALE]);
  maxiter=o->maxiter=find_int (dictionary_names[O_MAX_STEP]);
  printf("OPtimizacion\n");
  printf("eref %f\n", o->eref);
  printf("iopt %d\n", o->iopt);
  printf("iweight %d\n", o->iweight);
  printf("emin %f   emax %f\n", o->emin, o->emax);
  printf("wmin %f   wmax %f\n", o->wmin, o->wmax);
 printf("tol %f sca %f it %d \n",o->ftol,o->scale,o->maxiter);
  

 }


int read_config(simulation *s)
{
  float fgraph_high, fgraph_low, fdt;
  float fbox_x, fbox_y, fbox_z, finitial_spread, tun_a, tun_b, tun_h,
    fconv_factor;
  int i,mol_charge,nc;
  float f_gauss_t_alpha;

//**********************************************************************

  restart = 
    find_string_to_int(initial_position_names, dictionary_names[INITIAL_POSITION]);
  s->e->ini_walkers = find_int (dictionary_names[WALKERS]);
  s->e->num_walkers=s->e->ini_walkers;
  max_branch = find_int(dictionary_names[MAXIMUM_BRANCHES]);
  fconv_factor = find_float(dictionary_names[DT_CONVERGENCE_FACTOR]);
  finitial_spread = find_float(dictionary_names[GAUSSIAN_SPREAD_RANGE]);
 
  
  fdt = find_float(dictionary_names[TIME_STEP]);
  seed_fija = find_int(dictionary_names[RANDOM_NUMBER_GENERATOR_SEED]);
  
  dt_convergence_factor = (double) fconv_factor;

  dt = (double) fdt;
  initial_spread = (double) finitial_spread;

  
  //Type of simulation								      
  system_type = 
    find_string_to_int ( system_names, dictionary_names[SYSTEM]);
  
  switch (system_type) 
    {
    case EDU_SIM:
       s->e->dim = 
	 find_string_to_int (dimension_names, dictionary_names[DIMENSIONS]) + 1;
       s->e->num_pars = 
	 find_int (dictionary_names[PARTICLES]);
       potential_type = 
	 find_string_to_int ( potential_names, dictionary_names[POTENTIAL]);
       trial_function_type = 
	 find_string_to_int (trial_function_names,
			     dictionary_names[TRIAL_FUNCTION]);
       f_gauss_t_alpha = 
	 find_float(dictionary_names[GAUSSIAN_TRIAL_FUNCTION_ALPHA]);
       trial_alpha = (double) f_gauss_t_alpha;
       trial_alpha2 = trial_alpha * trial_alpha;
       if(potential_type== TUNNEL_POTENTIAL || potential_type==WELL_POTENTIAL)
	 {
	   
	   tun_a = find_float(dictionary_names[TUNNEL_POTENTIAL_A]);
	   tun_b = find_float(dictionary_names[TUNNEL_POTENTIAL_B]);
	   tun_h = find_float(dictionary_names[TUNNEL_POTENTIAL_C]);
	   fbox_x = find_float(dictionary_names[POTENTIAL_BOX_X]);
	   fbox_y = find_float(dictionary_names[POTENTIAL_BOX_Y]);
	   fbox_z = find_float(dictionary_names[POTENTIAL_BOX_Z]);
	   for (i = 0; i < s->e->dim; i++)
	     half_box[i] = 0.5;
	   half_box[0] = (double) fbox_x / 2;
	   half_box[1] = (double) fbox_y / 2;
	    half_box[2] = (double) fbox_z / 2;
	    
	    tunnel_a = (double) tun_a;
	    tunnel_b = (double) tun_b;
	    tunnel_h = (double) tun_h;
	 }
       break;
    case BTF_SIM:
      s->parameters->path = 
	my_pl_get_string(dictionary_names[PARAMETERS_PATH]);
      trial_function_type=CORRELATED_EXPONENTIAL;
      s->e->dim = 3;
      dimensions=3;
      parity = find_int(dictionary_names[SPIN_PARITY_OPERATOR]);
      s->e->ee_scale = find_float(dictionary_names[ELECTRONIC_REPULSION]);
      eescale=s->e->ee_scale ;

      // rulo_pl_get_matrix_size(dictionary_names[SYM_MATRIX],&nperm ,&num_pars);
      //s->e->num_pars=num_pars=num_pars-1;
      //if(nperm > MAX_PERM)
      //	{printf("To many permutations nperm= %d \n",nperm);exit(0);}
      //read_correlated_symmetry(s->e);

      molecule_type = find_int (dictionary_names[MOLECULE]);
      if(molecule_type==NORMAL)
	{
	  mol_charge= find_int (dictionary_names[MOL_CHARGE]);

	}
      nc=nucleus_charge();
    

      s->e->num_pars = num_pars=nc-mol_charge;
      
      break;
    case STO_SIM:
      trial_function_type =SLATER_TYPE_ORBITALS;
      s->e->dim = 3;
      printf("STO Trial Function no yet");
      exit(0);
      break;
      
    case CLUSTER_SIM:
       s->e->dim = 
	 find_string_to_int (dimension_names, dictionary_names[DIMENSIONS]) + 1;
       lehnard_jones_sigma = find_float(dictionary_names[LEHNARD_JONES_SIGMA]);
       lehnard_jones_epsilon = find_float(dictionary_names[LEHNARD_JONES_EPSILON]);
       printf("Clusters no yet");
       exit(0);
       break;
       
     case PATH_SIM:
       s->e->dim = 
	 find_string_to_int (dimension_names, dictionary_names[DIMENSIONS]) + 1;
       potential_type = 
	  find_string_to_int ( potential_names, dictionary_names[POTENTIAL]);
	 s->e->num_pars = 
	   find_int (dictionary_names[PARTICLES]);
       break;
     }
   //****************  MUERE  **********
   dimensions=s->e->dim;
   num_pars=s->e->num_pars;
  //******************************************
 
    //*************************************** Graficas  
  fgraph_high = find_float(dictionary_names[GRAPH_MAXIMUM]);
  fgraph_low = find_float(dictionary_names[GRAPH_MINIMUM]);
  graph_bins = find_int(dictionary_names[DIVS_PER_DIMENSION]);

 graph_bins = (int) pow((double) graph_bins, (1.0 / (double) s->e->dim));
  graph_high = (double) fgraph_high;
  graph_low = (double) fgraph_low;
  //**************************************  

  //  dynamic_init(s);
  init(s, find_int (dictionary_names[RANDOM_NUMBER_GENERATOR_SEED]));
    dynamic_init(s);
 
   





    return 1;
}


void bto_init(simulation * s)
{
  inter_nuclear_potential_init(s->e);
  read_correlated_psi(s);

  read_correlated_symmetry(s->e);

  read_par(s->parameters->path);
  make_perm(s->e);
  print_info(s->e);
 
}

void inter_nuclear_potential_init(ensemble * e ) {
  
  int i, A, B;
  gdouble temp1;
  walker * w;

  num_dee = (num_pars * (num_pars - 1)) / 2;
  num_deN = num_pars * num_nucleos;
  
  /* compute the nuclear potential energy, and store it */
  /* we get it from the first walker in the ensemble b/c this is 
	 just an initialization function. Probably we'll need a more general
	 inter_nuclear_potential that returns it for non adiabatic systems */
  e->interN_pot = 0.0;
  w = &e->walkers[0];

  for (A = 1; A < num_nucleos; A++)
	for (B = 0; B < A; B++) 
	  {
	    temp1 = 0.0;
	    for (i = 0; i < 3; i++)
	      temp1 += P2((w->nuc_R[A+i] - w->nuc_R[i+B]));
	    temp1 = sqrt(temp1);
	    e->interN_pot += w->nuc_Z[A] * w->nuc_Z[B] / temp1;
	  }
  for (i = 0; i < e->num_walkers ; i++)
    e->walkers[i].interNpot=e->interN_pot;


  printf("Internuclear potential %15.10f\n", e->interN_pot);

}

// Rulo asks
//Donde se lee emass, echarge y se asig. nmm_dee y num_deN ????????


/* read the permutational and space symmetry */


void read_correlated_symmetry(ensemble * e)
{
  /* 
  FIXME: We also need to change sign[i] and perm[i][j] to actual
  gsl_matrix_reads... mmh I think I will do it, it will be less
  dirty */

  gsl_matrix_int * temp_symmetry;

  //FILE *fp;
  unsigned int i, j;

  //fp = efopen("symmetry.dat", "r");

  //  fscanf(fp, "%d", &nperm);
  //  if (nperm > MAX_PERM)
  // error("nperm > MAXPERM");
  // size1 = symterms and size2 = numparticles+1
  
  temp_symmetry = my_pl_get_matrix_int(dictionary_names[SYM_MATRIX]);
 
 nperm=temp_symmetry->size1;
  g_assert (nperm < MAX_PERM);
  // and now callocing the symmetry and symmetry_signs matrix and vector
  if(parity!=0){
    symmetry = gsl_matrix_int_calloc(2*temp_symmetry->size1, (temp_symmetry->size2 ));
    symmetry_signs = gsl_vector_int_calloc(2*temp_symmetry->size1);
  }
  else{
    symmetry = gsl_matrix_int_calloc(temp_symmetry->size1, (temp_symmetry->size2 ));
    symmetry_signs = gsl_vector_int_calloc(temp_symmetry->size1);
  }

 for (i = 0; i < nperm; i++)
   {
     /* this is the sign column */
     gsl_vector_int_set(symmetry_signs,i,gsl_matrix_int_get(temp_symmetry, i , 0));
     /* the first column is the signs, the second (j=1 to number of particles
	is actually the matrix itself DOCUMENTME */
     for (j = 1; j < temp_symmetry->size2; j++) 
       {
	 /* setting the matrix is tricky, we have to remember it's displaced */
	 gsl_matrix_int_set(symmetry, i, j - 1, gsl_matrix_int_get(temp_symmetry, i, j));
       }
   }

   
}


/*------------------------ psi handling --------------------------------*/

/* define the structure of the wavefunction */

void read_correlated_psi(simulation *s)
{
  int jen;
  /* dummy parameter */
  simulation * sim;
  gsl_matrix_float * prefactors;
  double * masses;
  double * charges;


   int i;
   // FILE *fp;
  
  // fp = efopen(fname, "r");
  
  // nterm = fassign("nterm", fp);
  sim = s;

  nterm = my_pl_get_int(dictionary_names[CE_TERMS]);

    // jen = fassign("jen", fp);
    // jee = fassign("jee", fp);
  jee = find_string_to_int(electron_tf_names, dictionary_names[ELEC_ELEC_TF]);
  // FIXME: This should be cleaner 
  if (jee == NONE) jee= -1;
  jen = find_string_to_int(nuclei_tf_names, dictionary_names[NUCLEI_ELEC_TF]);
  
  
  
    //symee = fassign("symee", fp);
  //symen = fassign("symen", fp);
  symee = find_string_to_int(boolean_names, dictionary_names[SYM_ELEC_ELEC]);
   symen = find_string_to_int(boolean_names, dictionary_names[SYM_ELEC_NUCLEI]);
  //eNparam = fassign("numen", fp);
  eNparam = my_pl_get_int(dictionary_names[ELEC_NUCLEI_PARAMS]);

  /*    numee = fassign("numee",fp); */
  
  //numpr = fassign("numpr", fp);	/* prefactor parameters */
  numpr = my_pl_get_int(dictionary_names[CE_PREFACTOR_TERMS]);
	
  if (eNparam == 0)
    eNparam = (jen + 1) * num_deN;

  eeparam = 0;
  if (jee >= 0)
    eeparam = (jee + 1) * num_dee;
  
  numsym = 0;
  if (find_string_to_int(boolean_names,dictionary_names[SYM_ELEC_ELEC]))
    numsym += 2;
  if (find_string_to_int (boolean_names,dictionary_names[SYM_ELEC_NUCLEI]))
    numsym += 2;

    npar = eeparam + numpr + eNparam + numsym + 1;

    //find_target("prefactor", fp);
    //for (i = 0; i < nterm; i++)
    /* FIXME: type_pr should be a gsl_vector later */
    
    //fscanf(fp, "%d", &type_pr[i]);
    
    //fclose(fp);

if(molecule_type==NORMAL)
  {
    for (i = 0; i < num_pars; i++)
      {
	echarge[i] = (-1);
	emass[i] = 1;
      }
  }
 else
  {   
    charges = 
      pl_array_to_double(my_pl_get_array(dictionary_names[PARTICLE_CHARGES]));
    masses = 
      pl_array_to_double(my_pl_get_array(dictionary_names[PARTICLE_MASSES]));
    
    /* reading the particles charge*/
    
    for (i = 0; i < num_pars; i++)
      echarge[i] = charges[i];
    /* reading the particles masas */
    
    for (i = 0; i < num_pars; i++)
      emass[i] = masses[i];

  }

    /* reading the type_pr matrix */
      prefactors = my_pl_get_matrix_float (dictionary_names[CE_PREFACTORS]);
    for (i = 0; i < nterm; i++)
    type_pr[i] = gsl_matrix_float_get(prefactors, i, 0);
    
}


/* read the parameters of the wavefunction */
/* TODO, add prefactor parameters */

void read_par(char *fname)
{
    FILE *fp;
    int i, j, ien, ipr, iee, isym;
    
    

    fp = efopen(fname, "r");

    ien = iee = isym = ipr = 0;
    for (i = 0; i < nterm; i++) {
	fscanf(fp, "%lf", &plin[i]);
	for (j = 0; j < eNparam; j++)
	    fscanf(fp, "%lf", &parater_eN[ien++]);
	for (j = 0; j < numpr; j++)
	    fscanf(fp, "%lf", &ppr[ipr++]);

	for (j = 0; j < eeparam; j++)
	    fscanf(fp, "%lf", &parater_ee[iee++]);

	for (j = 0; j < numsym; j++)
	    fscanf(fp, "%lf", &psym[isym++]);
    }
    fclose(fp);
}


void print_info(ensemble * e)
{
    int i, j, iee, ien, ipr, isym;


    printf("-------------------------\n");
    for (i = 0; i < num_pars; i++) {
      
	printf("Particle %d  mass %f charge %f \n",i,emass[i],echarge[i]);
      }
      
      printf("Permutations: %d\n", nperm);
    printf("-------------------------\n");

    for (i = 0; i < nperm; i++) {
	  printf("% d   : ", gsl_vector_int_get(symmetry_signs, i));
	for (j = 0; j < num_pars; j++)
	    printf("%d ", gsl_matrix_int_get(symmetry, i, j));
	printf("\n");
    }


    printf("R(e-n) Transformations:\n");
    printf("-------------------------\n");

    for (i = 0; i < nperm; i++) {
	printf("%d : ", i);
	for (j = 0; j < num_deN; j++)
	    printf("%d ", permen[i][j]);
	printf("\n");
    }

    printf("R(e-e) Transformations:\n");
    printf("-------------------------\n");

    for (i = 0; i < nperm; i++) {
	printf("%d : ", i);
	for (j = 0; j < num_dee; j++)
	    printf("%d ", permee[i][j]);
	printf("\n");
    }


    printf("jen = %d\n", jen);
    printf("jee = %d\n", jee);
    printf("symee  = %d\n", symee);
    printf("symen  = %d\n", symen);
    printf("numen  = %d\n", eNparam);
    printf("numee  = %d\n", eeparam);
    printf("numpr  = %d\n", numpr);
    printf("numsym = %d\n", numsym);
    printf("parity = %d\n", parity);

    printf("Psi: %d terms, %d parameters each.\n", nterm, npar);
    printf("-------------------------\n");

    ien = iee = isym = ipr = 0;
    for (i = 0; i < nterm; i++) {

	printf("l(%d)  = %.16f    type: %d\n", i, plin[i], type_pr[i]);

	for (j = 0; j < eNparam; j++)
	    printf("en(%d) = %6.16f\n", j, parater_eN[ien++]);

	for (j = 0; j < numpr; j++)
	    printf("pr(%d) = %6.16f\n", j, ppr[ipr++]);

	for (j = 0; j < eeparam; j++)
	    printf("ee(%d) = %6.16f\n", j, parater_ee[iee++]);

	for (j = 0; j < numsym; j++)
	    printf("sy(%d) = % -f\n", j, psym[isym++]);
    }
    printf("-------------------------\n");

    printf("e %d\n", num_pars);
//    printf("dim  %d\n", dimensions);

}


/* generate the e-n and e-e permutations */

void make_perm(ensemble *e)
{
    int r[num_pars][num_pars];
    int i, j, k, n, jj, jn, t1, t2;

    /*
       ** here we want see how the vector of e-n distances transforms
       ** under the various permutations
     */

    for (n = 0; n < nperm; n++) {
      for (j = 0; j < num_pars; j++) {
	//jj = perm[n][j] * num_nucleos;
	  jj = gsl_matrix_int_get(symmetry, n, j) * num_nucleos;
	  jn = j * num_nucleos;
	  
	  for (k = 0; k < num_nucleos; k++)
	    permen[n][jn + k] = jj + k;
	  

	  
	}
    }

    /*
       ** now it is the turn of e-e distances
       ** fill the order of the distances: r[i][j] contains  the index
       ** The order is: r12,r13....r1n,r23,....rn-1n.
     */

    for (i = k = 0; i < num_pars - 1; i++)
	for (j = i + 1; j < num_pars; j++, k++)
	    r[i][j] = r[j][i] = k;

    for (n = 0; n < nperm; n++)
	for (i = k = 0; i < num_pars - 1; i++)
	    for (j = i + 1; j < num_pars; j++, k++) {
		  //t1 = perm[n][i];
		  //t2 = perm[n][j];
		  t1 = gsl_matrix_int_get(symmetry, n, i);
		  t2 = gsl_matrix_int_get(symmetry, n, j);
		permee[n][k] = r[t1][t2];
	    }

    /*
       ** now check for parity operator
       ** WORKS ONLY FOR 2 NUCLEOS
       ** generate the new distances exchanging the e-n distances
     */

    if (parity != 0) {
	for (i = 0; i < nperm; i++) {
	  //sign[i + nperm] = parity * sign[i];
	  gsl_vector_int_set(symmetry_signs ,      i + nperm       ,  parity * gsl_vector_int_get(symmetry_signs,i)              );
	  for (j = 0; j < num_pars; j++) {
		jj = j * num_nucleos;
		//perm[i + nperm][j] = perm[i][j];
		gsl_matrix_int_set(symmetry,        i + nperm,      j,           gsl_matrix_int_get(symmetry, i, j));
		permen[i + nperm][jj + 0] = permen[i][jj + 1];
		permen[i + nperm][jj + 1] = permen[i][jj + 0];
	    }
	    for (j = 0; j < num_dee; j++)
		permee[i + nperm][j] = permee[i][j];
	}
	nperm = 2 * nperm;
    }
}

void fill_walkers(ensemble * e,int bol_IS )
{
  double  epot;
  int i, n;

  e->energy=0;
  //for (i = 0; i < num_walkers; i++)
  //  printf("ppp %f %d\n",e->walkers[i].R[0],i);

  for (n = 0; n < e->num_walkers; n++)
    {
      
      //printf("213eee %d %f \n",n,e->walkers[n].R[0]);
      
      epot = (*V2)(&e->walkers[n]);
	  (*Derivs)(&e->walkers[n]);
    
  // exit(0);for (n = 0; n < num_walkers; n++)
		 // {
		 
		 if(bol_IS==ON)
		   {
			 e->walkers[n].Egy_Lcl = -0.5 * 
			   e->walkers[n].del2 / e->walkers[n].Psi + epot;
			 for (i = 0 ; i < ncoord; i++)
			   e->walkers[n].Force[i] =
				 2 * e->walkers[n].grad[i] / e->walkers[n].Psi;
		   }	
		 else
		   e->walkers[n].Egy_Lcl = epot;
		 
		 if (isnan(e->walkers[n].Egy_Lcl)){//for some reason the walker is fucked up
		   
		   if (n == 0)
			 {
			   
			 g_error("Fatal error: The first walker is fucked up, check your restart file or change the seed or \"hazte una chaqueta carnalito\"");
			 }
		   else {
			 g_warning("Walker no. %d was sick, copying the previous walker.",n);
			 copy_walker(&e->walkers[n-1],&e->walkers[n]);
		   }
		 }
		 e->energy = e->energy +  e->walkers[n].Egy_Lcl;
		 //printf ("*** %d %f \n",n, e->walkers[n].Egy_Lcl);
    }    
  e->energy = e->energy / ((double) e->num_walkers);
}


int read_gmoiss_from_file(char * filename, proplist_t  * dict) {
    int i;
    unsigned char * charalito;
  
    i = my_pl_open_call (dict, filename);
    switch (i) {
	case OPEN_NOT_VALID:
	    
	    g_error("The file is not a valid calculation file \n Please feed me a valid input file.");
	    break;
	case OPEN_NOT_GMOISS:
	    g_error("The file appears to be a GNUStep Properties file, but it's not in the valid GMOISS format.");
	    break;
	case OPEN_OK:
	    /*FIXME debugging */
	    if (!dict) {
		    g_error("Dict is null it shouldn't be. Call your nearest customer service representative.");
	    }

	    charalito = PLGetDescriptionIndent(*dict, 4);
	    // printf("File loaded in init.c: \n %s ", charalito);
	    free(charalito);

	    /*end of debugging */
	    break;
	case OPEN_NOT_CORRECT_VERSION:
	    
	    break;



	default:
	    g_error("Unexpected error while loading. Contact your nearest service representative, 1-800-CALL-MOISS.");
    }
	return i;

}

void read_gmoiss_from_file_no_pl_c (char * input_file){
    /* This is done while we fix the other read_gmoiss_from_file
       and my_pl_open_dict (EMERGENCY for the CONGRESS) */
    char * charalito;
    
    calculation_dict = PLGetProplistWithPath(input_file);
    
    charalito = PLGetDescriptionIndent(calculation_dict, 4);
    // printf("File loaded in init.c: \n %s\n", charalito);
    free(charalito);
   
}

int nucleus_charge(void)
{
  int elem,i,nc;
  proplist_t key, array, key_Z,value_Z,element;
  key = PLMakeString(dictionary_names[ATOMIC_COORDINATES]);
  array = PLGetDictionaryEntry(calculation_dict, key);
  elem= PLGetNumberOfElements(array);
  nc=0;
 
 for (i = 0; i < elem; i++) {
    element = PLGetArrayElement(array, i);
    key_Z = PLMakeString(atomic_coordinates[ATOMIC_NUMBER]);
    value_Z = PLGetDictionaryEntry(element, key_Z);
    nc+=atof(PLGetString(value_Z));
  }
return nc;
  }

void read_nucleos(walker * w)
{
  int elements,i;
  char * charalito;
  proplist_t key, array, key_x, key_y, key_z, key_Z;
  proplist_t value_x, value_y, value_z, value_Z, element;
  float x,y,z,Z;

  key = PLMakeString(dictionary_names[ATOMIC_COORDINATES]);
  
  array = PLGetDictionaryEntry(calculation_dict, key);
  charalito = PLGetDescriptionIndent(array, 4);

  elements = PLGetNumberOfElements(array);

  
  free(charalito);

  w->nuc_R = (double *) malloc(elements*3 * sizeof(double));
  if (w->nuc_R == NULL)
    g_error("No memory for the nuclei array!");
  w->nuc_Z = (double *) malloc(elements * sizeof(double));
  if (w->nuc_Z == NULL)
    g_error("No memory for the nuclei array!");
  


  num_nucleos=elements;
  w->num_nucleos=elements;
  

  for (i = 0; i < elements; i++) {
    
    element = PLGetArrayElement(array, i);
    key_x = PLMakeString(atomic_coordinates[ATOMIC_X]);
    key_y = PLMakeString(atomic_coordinates[ATOMIC_Y]);
    key_z = PLMakeString(atomic_coordinates[ATOMIC_Z]);
    key_Z = PLMakeString(atomic_coordinates[ATOMIC_NUMBER]);
    
    value_x = PLGetDictionaryEntry(element, key_x);
    value_y = PLGetDictionaryEntry(element, key_y);
    value_z = PLGetDictionaryEntry(element, key_z);
    value_Z = PLGetDictionaryEntry(element, key_Z);
    
    x= atof(PLGetString(value_x));
    y= atof(PLGetString(value_y));
    z= atof(PLGetString(value_z));
    Z= atof(PLGetString(value_Z));
    w->nuc_R[i]   = x;
    w->nuc_R[i+1] = y;
    w->nuc_R[i+2] = z;
    w->nuc_Z[i]=Z;
    PLRelease(key_x);
    PLRelease(key_y);
    PLRelease(key_z);
    PLRelease(key_Z);
  }
}



void read_walkers(simulation *s)
{
  int i,j,k,l,checa;
  float fdumb;
  
  if ((s->restart->fp = fopen(s->restart->path, "r")) == NULL) 
    {
      printf("I can't open the file %s \n", s->restart->path);
      exit(1);
    }
  fscanf(s->restart->fp,"%d",&checa);
  if(checa!=s->e->ini_walkers)
    if(checa>(int)(s->e->ini_walkers * mem_extra*0.85))
      {
	printf("Too many walkers in the file\n");
      }
    else{
      g_warning("The number of walker (%d) in file %s isn't the same as the walkers line in the input file (%d) ", checa, s->restart->path, s->e->ini_walkers);
    }
  printf("Reading the walkers \n" );
  for (i = 0; i < checa; i++) {
    for (j = 0,l=0; j < s->e->num_pars; j++) 
      for (k = 0; k < s->e->dim; k++,l++)
	{
	  fscanf(s->restart->fp,"%f",&fdumb);
	  s->e->walkers[i].R[l]=(double)fdumb; 
	}
  }
  fclose(s->restart->fp);
  
}







