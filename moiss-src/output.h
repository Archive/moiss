/* This file is part of MOISS
   
   Copyright (C) 1998-99 Alan Aspuru Guzik
			 Raul Perusquia Flores
			 Carlos Amador Bedolla
			 Francisco Bustamante
			 Dario Bressanini
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
               
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
                           
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
                                    
*/

#ifndef __OUTPUT_H__
#define __OUTPUT_H__

void plotvec (double pvector[], int components);
void escribir (simulation * s, int tblk);
void escriir_no ( simulation *s, int file_number, int num_blocks, int num_steps);
void graph_walkers (ensemble *  e, file * walker);
void grafica_fun_onda (int norm_steps, int norm_bin,  simulation * s);
void write_walkers(ensemble * e);
void write_psi_par(char *fname);

#endif /* __OUTPUT_H__ */





