/*
   ** opt.c
   **
   **   (C) Copyright Dario Bressanini
   **   Dipartimento di Scienze Matematiche Fisiche e Chimiche 
   **   Universita' dell'Insubria, sede di Como
   **   E-mail: dario@fis.unico.it
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <math.h>
#include <glib.h>
#include "moiss.h"
#include "mymath.h"
#include "proto.h"
#include "bto.h"
#include "../gmoiss-src/pl.h"
#include "stats.h"
#include "optimiza.h"
#include "init.h"
#include "powell.h"
#include <errno.h>
#include <string.h>
#define MAXDIM 3
#define MAXPAR	100
#define MAXELE 5
#define MAXWALKS 20000


//static double vv[MAXCOORD * MAXWALKS];
static double psi[MAXWALKS];
static double epot[MAXWALKS];
static double eloc[MAXWALKS];




/*--- Optimization section ---*/
/*
static int iopt;	
static int iweight;	
static int eclip;	
static double eref;	
static double emin, emax;	
static double wmin, wmax;
static int maxiter;		
static double ftol;		
static double scale;		
*/
static double e0, s20, sm20, weff0;



void optima(opto *o,ensemble * ens)
{
  double p[MAXPAR];
  int i, l_npar;
  double fmin;
  int j, k, ien, ipr, iee, isym,ii;
  ien = iee = isym = ipr = 0;
  k = 0;


 for (i = 0; i < ens->num_walkers; i++) 
    {
      epot[i]=(*V2)(&ens->walkers[i]); 
      (*Derivs)(&ens->walkers[i]); 
      //  printf("######## %f \n",ens->walkers[i].Psi);
      psi[i]= ens->walkers[i].Psi;
    }
 //  for (ii=0;ii<6;ii++)
 //  printf("########  %f \n",psi[ii]);

 
  
  printf("****************** %d \n",o->maxiter);
  
  //  npar = get_psi(p);		/* Ok, Ok,  no error check. So what? */
  
  l_npar=npar*nterm;
  for (i = 0; i < nterm; i++) 
    {
      p[k++] = plin[i];
      
      for (j = 0; j < eNparam; j++)
	p[k++] =parater_eN[ien++];
      
      for (j = 0; j < numpr; j++)
	p[k++] = ppr[ipr++];
      
      for (j = 0; j < eeparam; j++)
	p[k++] = parater_ee[iee++];
      for (j = 0; j < numsym; j++)
	p[k++] = psym[isym++];
    }
  printf("npar = %d\n", l_npar);
  for (i = 0; i < l_npar; i++)
    printf("p[%d]=%f\n", i, p[i]);
 
  powell(cost,ens, p, l_npar, o->ftol, o->scale, o->maxiter, &fmin);
  
  
 for (i = 0; i < l_npar; i++)
    printf("p[%d]=%f\n", i, p[i]);


 
  
  


}

/*
   ** this function evaluate the 'cost' of the set p[] of wavefunction
   ** parameters
   ** The 'cost' can be <H>, S2(H), Mu2(H) ....
 */

double cost(double p[],ensemble * ens)
{

    double grad[MAXCOORD];
    double w, wtot, el, val, del2, e, e2;
    double w2,eng_v;
    int i,j, k, ien, ipr, iee, isym,ii; 
    //change_psi(p);		/* tell psi.c to use a new function */


    ien = iee = isym = ipr = 0;
    k = 0;
    for (i = 0; i < nterm; i++) 
    {
      plin[i] = p[k++];
      for (j = 0; j < eNparam; j++)
	parater_eN[ien++] = p[k++];       /* -abs() is enforced later */
      for (j = 0; j < numpr; j++)
	ppr[ipr++] = p[k++];
        for (j = 0; j < eeparam; j++)
	  parater_ee[iee++] = p[k++];
        for (j = 0; j < numsym; j++)
	  psym[isym++] = p[k++];
    }
    


    e = e2 = wtot = w2 = 0.0;
    for (i = 0; i < ens->num_walkers; i++) 
      {
	//j = i * ncoord;
		 eng_v = (*V2)(&ens->walkers[i]);  
	//get_del(&vv[j], &val, &del2, grad);	/* pass address of i-th walker */
	(*Derivs)(&ens->walkers[i]);
	 //el = -0.5 * del2 / val + epot[i];
	//for (ii=0;ii<6;ii++)
	//  printf("%f \n",psi[ii]);
	el=-0.5 * ens->walkers[i].del2 / ens->walkers[i].Psi + eng_v;
	
	if (iweight == 0)
	  w = 1.0;
	else
	  // w = (val * val) / (psi[i] * psi[i]);
	  w= (ens->walkers[i].Psi *ens->walkers[i].Psi)/(psi[i]*psi[i]);
	    
	    if (w > wmax)
	  w = wmax;
	else if (w < wmin)
	  w = wmin;
	
#if 0
	if (el < emin || el > emax)
	  w = 0.0;
	if (eclip > 0)
	  if (el > emax)
	    el = emax;
	  else if (el < emin)
	    el = emin;
#endif
	
	wtot += w;
	w2 += w * w;
	e += w * el;
	e2 += w * el * el;
      }
    e /= wtot;
    e2 /= wtot;
    wtot /= ens->num_walkers;
    w2 /= ens->num_walkers;

    e0 = e;
    s20 = e2 - e * e;
    sm20 = s20 + (e - eref) * (e - eref);
    weff0 = wtot * wtot / w2;	/* this measure the efficiency */

#if 0
    printf("C %.8f %f %f \n", sm20, wtot, (wtot * wtot) / w2);
#endif

    if (iopt == 0)
	return e0;
    else if (iopt == 1)
	return s20;
    else
	return sm20;
}

/* transmit to caller the 'status' of the optimization */

void weight(double *e, double *s2, double *sm2, double *weff)
{
    *e = e0;
    *s2 = s20;
    *sm2 = sm20;
    *weff = weff0;
}




