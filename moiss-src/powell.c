/*
   ** FILE
   **   powell.c - powell's conjugate directions minimization method
   **
   ** DESCRIPTION
   **   performs a minimization of a multidimensional user supplied function
   **
   ** USES
   **   linmin.c
   **
   ** v 1.7  19 Sep. 1997
   **   STATIC ARRAY VERSION
   **   Added maxiter parameter
   **
   **   (C) Copyright Dario Bressanini
   **   Dipartimento di Scienze Matematiche Fisiche e Chimiche 
   **   Universita' dell'Insubria, sede di Como
   **   E-mail: dario@fis.unico.it
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include "moiss.h"
#include "mymath.h"


int linmin(PFNe f,ensemble * ens, double p[], double dirv[], int n, double *fmin);
void weight(double *e, double *s2, double *sm2, double *weff);

/*---------------------------------------------------------------------------*/

#define MAXDIM		100

static double pold[MAXDIM];
static double pextrap[MAXDIM];
static double new_dir[MAXDIM];
static double dir[MAXDIM][MAXDIM];

/* n is the number of dimensions */
/* dir is the matrix of column directions vectors */

int powell(PFNe f,ensemble * ens, double p[], int n, double tol,
	   double scale, int maxiter, double *fmin)
{
    double new_value, cur_value, prev_value, trial_value;
    double max_decr;		/* max. function decrease */
    double tmp;
    int i, j, iter, flag;
    int go_down;		/* direction of descent */

    double e, s2, sm2, weff;	/* used for weight() */

/*    tol = max(tol, SQRT_DBL_EPSILON); */

 

    if (n > MAXDIM) {
	printf("***ERROR:powell: Too many parameters. Max = %d\n", MAXDIM);
	exit(-1);
    }
    for (i = 0; i < n; i++)	/* set up scale lenght */
	for (j = 0; j < n; j++)
	    dir[i][j] = ((i == j) ? scale : 0.0);

    new_value = f(p,ens);		/* compute initial value */
    for (i = 0; i < n; i++)	/* save the old point */
	pold[i] = p[i];
    
  
    
    for (iter = 0; iter < maxiter; iter++) 
      {

	go_down = -1;		/* impossible direction */
	max_decr = 0.0;
	cur_value = new_value;
	
	/*
	** now loop over the direction set and try to minimize along each one
	*/

	for (i = 0; i < n; i++) 
	  {

	    for (j = 0; j < n; j++)
	      new_dir[j] = dir[j][i];
	    prev_value = new_value;
	    flag = linmin(f,ens, p, new_dir, n, &new_value);
//	    printf("%d %d  \n",flag,M_FOUND);exit(0);
	     if (flag != M_FOUND)
	       // printf("dir %d ener %.13g\n", i, new_value);
	       1;
	     else if (prev_value - new_value > max_decr) 
	      {
		 max_decr = prev_value - new_value;
		 go_down = i;
	      }
	     weight(&e, &s2, &sm2, &weff);
	   //  printf("% d %.6f   %.8f  %.8f  %.8f  %.5f\n",
		  //  i, new_value, e, s2, sm2, weff);
	  }

       


	if (2 * abs(cur_value - new_value) <= tol * (abs(cur_value) + abs(new_value))) {
	    *fmin = new_value;
	    return M_FOUND;	/* found minimum. p[] contains the point */
	}
	if (go_down == -1) {
	    printf("***ERROR, go_down = -1\n");
	    printf("cur_value %f new_value %f max_decr %f\n", cur_value,
		   new_value, max_decr);
	    printf("check %f <= %f\n", 2 * abs(cur_value - new_value),
		   tol * (abs(cur_value) + abs(new_value)));
	    exit(1);
	}
	/*
	 * construct the extrapolated point and the average direction moved
	 */
	for (i = 0; i < n; i++) {
	    pextrap[i] = 2 * p[i] - pold[i];	/* extrapolated point */
	    new_dir[i] = p[i] - pold[i];
	    pold[i] = p[i];
	}
	trial_value = f(pextrap,ens);
	if (trial_value >= cur_value)	/* don't use new direction */
	    continue;		/* jump to next iteration */

	tmp = (cur_value - new_value - max_decr) * (cur_value - new_value - max_decr);
	tmp = tmp * 2 * (cur_value - 2 * new_value + trial_value);
	tmp = tmp - max_decr *
	    (cur_value - trial_value) * (cur_value - trial_value);
	if (tmp < 0.0) {	/* good direction */
	    flag = linmin(f,ens, p, new_dir, n, &new_value);	/* move along it */
	    for (i = 0; i < n; i++)	/* update dir */
		dir[i][go_down] = new_dir[i];
	}
#if 1
	printf("E %.16f\n", new_value);
	for (i = 0; i < n; i++)	/* save the old point */
	    printf("   %- f\n", p[i]);
#endif
    }
    /* performed maxiter iterations. Assume not found */

    *fmin = new_value;
    return M_NOT_FOUND;
}

/*-------------------------------------------------------------------*/

#ifdef TEST
#include <math.h>

double test1(double x[])
{
    return -sin(x[0] / 2) * sin(x[1] / 2);
}


int main()
{
    double fmin;

    double p[10];

    p[0] = p[1] = 0.1;
    powell(test1, p, 2, 1e-8, 0.1, &fmin);
    printf("x1 % .13f x2 % .13f\n", p[0], p[1]);
    exit(1);
}

#endif














