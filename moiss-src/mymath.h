/* This file is part of MOISS
   
   Copyright (C) 1998-99 Alan Aspuru Guzik
			 Raul Perusquia Flores
			 Carlos Amador Bedolla
			 Francisco Bustamante
			 Dario Bressanini
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
               
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
                           
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
                                    
*/

#ifndef __MYMATH_H__
#define __MYMATH_H__


/*
** mymath.h - useful math related definitions, macros and enums
**
**  6 jun 1997
**	included definitions for min, max, abs and sign
**	eliminated mcode, use define (back to the origin)
*/

#ifndef MYMATH_H
#define MYMATH_H

#define SQRT_DBL_EPSILON   1e-8		/* should be >=< sqrt(DBL_EPSILON) */


#define    M_OK  	0
#define    M_FOUND  	0
#define    M_NOT_FOUND	1
#define    M_FAILURE	2
#define    M_MAX_ITER	3
#define    M_LAST_CODE	4


#undef abs
#define abs(x)		((x)>=0  ? (x) : -(x))

#undef max
#define max(a,b)        ((a)<(b) ? (b) : (a))	/* WARNING: side effects */

#undef min
#define min(a,b)        ((a)>(b) ? (b) : (a))

#undef sign
#define sign(x)		((x) > 0 ? 1.0 : ((x) < 0 ? -1.0 : 0.0 ))


/* useful definitions for functions */

typedef double (*FUNC)(double);			/* f: R -> R */
typedef double (*FUNCe)(double, ensemble *);		      
typedef double (*PF)(double);			/* f: R -> R */
typedef double (*PF2)(double, double);		/* f: R^2 -> R */
typedef double (*PFN)(double *);		/* f: R^N -> R */
typedef double (*PFNe)(double *, ensemble *);		/* f: R^N -> R */

#endif /* MYMATH_H */

#endif /* __MYMATH_H__ */






