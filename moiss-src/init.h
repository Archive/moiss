/* This file is part of MOISS
   
   Copyright (C) 1998-99 Alan Aspuru Guzik
			 Raul Perusquia Flores
			 Carlos Amador Bedolla
			 Francisco Bustamante
			 Dario Bressanini
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
               
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
                           
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
                                    
*/

#ifndef __INIT_H__
#define __INIT_H__

#include <gsl/gsl_rng.h> 

int read_gmoiss_from_file(char * filename, proplist_t * dict);
void read_gmoiss_from_file_no_pl_c();
void dynamic_init (simulation * s);
void init (simulation * s, int randseed);
void read_config_opt(opto *o);
int read_config (simulation * s);              
void init_slater (void);
void bto_init(simulation * s);
void read_system(walker *w);
void read_correlated_symmetry(ensemble *e);
void read_correlated_psi(simulation * s);
void read_par(char *fname);
void print_info(ensemble * e);
void make_perm(ensemble * e);
void fill_walkers(ensemble * e,int bol_IS);
int nucleus_charge(void);
void read_nucleos(walker * w);
void inter_nuclear_potential_init(ensemble * e);
float find_float (char * name);
int find_int (char * name);
void read_walkers(simulation *s);

/* Receives a mc_pl and transforms it into a MonteCarlo GList */
GList * mc_pl_to_list(proplist_t mc_pl);

#endif /* __INIT_H__ */


