#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include "moiss.h"
#include "method.h"
#include "gui.h"

void slater_phi_deriv(walker * w, double **psi, double **dpsix, double **dpsiy
		     ,double **dpsiz, double **ddpsi, double interN_pot)
{
    int i, j, A = 0, dim, basis = 0;
    double temp1 = 0, ex;

    double dNe[3][num_nucleos][num_pars];
    double dee[3][num_pars][num_pars];

    float jstrw_a;

    hamiltonian_V = 0.0;
    for (i = 0; i < num_pars; i++) {
	laplc_jastrw[i] = 0.0;
	for (j = 0; j < 3; j++)
	    gradjstrw[j][i] = 0.0;
    }

    /* Primero calculamos las distancias electron-electron */

    for (i = 1; i < num_pars; i++)
	for (j = 0; j < i; j++) {
	    temp1 = 0.0;
	    for (dim = 0; dim < 3; dim++) {
		dee[dim][i][j] = w->pars[i].R[dim] - w->pars[j].R[dim];
		temp1 += P2(dNe[dim][A][j]);
	    }
	    distee[i][j] = sqrt(temp1);
	    distee[j][i] = distee[i][j];
	    hamiltonian_V += 1.0 / distee[i][j];
	}

    for (A = 0; A < num_nucleos; A++) {
	/* Aqui se calculan las diatancias Nucleo-electron  */
	for (j = 0; j < num_pars; j++) {
	    temp1 = 0.0;
	    for (dim = 0; dim < 3; dim++) {
		dNe[dim][A][j] = w->pars[j].R[dim] - nucleo[A].R[dim];
		temp1 += P2(dNe[dim][A][j]);
	    }
	    disteN[A][j] = sqrt(temp1);
	    hamiltonian_V -= nucleo[A].Z / disteN[A][j];
	}

	/* Ahora calculamos las funciones de onda, su gradiente y 
	   su laplaciano  */

	/*  orbitales 1s   */

	for (i = 0; i < nucleo[A].n1s; i++) {
	    basis++;
	    for (j = 0; j < num_pars; j++) {
		ex = exp(-zeta[basis] * disteN[A][j]);
		psi[j + num_up][basis] = C1s[basis] * ex;
		temp1 = -C1s[basis] * zeta[basis] * ex / disteN[A][j];
		dpsix[j + num_up][basis] = temp1 * dNe[0][A][j];
		dpsiy[j + num_up][basis] = temp1 * dNe[1][A][j];
		dpsiz[j + num_up][basis] = temp1 * dNe[2][A][j];
		ddpsi[j + num_up][basis] = temp1 * (2.0 - zeta[basis] * disteN[A][j]);
	    }
	    distNe[A][j] = sqrt(temp1);
	    hamiltonian_V -= nucleo[A].Z / distNe[A][j];
	}

	/* orbitales 2s  */

	for (i = 0; i < nucleo[A].n2s; i++) {
	    basis++;
	    for (j = 0; j < num_pars; j++) {
		ex = disteN[A][j] * exp(-zeta[basis] * disteN[A][j]);
		psi[j][basis] = C1s[basis] * ex;
		temp1 = -C1s[basis] * zeta[basis] * ex / disteN[A][j];
		dpsix[j + num_up][basis] = temp1 * dNe[0][A][j];
		dpsiy[j + num_up][basis] = temp1 * dNe[1][A][j];
		dpsiz[j + num_up][basis] = temp1 * dNe[2][A][j];
		ddpsi[j + num_up][basis] = temp1 * (2.0 - zeta[basis] * disteN[A][j]);
	    }
	}

	/* orbitales 2s  */

	for (i = 0; i < nucleo[A].n2s; i++) {
	    basis++;
	    for (j = 0; j < num_pars; j++) {
		ex = distNe[A][j] * exp(-zeta[basis] * distNe[A][j]);
		psi[j][basis] = C1s[basis] * ex;
		temp1 = -C1s[basis] * zeta[basis] * ex / distNe[A][j];
		dpsix[j + num_up][basis] = temp1 * dNe[0][A][j];
		dpsiy[j + num_up][basis] = temp1 * dNe[1][A][j];
		dpsiz[j + num_up][basis] = temp1 * dNe[2][A][j];
		ddpsi[j + num_up][basis] = temp1 * (2.0 - zeta[basis] * distNe[A][j]);
	    }
	}
    }
    hamiltonian_V += interN_pot;

    /* Funciones de correlacion tipo Pade-Jastrow  */

    temp1 = 0.0;
    jastrow = 0.0;
    for (i = 1; i < num_pars; i++)
	for (j = 0; j < i; j++) {
	    if (i >= num_up && j < num_up)
		jstrw_a = 0.5;
	    else
		jstrw_a = 0.25;
	    temp1 = (1.0 + (jstrw_b * distee[i][j]));
	    jastrow += (jstrw_a * distee[i][j]) / temp1;
	    for (dim = 0; dim < 3; dim++)
		gradjstrw[dim][i] += (dee[dim][i][j] * jstrw_a) / (distee[i][j] * P2(temp1));
	    laplc_jastrw[i] += (jstrw_a * (2.0 * INV(distee[i][j]) * temp1 + jstrw_a)) / P4(temp1);

	}
    jastrow = exp(temp1);

    for (i = 0; i < num_pars; i++)
	for (j = i + 1; j < num_pars; j++) {
	    if (j >= num_up && i < num_up)
		jstrw_a = 0.5;
	    else
		jstrw_a = 0.25;
	    temp1 = (1.0 + (jstrw_b * distee[i][j]));
	    for (dim = 0; dim < 3; dim++)
		gradjstrw[dim][i] += (dee[dim][i][j] * jstrw_a) / (distee[i][j] * P2(temp1));
	    laplc_jastrw[i] += (jstrw_a * (2.0 * INV(distee[i][j]) * temp1 + jstrw_a)) / P4(temp1);
	}
}
/* FIXME: we need to change this function prototype for receiving an ensemble
   for the interN_pot */
void slater_force(walker * w)
{
    int i, j, k, det, dstp = 0;
    double vec1[num_up][num_up], vec2[num_up][num_up];
    double vec3[num_up][num_up], vec4[num_up][num_up];
    double vec5[num_up][num_up], vec10[num_up][num_up];
    double vec6[num_pars][num_basis], vec7[num_pars][num_basis];
    double vec8[num_pars][num_basis], vec9[num_pars][num_basis];
    double vec0[num_pars][num_basis];
    double vec11[num_pars][3];
    double laplacn_opsi[num_pars];

    double **slater_mtx, **islater_mtx, **sltrm_dx, **sltrm_dy, **sltrm_dz,
    **sltrm_dd;
    double **slater_psi, **dslater_psix, **dslater_psiy;
    double **dslater_psiz, **ddslater_psi, **grad_opsi;
    double d, laplacianterm, dot_psi_jstrw;

    islater_mtx = (double **) malloc((unsigned) num_up * sizeof(double *));
    slater_mtx = (double **) malloc((unsigned) num_up * sizeof(double *));
    sltrm_dx = (double **) malloc((unsigned) num_up * sizeof(double *));
    sltrm_dy = (double **) malloc((unsigned) num_up * sizeof(double *));
    sltrm_dz = (double **) malloc((unsigned) num_up * sizeof(double *));
    sltrm_dd = (double **) malloc((unsigned) num_up * sizeof(double *));
    slater_psi = (double **) malloc((unsigned) num_pars * sizeof(double *));
    dslater_psix = (double **) malloc((unsigned) num_pars * sizeof(double *));
    dslater_psiy = (double **) malloc((unsigned) num_pars * sizeof(double *));
    dslater_psiz = (double **) malloc((unsigned) num_pars * sizeof(double *));
    ddslater_psi = (double **) malloc((unsigned) num_pars * sizeof(double *));
    grad_opsi = (double **) malloc((unsigned) num_pars * sizeof(double *));

    for (i = 0; i < num_pars; i++) {
	slater_psi[i] = vec6[i];
	dslater_psix[i] = vec7[i];
	dslater_psiy[i] = vec8[i];
	dslater_psiz[i] = vec9[i];
	ddslater_psi[i] = vec0[i];
	grad_opsi[i] = vec11[i];
    }
    for (i = 0; i < num_up; i++) {
	slater_mtx[i] = vec1[i];
	sltrm_dx[i] = vec2[i];
	sltrm_dy[i] = vec3[i];
	sltrm_dz[i] = vec4[i];
	sltrm_dd[i] = vec5[i];
	islater_mtx[i] = vec10[i];
	grad_opsi[i] = vec11[i];
    }

	/*FIXME, make this work with system, ensemble, etc. */
	/* when it's done, uncomment the line below */
    //slater_phi_deriv(w, slater_psi, dslater_psix, dslater_psiy, dslater_psiz, ddslater_psi, s->e->interN_pot);

    for (i = 0; i < num_pars; i++) {
	for (j = 0; j < 3; j++)
	    grad_opsi[i][j] = 0.0;
	laplacn_opsi[i] = 0.0;
    }

    for (det = 0; det < num_det; det++, dstp = (det - 1) * num_det) {

	for (i = 0; i < num_up; i++)
	    for (j = 0; j < num_up; j++) {
		slater_mtx[i][j] = 0.0;
		sltrm_dx[i][j] = 0.0;
		sltrm_dy[i][j] = 0.0;
		sltrm_dz[i][j] = 0.0;
		sltrm_dd[i][j] = 0.0;
	    }

	for (i = 0; i < num_up; i++)
	    for (j = 0; j < num_up; j++)
		for (k = 0; k < num_basis; k++) {
		    slater_mtx[i][j] += const_norm[i + dstp][k] * slater_psi[j][k];
		    sltrm_dx[i][j] += const_norm[i + dstp][k] * dslater_psix[j][k];
		    sltrm_dy[i][j] += const_norm[i + dstp][k] * dslater_psiy[j][k];
		    sltrm_dz[i][j] += const_norm[i + dstp][k] * dslater_psiz[j][k];
		    sltrm_dd[i][j] += const_norm[i + dstp][k] * ddslater_psi[j][k];
		}

	inv_matrix(slater_mtx, num_up, &d, islater_mtx);

	for (i = 0; i < num_up; i++)
	    for (j = 0; j < num_up; j++) {
		grad_opsi[0][i] += sltrm_dx[j][i] * islater_mtx[j][i];
		grad_opsi[1][i] += sltrm_dy[j][i] * islater_mtx[j][i];
		grad_opsi[2][i] += sltrm_dz[j][i] * islater_mtx[j][i];
		laplacn_opsi[i] += sltrm_dd[j][i] * islater_mtx[j][i];
	    }

	for (i = 0; i < num_up; i++)
	    for (j = 0; j < num_up; j++) {
		slater_mtx[i][j] = 0.0;
		sltrm_dx[i][j] = 0.0;
		sltrm_dy[i][j] = 0.0;
		sltrm_dz[i][j] = 0.0;
		sltrm_dd[i][j] = 0.0;
	    }

	for (i = 0; i < num_down; i++)
	    for (j = 0; j < num_down; j++)
		for (k = 0; k < num_basis; k++) {
		    slater_mtx[i][j] += const_norm[i + num_up + dstp][k] * slater_psi[j + num_up][k];
		    sltrm_dx[i][j] += const_norm[i + num_up + dstp][k] * dslater_psix[j + num_up][k];
		    sltrm_dy[i][j] += const_norm[i + num_up + dstp][k] * dslater_psiy[j + num_up][k];
		    sltrm_dz[i][j] += const_norm[i + num_up + dstp][k] * dslater_psiz[j + num_up][k];
		    sltrm_dd[i][j] += const_norm[i + num_up + dstp][k] * ddslater_psi[j + num_up][k];
		}

	inv_matrix(slater_mtx, num_down, &d, islater_mtx);

	for (i = 0; i < num_down; i++)
	    for (j = 0; j < num_down; j++) {
		grad_opsi[0][i + num_up] += sltrm_dx[j][i] * islater_mtx[j][i];
		grad_opsi[1][i + num_up] += sltrm_dy[j][i] * islater_mtx[j][i];
		grad_opsi[2][i + num_up] += sltrm_dz[j][i] * islater_mtx[j][i];
		laplacn_opsi[i + num_up] += sltrm_dd[j][i] * islater_mtx[j][i];
	    }

    }
    laplacianterm = 0.0;

    for (i = 0; i < num_pars; i++) {
	if (i < num_up) {
	    w->pars[i].Force[0] = grad_opsi[0][i] + gradjstrw[0][i];
	    w->pars[i].Force[1] = grad_opsi[1][i] + gradjstrw[1][i];
	    w->pars[i].Force[2] = grad_opsi[2][i] + gradjstrw[2][i];
	    dot_psi_jstrw = 0.0;
	    for (j = 0; j < dimensions; j++)
		dot_psi_jstrw += grad_opsi[j][i] * gradjstrw[j][i];
	    laplacianterm += laplacn_opsi[i] + laplc_jastrw[i] + 2.0 * dot_psi_jstrw;
	} else {
	    w->pars[i].Force[0] = grad_opsi[0][i] + gradjstrw[0][i];
	    w->pars[i].Force[1] = grad_opsi[1][i] + gradjstrw[1][i];
	    w->pars[i].Force[2] = grad_opsi[2][i] + gradjstrw[2][i];
	    dot_psi_jstrw = 0.0;
	    for (j = 0; j < dimensions; j++)
		dot_psi_jstrw += grad_opsi[j][i] * gradjstrw[j][i];
	    laplacianterm += laplacn_opsi[i] + laplc_jastrw[i] + 2.0 * dot_psi_jstrw;

	}
    }
    E_local = hamiltonian_V - D * laplacianterm;

}
