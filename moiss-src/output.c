#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include "moiss.h"
#include "output.h"
#include "gui.h"
#include "proto.h"

#define MAX_FILENAME_SIZE 255

void plotvec(double pvector[], int components)
{
    int i;
    FILE *file_pvector;
    if ((file_pvector = fopen("vareng", "wb")) == NULL) {
	printf("I can't create the file veng  \n");
	exit(1);
    }
    for (i = 0; i < components; i++) {
	fprintf(file_pvector, "%d %f\n", i, pvector[i]);
    }
    fclose(file_pvector);
    /*free (energy_vs_time); */

}

void escribir_no(simulation *s, int file_number, int num_blocks, int num_steps)
{
    int i;
    char *filename;
    filename = malloc(MAX_FILENAME_SIZE);
    snprintf(filename,MAX_FILENAME_SIZE,"%s-%d", s->energy->path, file_number);
    
    if ((s->energy->fp = fopen(filename, "wb")) == NULL) {
	printf("I can't create the file %s \n", s->energy->path);
	exit(1);
    }
    for (i = 1; i < num_steps * num_blocks; i++) {
	fprintf(s->energy->fp, "%f %f\n",i*dt, energy_vs_time[i]);
    }
    fclose(s->energy->fp);
    /*free (energy_vs_time); */
    free(filename);
}

void escribir(simulation * s, int tblk)
{
   int i;
    if ((s->energy->fp = fopen(s->energy->path, "wb")) == NULL) {
	printf("I can't create %s \n", s->energy->path);
	exit(1);
    }
    for (i = 1; i < tblk; i++) {
      	fprintf(s->energy->fp, "%i %f\n",i, energy_vs_time[i]);
    }
}

void write_psi_par(char *fname)
{
 int i, j, ien, ipr, iee, isym;
    FILE *fp;

    fp = efopen(fname, "w");
    ien = iee = isym = ipr = 0;
    printf("Optimization: Escribiendo nuevos parametros\n");
    for (i = 0; i < nterm; i++) 
      {
	
	fprintf(fp,"%6.16f\n",plin[i]);
	for (j = 0; j < eNparam; j++) 
	  fprintf(fp,"%6.16f\n",parater_eN[ien++]);
	for (j = 0; j < numpr; j++) 
	  fprintf(fp,"%6.16f\n",ppr[ipr++]);
      for (j = 0; j < eeparam; j++)
	fprintf(fp,"%6.16f\n",parater_ee[iee++]);
      for (j = 0; j < numsym; j++)
	fprintf(fp,"%6.16f\n",psym[isym++]);
    }
    
    fclose(fp);
    
    
    

}
void graph_walkers(ensemble *  e, file * walker)
{
    int i, j, k,l;

    if ((walker->fp = fopen(walker->path, "wb")) == NULL) {
	printf("I can't create %s \n", walker->path);
	exit(1);
    }
    fprintf(walker->fp, "%d \n",e->num_walkers);
    for (i = 0; i < e->num_walkers; i++)
      {
	for (j = 0,l=0; j < e->num_pars; j++) 
	  {
	    for (k = 0; k < e->dim; k++,l++)
	      fprintf(walker->fp, "%6.16f ", e->walkers[i].R[l]);
	    fprintf(walker->fp, "\n");
	  }
	 fprintf(walker->fp, "\n");
      }
	fclose(walker->fp);
}

void grafica_fun_onda(int norm_steps, int norm_bin, simulation *  s)
{
    int i, j;
    int plot[4];
    double purito = 0;
    double  entes_cuadrado[norm_bin];
    double q;
    
    for (i = 0; i < norm_bin; i++) {
      
      entes_cuadrado[i] = (double)entes[i]*entes[i]/(double)norm_steps*norm_steps;
      purito += entes_cuadrado[i];
    }

    
    purito=purito*delta_norm;

    for (i = 0; i < norm_bin; i++)
      entes_cuadrado[i] = sqrt(entes_cuadrado[i] / purito);
    
    
    if ((s->wavefun->fp = fopen(s->wavefun->path, "wb")) == NULL) {
      printf("I can't create  %s\n", s->wavefun->path);
      exit(0);
    }
    for (i = 0; i < s->e->dim; i++)
	plot[i] = -1;
    i = 0;

    while (i < pow(graph_bins, s->e->dim)) {
      for (j = 0; j < s->e->dim; j++) {
	if (fmod    (i,   pow( graph_bins, j)   ) == 0)
	    plot[j]++;
	    if (plot[j] >= graph_bins)
		plot[j] = plot[j] - graph_bins;
	}
	for (j = s->e->dim - 1; j >= 0; j--) {
	    q = (graph_low + (delta_norm / 2)) + delta_norm * plot[j];
	    fprintf(s->wavefun->fp, " %f ", q);
	}
	fprintf(s->wavefun->fp, "%f \n ", entes_cuadrado[i]);
	i++;
    }
    fclose(s->wavefun->fp);
 
}


// borrar write_walker obsoleto

void write_walkers(ensemble * e)
{
  /* Write walker[i].R[j] */
    FILE *fp;
    int i, j, n;


    fp = efopen("file1", "w");


    fprintf(fp, "%d\n", e->ini_walkers);

    for (i = 0; i < e->ini_walkers; i++)
	for (j = 0; j < e->num_pars; j++) {
	    for (n = 0; n < e->dim; n++)
		fprintf(fp, "% 16.16f", e->walkers[1].R[n+j]);
	    fprintf(fp, "\n");
	}

}









