/*
   ** FILE
   **   linmin.c - line minimization of a N dimensional function
   **
   ** DESCRIPTION
   **   This routine perform the minimization of a R^N->R
   **   along a user-specified direction.
   **
   ** USES:
   **   minimize.c
   **
   ** HISTORY
   **   v 1.0    1 feb 1993     start from Numerical Recipes Version
   **   v 1.1   17 Jun 1993     further modified.
   **   v 1.2    6 Jun 1997     little cleanup (no longer use defs.h)
   **                              STATIC ARRAY VERSION
   **
   ** AUTHOR
   **   (C) Copyright Dario Bressanini
   **   Dipartimento di Scienze Matematiche Fisiche e Chimiche 
   **   Universita' dell'Insubria, sede di Como
   **   E-mail: dario@fis.unico.it
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <glib.h>
#include "moiss.h"
#include "mymath.h"

int bracket(FUNCe f, ensemble * ens,double *x0, double *x1, double *x2);
int brent(FUNCe f,ensemble * ens, double ax, double bx, double cx, double tol,
	  double *xmin, double *fmin);

/*--------------------------------------------------------------------------*/

#define PTOL	1e-6		/* doesn't need high accuracy */

#define MAXDIM	1024		/* max number of parameters */

static PFNe func;		/* this stores the function name */
static int ndim;		/* number of dimensions */
static double *point;		/* store the address of a vector */
static double *direc;		/* ............................. */
static double ptrial[MAXDIM];	/* trial point. */
static ensemble * ensb;

/*
   ** This function acts as interface between a monodimensional minimization
   ** function such as brent and a multidimensional function
   ** This function is called from monodimensional routines and uses globals
   ** point[] and direc[] are duplicate
   ** pointers to the real arrays received by linmin, same for (*func)()
 */

static double f1dim(double x, ensemble *e)
{
    int j;

    for (j = 0; j < ndim; j++)
	ptrial[j] = point[j] + x * direc[j];

    return func(ptrial,e);
}


/*
   ** minimize a user supplied function of 'n' variables along the
   ** direction specified by dirv[]
   ** if M_FOUND, set the point vector p to the new point and reset the
   ** direction vector to the new displacement vector
   ** the value of the function at the minimum is returned in *ymin
   **
   ** RETURN
   **   M_FOUND         minimum found
   **   int from bracket() or brent()
 */

int linmin(PFNe f,ensemble *ens, double p[], double dirv[], int n, double *fmin)
{
    double x1, x2, x3, xmin;
    int i;
    int code;

    ensb=(ensemble *)malloc(sizeof(ensemble));

    ensb=ens;
    ndim = n;			/* save the number of dimensions */
    point = p;			/* copy the pointers */
    direc = dirv;		/* so that f1dim can find them */
    func = f;

    x1 = 0.0;			/* MUST CHANGE THIS.... */
    x2 = 0.1;
    if (
	(code = bracket(f1dim,ens, &x1, &x2, &x3)) 
	!= M_FOUND
	)
	return code;

    if ((code = brent(f1dim,ens, x1, x2, x3, PTOL, &xmin, fmin)) != M_FOUND)
	return code;

    for (i = 0; i < ndim; i++) {
	dirv[i] *= xmin;	/* update direction vector */
	p[i] += dirv[i];	/* construct the vector result */
    }

    return M_FOUND;
}


/*--------------------------------------------------------------------------*/

#ifdef TEST1

#include <stdio.h>
#include <math.h>

double test1(double x[])
{
    return -sin(x[0]) * sin(x[1]) * exp(-x[0] * x[0] + x[0] + 2 + x[1] * x[1]);
}

main()
{
    double fmin, f;
    int i;
    double x[2], d[2];

    x[0] = -3.0;
    x[1] = 3;
    f = test1(x);
    printf("initial value %.8f\n", f);
    for (i = 0; i < 10; i++) {
	d[0] = 1;
	d[1] = 0;
	linmin(test1, x, d, 2, &fmin);
	printf("i %i x %.13f %.13f d %.6g %.6g f %.13f\n",
	       i, x[0], x[1], d[0], d[1], fmin);
	d[0] = 0;
	d[1] = 1;
	linmin(test1, x, d, 2, &fmin);
	printf("i %i x %.13f %.13f d %.6g %.6g f %.13f\n",
	       i, x[0], x[1], d[0], d[1], fmin);
    }

    printf("x %.13f %.13f d %.6g %.6g f %.13f\n", x[0], x[1], d[0], d[1], fmin);
}
#endif



