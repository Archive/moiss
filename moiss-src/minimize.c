/*
   ** FILE
   **   minimize.c - minimization of 1-dimensional functions 
   **
   ** DESCRIPTION
   **   this file contains various minimization routines to perform
   **   a minimization of a user supplied function
   **
   ** HISTORY
   **   v 1.0    1 feb 1993     start from Numerical Recipes Version
   **   v 1.1   17 Jun 1993     further modified.
   **   v 1.2    6 Jun 1997     little cleanup (no longer use defs.h)
   **                              STATIC ARRAY VERSION
   **
   ** AUTHOR
   **   (C) Copyright Dario Bressanini
   **   Dipartimento di Scienze Matematiche Fisiche e Chimiche 
   **   Universita' dell'Insubria, sede di Como
   **   E-mail: dario@fis.unico.it
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


#include <stdio.h>
#include <glib.h>
#include "moiss.h"

#include "mymath.h"

/*-----------------------------------------------------------------------*/

#define GOLD 	1.618033988	/* golden section, (sqrt(5)+1)/2 */
#define RGOLD	(GOLD - 1.0)	/* 0.618034 */
#define CGOLD	(1.0 - RGOLD)	/* 0.3819660 */

#define MAX_ITER	30	/* max. number of iteration */

/*
   ** Try to bracket a minimum of the user supplied function f() given
   ** two initial points *x0 and *x1.
   ** First look for the minimum inside the interval. If not found
   ** go downhill until found or until MAX_ITER iteration have been done
   ** If bracketing succedeed the triple is returned in *x0, *x1 and *x2.
   ** Simpler than the one given in Numerical Recipes but sufficient for
   ** most purposes
   **
   ** RETURN
   **   M_FOUND: bracketing OK.
   **   M_MAX_ITER: too many iterations performed.
 */

#define NUM_DIV		8	/* number of divisions of interval */

int bracket(FUNCe f,ensemble *ens, double *x0, double *x1, double *x2)
{
    double ax, bx, cx;
    double fa, fb, fc;
    double x[NUM_DIV + 1], fx[NUM_DIV + 1];	/* start from 0 */
    int i = 0;


    fa = f(*x0,ens);
    fb = f(*x1,ens);
    if (fa > fb) {		/* must be fa > fb; in case switch so we */
	ax = *x0;		/* can go downhill */
	bx = *x1;
    } else {
	ax = *x1;
	bx = *x0;
	fb = fa;		/* we need only the lowest */
    }

    /* first search inside (ax,bx) dividing the interval in NUM_DIV steps */
    for (i = 0; i < NUM_DIV + 1; i++) {
	x[i] = ax + i * (bx - ax) / ((double) NUM_DIV);
	fx[i] = f(x[i],ens);
    }
    for (i = 1; i < NUM_DIV; i++)
	if (fx[i] < fx[i - 1] && fx[i] < fx[i + 1]) {	/* found!! */
	    *x0 = x[i - 1];
	    *x1 = x[i];
	    *x2 = x[i + 1];
	    return M_FOUND;
	}
    /* not found inside, so go downhill */

    cx = bx + GOLD * (bx - ax);
    fc = f(cx,ens);
    while (fb >= fc) {
	ax = bx;
	bx = cx;
	cx = bx + GOLD * (bx - ax);
	fb = fc;
	fc = f(cx,ens);
	if (i++ == MAX_ITER)	/* too many iterations */
	    return M_MAX_ITER;
	/* printf("c %15.10f  fc %15.10f\n",cx,fc); */
    }

    *x0 = ax;
    *x1 = bx;
    *x2 = cx;
    return M_FOUND;
}


/*
   ** DESCRIPTION
   **   given three points, x0,x1,x2 that bracket a minimum of the
   **   user supplied function, this routine locate the minimum using 
   **   the Golden Search method and return its position, if found
   **
   ** INPUT
   **   x0,x1,x2: must be x1 between x0 and x2 and  f1<f0, f1<f2
   **   f       : the user supplied function
   **   tol     : tolerance to locate the minimum
   **
   ** OUTPUT
   **   *xmin: the guessed minimum (if found)
   **   *fmin: value of f at minimum
   **
   ** RETURN
   **   M_FOUND if found, M_NOT_FOUND otherwise
   **
   ** WARNINGS
   **   minimal error checking is done. accuracy > sqrt(DBL_EPSILON)
 */

int golden(FUNC f,double x0, double x1, double x2, double tol,
	   double *xmin, double *fmin)
{
    double x3;
    double f1, f2;
    int i = 0;

    tol = max(tol, SQRT_DBL_EPSILON);

    x3 = x2;
    /* at each step we keep track of four points, x0 ; x1 ; x2 ; x3 */
    if (abs(x3 - x1) > abs(x1 - x0)) {	/* x0-x1 is the smaller segment */
	x2 = x1 + CGOLD * (x3 - x1);
    } else {
	x2 = x1;
	x1 = x2 - CGOLD * (x2 - x0);
    }

    f1 = f(x1);
    f2 = f(x2);
    while (abs(x3 - x0) > tol * (abs(x1) + abs(x2))) {
	if (f2 < f1) {
	    x0 = x1;
	    x1 = x2;
	    x2 = RGOLD * x1 + CGOLD * x3;	/* new point .... */
	    f1 = f2;
	    f2 = f(x2);		/* ... and new value */
	} else {
	    x3 = x2;
	    x2 = x1;
	    x1 = RGOLD * x2 + CGOLD * x0;
	    f2 = f1;
	    f1 = f(x1);
	}
	if (i++ == MAX_ITER)
	    return M_MAX_ITER;
    }

    /* we are done. return the best point */

    *xmin = (f1 < f2) ? x1 : x2;
    *fmin = min(f1, f2);

    return M_FOUND;
}


/*
   ** DESCRIPTION
   **   find the minimum of a user supplied functions using brent's method
 */

#define TINY  1.0e-10

int brent(FUNCe f, ensemble *ens,double ax, double bx, double cx, double tol,
	  double *xmin, double *fmin)
{
    double a, b, d, e, u, v, w, x;
    double p, q, r;
    double fx, fu, fv, fw;
    double xm, tol1, tol2;
    double temp;
    int i, flag;

    /* tol = max(tol, SQRT_DBL_EPSILON); */

    a = min(ax, cx);		/* a and b must be in ascending order */
    b = max(ax, cx);
    x = w = v = bx;
    d = e = 0.0;		/* e: distance moved on the step before last */
    fv = fw = fx = f(x,ens);
    for (i = 0; i < MAX_ITER; i++) {
	xm = (a + b) / 2.0;
	tol1 = tol * abs(x) + TINY;	/* this avoids searching for 0 */
	tol2 = 2 * tol1;
	if (abs(x - xm) <= (tol2 - (b - a) / 2.0)) {
	    *xmin = x;
	    *fmin = fx;
#if 0
	    printf("Brent Iterations %d\n", i);
#endif
	    return M_FOUND;
	}
	flag = 0;
	if (abs(e) > tol1) {	/* try parabolic fit */
	    r = (x - w) * (fx - fv);
	    q = (x - v) * (fx - fw);
	    p = (x - v) * q - (x - w) * r;
	    q = 2 * (q - r);
	    if (q > 0.0)
		p = -p;
	    q = abs(q);
	    temp = e;
	    e = d;
	    /* now check if parabolic fit is ok */
	    if (abs(p) < abs(.5 * q * temp) || (p > q * (a - x) && p < q * (b - x))) {
		d = p / q;
		u = x + d;
		if (u - a < tol2 || b - u < tol2)
		    d = tol1 * sign(xm - x);
		flag = 1;
	    }
	}
	if (flag == 0) {
	    if (x >= xm)
		e = a - x;
	    else
		e = b - x;
	    d = CGOLD * e;
	}
	if (abs(d) >= tol1)
	    u = x + d;
	else
	    u = x + tol1 * sign(d);

	fu = f(u,ens);
	if (fu <= fx) {
	    if (u >= x)
		a = x;
	    else
		b = x;
	    v = w;
	    fv = fw;
	    w = x;
	    fw = fx;
	    x = u;
	    fx = fu;
	} else {
	    if (u < x)
		a = u;
	    else
		b = u;

	    if (fu <= fw || w == x) {
		v = w;
		fv = fw;
		w = u;
		fw = fu;
	    } else if (fu <= fv || v == x || v == w) {
		v = u;
		fv = fu;
	    }
	}
    }
    *xmin = x;
    *fmin = fx;
    return M_MAX_ITER;
}

/*
   ** check that x1<x2<x3 or x1>x2>x3 and f(x2) < f(x1) and f(x2) < f(x3)
 */

int check_brak(FUNC f, double x1, double x2, double x3)
{
    double f1, f2, f3;

    if (x2 <= min(x1, x3) || x2 >= max(x1, x3))
	return M_FAILURE;

    f1 = f(x1);
    f2 = f(x2);
    f3 = f(x3);

    if (f2 >= f1 || f2 >= f3)
	return M_FAILURE;

    return M_FOUND;
}

/*-------------------------------------------------------------------------*/


#ifdef TEST
#include <math.h>

double test1(double x)
{
    return 3 * x * x + 40 * sin(x);
}

double test(FUNC f, double x0, double x1)
{
    double x, x2, fx;

    printf("------------\n");
    if (bracket(f, &x0, &x1, &x2) != M_FOUND)
	printf("Can't bracket\n");
    else {
	printf("Bracket x0 %f x1 %f x2 %f \n", x0, x1, x2);
	if (golden(f, x0, x1, x2, 1e-15, &x, &fx) != M_FOUND)
	    printf("Golden Can't find the minimum\n");
	else
	    printf("Golden minimum f(%.14f)=%.14f\n", x, fx);

	if (brent(f, x0, x1, x2, 1e-15, &x, &fx) != M_FOUND)
	    printf("brent Can't find the minimum\n");
	else
	    printf("brent minimum f(%.14f)=%.14f\n", x, fx);
    }
    return x;
}

int main()
{
    double x;

    x = test(test1, 1.8, 1.9);
    printf("min %f\n", x);
    x = test(test1, 0, 1);
    printf("min %f\n", x);

    exit(0);
}

#endif				/* TEST */
