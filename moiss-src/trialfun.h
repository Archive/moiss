/* This file is part of MOISS
   
   Copyright (C) 1998-99 Alan Aspuru Guzik
			 Raul Perusquia Flores
			 Carlos Amador Bedolla
			 Francisco Bustamante
			 Dario Bressanini
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
               
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
                           
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
                                    
*/

#ifndef __TRIALFUN_H__
#define __TRIALFUN_H__

double Z_He=2.0;

#define Zprima_1  1.6875
#define Zprima_2  1.67
#define c 0.48

#define S(a,b) a+b
#define T(a,b) a-b
#define u(a,b) (((a-b)>0) ? (a-b) : -(a-b))
#define abs(a) (((a)>0) ? (a) : -(a))


#endif /* __TRIALFUN_H__ */

