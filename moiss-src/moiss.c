#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <glib.h>
#include <gsl/gsl_rng.h>
#include "moiss.h"
#include "output.h"
#include "init.h"
#include "vmc.h"
#include "gui.h"
#include <unistd.h>
#include <getopt.h>
#include "bto.h"
#include "bopt.h"
#include "path.h"
#include "optimiza.h"
#include "../gmoiss-src/gmoiss.h"
#include "../gmoiss-src/pl.h"
#include "stats.h"
//#include "method.h"
#include "proplist.h"
#include "walk.h"
#include "string.h"
#include <signal.h>

/* ASQUEROSO*/
/* #define PVM */

#ifdef HAVE_PVM

#include <pvm3.h>
#endif

char * prg_name;

/* internal prototypes */
int moiss_signal_handler (int sig_num);


/* returns a new file structure with a given "filename"
    the string so is copied so please free it */
file * new_file(gchar * filename) {
    gchar * fn;
    file * f;
    fn = g_strdup(filename);
    if (fn == NULL) g_error (" No memory for creating file pointer %s.", filename);
    f = g_new(file, 1);
    f->path = fn;

    return f;
}

int main(int argc, char*  argv[])
{
  simulation *s;            /* Main simulation ensemle */
 
  int c,norm_bins=0;
  int i, dohisto;
  int tot_blocks=0; /* total number of blocks in the simulation */
  GList * mc_node, * stats_node;
  MonteCarlo * montecarlo;
  Stat * stat;
  proplist_t key;
	



    /* PVM related stuff, mainly added by roadmaster. */

    // PVM int mytid;

    //PVM int parent_tid;
    /* Child info*/
    // PVM int  *tids, kchilds,nchilds;
    /* buffer id and info*/
    // PVM int bufid, info;
    /* tasks to spawn, for the time being it's a freaking integer*/
    // PVM int tasks=3;
    /* my seeds array*/
    /* FIXME::: do create the fucking seeds array, check what datatype it should have
       by chcking randinit in rand.c.
     */
    
    /* ------------------ end road's pvm stuff*/

    // PVM int digit_optind = 0;
 int bol_IS = OFF; /* importance sampling? */
 int bol_WAVEFUNCTION_PLOT = OFF; /* has average energy been initialized? */
 int bol_ENERGY_PLOT = OFF; /* has ENERGY_PLOT been initialized once? */
 char * monte_string;




    /* timer */
    GTimer* stopwatch;
 opto *opt;
 opt = (opto *) malloc( sizeof(opto));
 

#ifdef debug
    /* setting on error exit query from glib */
    prg_name = strdup(argv[0]);
    moiss_signal_handler(0);
#endif
    /* Getting the time */
    stopwatch = g_timer_new();
    g_timer_start(stopwatch);
   g_message("MOISS (C) 1999 R.A. Perusquia, A. Aspuru, C. Amador, D. Bressanini");
    /* creating the simulation */
   //  s = g_new(simulation, 1);
   s = (simulation *) malloc( sizeof(simulation));
   /* creating the ensemble */
   //s->e = g_new(ensemble, 1);
   s->e = (ensemble *) malloc( sizeof(ensemble));
   /* now we are going to create new default
      file handlers and the following code
      should be deprecated ASAP */
   s->input = new_file(INPUT_FILENAME);
   s->restart = new_file(RESTART_FILENAME);
   s->wavefun = new_file(HISTOGRAM_FILENAME);
   s->energy = new_file(ENERGY_FILENAME);
   s->walkers = new_file(WALKERS_FILENAME);
    s->ginput = new_file(GMOISS_FILENAME);
    s->parameters = new_file(PARAMETERS_FILENAME);

    restart=0;
    
    while (1) {
	/*int this_option_optind = optind ? optind : 1; */
	int option_index = 0;
	static struct option long_options[] =
	{
	    {"about", 0, 0, 'a'},
	    {"restart", 1, 0, 'r'},
	    {"variational", 0, 0, 'v'},
	    {"help", 0, 0, 'h'},
	    {"input", 1, 0, 'i'},
	    {"gmoiss-input", 1, 0, 'g'},
	    {"histogram", 1, 0, 'p'},
	    {"energy", 1, 0, 'e'},
	    {"walkers", 1, 0, 'w'},
	    {0, 0, 0, 0}
	};

	c = getopt_long(argc, argv, "ahifg:r:vp:e:w",
			long_options, &option_index);
	if (c == -1)
	    break;

	switch (c) {
	case 'a':
	    printf("  MOISS\n ");
		printf(" Molecular Intergration and Sampling Software\n");
	    printf(" version 0.5\n ");
	    printf(" Copyright (C) 1998  Under the GPL License\n ");
	    printf("                     Raul Perusquia \n ");
	    printf("                     Alan Aspuru \n ");
		printf("                     Carlos Amador \n ");
		printf("                     Dario Bressanini \n ");
		printf("                     Francisco Bustamante \n");
	    printf(" Send comments, requests for help,\n");
	    printf(" bugs, suggestions and patches to:\n");
	    printf(" moiss@eros.pquim.unam.mx\n");
		exit(0);
	    break;
		
	case 'r':
	  /*printf ("walkers data read from `%s'\n", optarg); */
	    s->restart->path = optarg;
	    restart=1;
	    printf("using `%s' as input file  \n", s->restart->path);
	    break;
	case 'v':
	    printf("runing only VMC\n");
	    break;
	case 'n':
	    printf("runing only DMC\n");
	    break;
	case 'i':
	    s->input->path = optarg;
	    printf("using `%s' as input file  \n", s->input->path);
	    break;
	case 'p':
	    s->wavefun->path = optarg;
	    printf("using `%s' as input file  \n", s->wavefun->path);
	    break;
	case 'e':
	    s->energy->path = optarg;
	    printf("using `%s' as input file  \n", s->energy->path);
	    break;
	case 'w':
	    s->walkers->path = optarg;
	    printf("using `%s' as input file  \n", s->walkers->path);
	    break;
	case 'g':
	    s->ginput->path = optarg;
	    printf("GMoiss input file is %s .\n", nom_gmoiss_input);
	    break;
	case 'h':
	    printf(" MOISS \n ");
	    printf("Monte Carlo Integration and Sampling Software \n ");
	    printf("http://eros.pquim.unam.mx/moiss/ \n \n ");
	    printf("-i <a> --input <a>    <a> is the name of the input file \n ");
	    printf("-r <b> --restart <b>  <b> is the name of the restart file \n ");
	    printf("-v     --variational   run only VMC  \n ");
	    printf("-n     --novar         run DMC without VMC\n ");
	    printf("-p <c> --histogram <c> name of the histogram output file \n ");
	    printf("-e <d> --energy <d>    name of the energy vs. t output file \n ");
	    printf("-w <e> --walkers <e>   name of the walkers positions output  file \n ");

	    exit(0);
	    break;
	case '?':
	    break;
	default:
	    printf("?? getopt returned character code 0%o ??\n", c);
	}
    }

    if (optind < argc) {
	printf("non-option ARGV-elements: ");
	while (optind < argc)
	    printf("%s ", argv[optind++]);
	printf("\n");
    }
    /* here starts the real program  */

#ifdef HAVE_PVM

    /* Try to enroll in pvm.*/
    mytid=pvm_mytid();
    if (mytid<0){
	g_error("Unable to register under pvm- since pvm is defined and I'm expecting\n it to be there, but it's not, i'm aborting.  ");
	exit(255);
    }
    else{
	g_message ("Successfully registered under pvm!!!!!!");
    }
    parent_tid=pvm_parent();


    g_message ("My parent is %d\n",parent_tid);   
    
    /* Different course of action depending on whether i'm dad or son*/
    if (parent_tid==PvmNoParent){
	/* Read the configuration from the input file */
	read_config(s);
	/* this is to read from gmoiss-generated input files*/

	/* This one reads from the input file and puts that shite into
           the calculation_dict. It is then trivial to PVMize: the
           father does this unmodified, the children will instead get
           the info from the parent. This way we ensure configuration
           is the same for them all.*/
	
	read_gmoiss_from_file_no_pl_c (s->ginput->path);
	
	
	/* Once I have the configuration, spawn the boyz.*/
	
	/* FIXME: please add a check to see whether memory allocation
           was successful, and puke if it wasn't.*/
	tids=(int *) malloc (tasks * sizeof(int));	
	/* See how many boyz I spawned (tids), make an array seeds[tids],
	   create tids random seeds and put em in the array, and send each child
	   a message containing the configuration and the corresponding seed.
	   As each child gets its startup information, it starts working.*/       
	
	
	/* BIG FIXME PINCHE ROADMASTER: relaying parameter lists is
           not as easy as it looks, PVM needs stripping of the
           program's name (basically removing the first element of the
           array) and it needs the array to be terminated by a null
           pointer. Therefore i'll need to build a function that
           constructs the array in the proper format based in main's
           argc and argv. For the time being we're using it null.*/
	nchilds=pvm_spawn("moiss", NULL, 0, NULL, tasks,tids);

	if (nchilds<=0){
	    g_error("Unable to spawn child tasks");
	    pvm_exit();
	    exit(1);
	}
	printf ("%d tasks have been successfully spawned\n",nchilds);
	/* FIXME: add a check to see if tasks > nchilds which would
	   indicate failure to start all the requested tasks, in this
	   case print out the error codes from the tids array to aid
	   in debugging. Error codes are negative values, while valid
	   tids are positive integers.*/
	
	
	
    }
    else{
	/* I'm a son, child or slave.*/

	/* First, initialize from values I will receive from my daddy.*/
	
	
    }
#endif	
    // Here starts the calculation
    read_gmoiss_from_file_no_pl_c(s->ginput->path);
    // FORK HERE

    read_config(s);
   
 


   
    
	// now initializng the Monte Carlo List
	key = PLMakeString(dictionary_names[CALCULATION]);
	s->mc_list = mc_pl_to_list(PLGetDictionaryEntry (calculation_dict, key));
	PLRelease(key);
	block_count=0;
	dohisto=0;
	
	/* calculating total blocks and total moves */
	for (mc_node = s->mc_list; mc_node != NULL; mc_node = mc_node->next) {
	  montecarlo = (MonteCarlo *) mc_node->data;
	  tot_blocks += montecarlo->blocks;
	}
	
	for (mc_node = s->mc_list; mc_node != NULL; mc_node = mc_node->next) {
	  montecarlo = (MonteCarlo *) mc_node->data;
	  
	  
	  /* send_pvm_results(dad) */
	  
	  monte_string = g_malloc(200);
	  monte_string = strncpy(monte_string, mc_names[montecarlo->type-MC_VARIATIONAL], 199);
	  printf("%s ", monte_string);
	  g_free(monte_string);
	  i=0;
	  /* now finding which stats are on in the simulation */
	  for (stats_node = montecarlo->stats; stats_node != NULL; 
	       stats_node = stats_node->next) 
	    {
	      /* here we do TWO things:
		 initialize the stats functions and also prepare all the structures
		 that they will need when they are called thousands of times inside
		 the algorithm */
	      
	      stat = (Stat *) stats_node->data;
	      
	      switch(stat->type) {
	      case IMPORTANCE_SAMPLING:
		bol_IS = ON;
		stat->function = stats_dummy;
		i=1;   // Flag for user info
		break;
	      case  MOVE_ELECTRONS:
		stat->function = stats_dummy;
	      break;
	      case GROWTH_ENERGY:
		stat->function = stats_dummy;
		break;
	      case INTERPARTICLE_DISTANCE:
		stat->function = stats_dummy;
	      break;
	      case ENERGY_PLOT:
			if (bol_ENERGY_PLOT == OFF) {
			  bol_ENERGY_PLOT = ON;
			 
			  energy_vs_time = (double *) malloc((tot_blocks + 1) * sizeof(double));
			  if (energy_vs_time == NULL)
				g_error("No memory for energy_vs_time !");
			}
			  stat->function = stats_dummy;
			
		break;
	      case WAVEFUNCTION_PLOT:
		/* initializing the wave function  plot options */
		bol_WAVEFUNCTION_PLOT++;
		  if(bol_WAVEFUNCTION_PLOT == 1) {
		    norm_bins = (int)pow(graph_bins, s->e->dim) - 1;
		    entes = (int *) malloc((norm_bins+1) * sizeof(int));
		    if (entes == NULL)
		      g_error("Sorry. No memory for array entes!");
		    for (i = 0; i < norm_bins; i++)
		       entes[i] = 0;
		    delta_norm = (double) (graph_high - graph_low) / graph_bins;
		  bol_WAVEFUNCTION_PLOT++;
		  }
		  /* and now setting up the function */
		  
		  stat->function = stats_histogram;printf("b\n");
	      break;
	    case DUMP_WALKERS:
	      stat->function = stats_dummy;
	      break;
	      
	      } // end switch
	    } // end stats_node for
	  /* now that the stats are done, we'll check for each MonteCarlo type and
		 run it */
	  if(i==1)
	    printf("with IS\n");
	  else
	    printf("\n");
	  

	  switch (montecarlo->type)
		{
		case MC_VARIATIONAL:
		

		  fill_walkers(s->e,TRUE);
		

		  Walk = variational_M;
		  walk_block(s->e, montecarlo);
		   graph_walkers(s->e,s->walkers);
		  break;
		case MC_VARIATIONAL_FP:
		  fill_walkers(s->e, TRUE);
		  Walk = variational_FPF;
		  walk_block(s->e, montecarlo);
 graph_walkers(s->e,s->walkers);
		  break;

		case MC_DIFFUSION:
		  fill_walkers(s->e, bol_IS);
		  if (bol_IS) {
			Walk = walk_with_branch_IS;
		  }
		  else {
			Walk = walk_with_branch;
		  }
		  walk_block(s->e, montecarlo);
		  break;
		case MC_PURE_DIFFUSION:
		  fill_walkers(s->e, bol_IS);
		  // Walk = walk_pure_1;
		  walk_block(s->e, montecarlo);
		  break;
		case MC_OPTIMIZATION:
		  read_config_opt(opt);
		  optima(opt,s->e);
		 
		  write_psi_par(s->parameters->path);
		  break;
		case MC_PATH:
		  path(montecarlo->blocks,bol_IS , montecarlo->iterations);
		}
	  if (stats_check(montecarlo->stats, WAVEFUNCTION_PLOT)) 
	    dohisto += montecarlo->blocks * montecarlo->iterations;
	} 
	//end MonteCarlo for


	
//	if(dohisto)
//	  grafica_fun_onda(dohisto,norm_bins, s);
	graph_walkers(s->e, s->walkers);
//	if (bol_ENERGY_PLOT == ON)
	  escribir(s, tot_blocks);
	
	/* Finish here. In theory this is where we should return our results to our parent.*/
	print_time(stopwatch);
	
	/* stopping and destroying the watch */
	
	g_timer_stop(stopwatch);
	g_timer_destroy(stopwatch);
	
	return(0);
	
}


walker
new_walker(int dimensions, int num_pars)
{
  
    walker newwalker;
    
    /* Labels for identifyin & number of electrons*/ 
    newwalker.dim=dimensions;
    newwalker.num_pars=num_pars;

    /* Allocate the memory for the new walker */
    newwalker.R = (double *) malloc(num_pars * dimensions * sizeof(double));
    if (newwalker.R == NULL)
	g_error("Not enough memory for creating a new walker!");

    newwalker.Force = (double *) malloc(num_pars * dimensions * sizeof(double));
    if (newwalker.Force == NULL)
      g_error("Not enough memory for creating a new walker!");
   
	newwalker.grad = (double *) malloc(num_pars * dimensions * sizeof(double));
    if (newwalker.grad == NULL)
      g_error("Not enough memory for creating a new walker!");

    /*Setng the weigth to 1  */
    newwalker.weight = 1.0;
    
    /* Initalizing Psi , E_Local and Z */
    newwalker.Psi = 0.0;
    newwalker.Egy_Lcl = 0.0;
    newwalker.Z = 1.0;
	newwalker.pot = 0;
	newwalker.weight = 1.0;
	newwalker.del2 = 0.0;

    return newwalker;
}
/* WARNING: this function only copies mutable quantities between
   walkers, do not use if you need to copy an unmutable quantity,
   you should use a non-existant function called full_copy_walker */
void copy_walker(walker * w1, walker * w2)
     /* Source walker w1, Dest walker w2 */
{
    int i;
    for (i = 0; i < ncoord  ; i++)
      {
      w2->R[i] = w1->R[i];
      w2->Force[i] = w1->Force[i];
	  w2->grad[i] = w1->Force[i];
      }
    w2->Psi = w1->Psi;
    w2->Egy_Lcl = w1->Egy_Lcl ;
	w2->pot = w1->pot;
	w2->weight = w1->weight;
	w2->del2 = w1->del2;
	  
}
/*
void print_walker(walker * w)
{
    int i, j;
    for (i = 0; i < w->num_pars; i++)
	printf("Walker (numpars = %d dimensions =%d):", w->num_pars, dimensions);
	for (j = 0; j < w->dim; j++) {
	    
	    printf(" %f", w->pars[i].R[j]);
	    
	}
	printf(", Z =%f \n", w->Z);
}

void print_r(double * r[])
{
  int i, j;
  for (i = 0; i <num_pars; i++) 
    for (j = 0; j < dimensions; j++) 
      printf(" %f \n",*( r[i]++));
}

*/
/*
void print_par(par * part)
{
  int i, j;
  for (i = 0; i <num_pars; i++) 
    for (j = 0; j < dimensions; j++) 
       printf(" %f \n",part->);
      //     normal_dist()*;
}

*/
/*
void flush_walker(walker * w)
{
    int i, j;
    for (i = 0; i < w->num_pars; i++)
	for (j = 0; j < w->dim; j++) {
	    w->pars[i].pastForce[j] = w->pars[i].Force[j];
	    w->pars[i].pastR[j] = w->pars[i].R[j];
	}
    w->pastPsi = w->Psi;

}

void reincarn_particle(walker * w, int i)
{
    int j;
    for (j = 0; j < w->dim; j++) {
	w->pars[i].Force[j] = w->pars[i].pastForce[j];
	w->pars[i].R[j] = w->pars[i].pastR[j];
    }
}
*/

//void flush_Force_par(par * pars)
//{
//    int i;
/*   pars.pastForce = pars.Force; */

/*   for (i=0; i<num_pars;i++) { */
/*   printf("Past Force: %f\n",pars->pastForce[i]); */
/*   printf("New Force: %f\n", pars->Force[i]); } */

    /* dumb implementation, we think there is a cooler way  with pointers to pars, free and such! */

//    for (i = 0; i < dim; i++) {
//	pars->pastForce[i] = pars->Force[i];
//    }

//}


walker
new_walker_from(walker * w1)
    /* Source walker w1, Returns Dest */
{
    walker w = new_walker(w1->dim,w1->num_pars);
    copy_walker(w1, &w);
    return w;
}

void initialize_walkers(walker * w1)
/* Initialliza the walker at the begining */
{
  int k;
   for (k = 0; k < ncoord; k++)
     w1->R[k] = initial_spread *2* (0.5 - gsl_rng_uniform(rng));
 
 
    

}
/*
double distance (walker * w, int pi, int pj) {
    double s=0;
    int i;
        
    for (i = 0; i < w->dim; i++) {
	s = s +  P2(( w->pars[pi].R[i] -
		       w->pars[pj].R[i]));
    }
    
    return sqrt(s);

}

*/


void print_time(GTimer* stopwatch)
{
    g_message("CPU time: %.2f secs. Effective time: %.2f secs.", (double)
	       clock() / CLOCKS_PER_SEC, g_timer_elapsed(stopwatch, NULL));
}
/*
  void cerrar(void)
  {
  int i, j;
  for (i = 0; i < s->e->ini_walkers * mem_extra; i++)
  for (j = 0; j < num_pars; j++)
  free(walkers[i].pars[j].R);
  for (i = 0; i < s->e->ini_walkers * mem_extra; i++)
  free(walkers[i].pars);
  free(walkers);
  }
*/
#ifdef debug1
int  moiss_signal_handler (int sig_num)
{
  static gboolean       in_call = FALSE;
  if (in_call)
	{     fprintf (stderr,
				   "\naborting on another signal: `%s'\n",
				   g_strsignal (sig_num));
	fflush (stderr);
	exit (-1);
	}
  else
	in_call = TRUE;
  
  signal (SIGINT,       moiss_signal_handler);
  signal (SIGTRAP,      moiss_signal_handler);
  signal (SIGABRT,      moiss_signal_handler);
  signal (SIGBUS,       moiss_signal_handler);
  signal (SIGSEGV,      moiss_signal_handler);
  signal (SIGPIPE,      moiss_signal_handler);
  /* signal (SIGTERM,   gvt_signal_handler); */
  
  if (sig_num > 0)
    {
	  fprintf (stderr, "%s: (pid: %d) caught signal: `%s'\n",
			   prg_name,
			   getpid (),
			   g_strsignal (sig_num));
	  fflush (stderr);
	  g_on_error_query (prg_name);
	}
  in_call = FALSE;
}
#endif




