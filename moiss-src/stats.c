#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <glib.h>
#include <gsl/gsl_rng.h>
#include "moiss.h"
#include "output.h"
#include "init.h"
#include "vmc.h"
#include "gui.h"
#include <unistd.h>
#include <getopt.h>
#include "bto.h"
#include "bopt.h"
#include "path.h"
#include "../gmoiss-src/gmoiss.h"
#include "../gmoiss-src/pl.h"
#include "walk.h"
#include "stats.h"
#include "method.h"
#include "proplist.h"

#ifdef HAVE_PVM

#include <pvm3.h>
#endif

void do_stats(ensemble * e, GList * stats) {
  GList * stats_node;
  Stat * stat;
  for (stats_node = stats; stats_node != NULL; stats_node = stats_node->next) {
	/* calling the function pointers */
	stat = (Stat *) stats_node->data;
	stat->function (e);
  }
}

void stats_dummy (ensemble *e ) {
  /* this is a dummy stats for testing */
}

void stats_histogram(ensemble * e)
{
    int i,j, k, n;
    int purito = 0;
    double delta;
    int dumb;
    

    delta = delta_norm;
    for (i = 0; i < e->num_walkers; i++)

	for (j = 0; j < e->num_pars; j++) 
	  {
	    for (k = 0; k < dimensions; k++)
	      if (e->walkers[i].R[k+j] > graph_low &&
		  e->walkers[i].R[k+j] < graph_high)
		purito++;
	    if (purito == dimensions) 
	      {
		n = 0;
		for (k = 0; k < dimensions; k++) 
		  {
		    dumb = ((e->walkers[i].R[k+j] - graph_low) / delta);
		    n += (int) pow(graph_bins, k) * dumb;
		  }
		entes[n]++;
		//printf("%d %d \n",entes[n],n);
		//printf("***** %f \n",entes[n]);
		purito = 0;
	      }
	    purito = 0;
	  }
 
   
}

BooleanType stats_check_average(GList * stats) {
  GList * stats_node;
  Stat * stat;
  for (stats_node = stats; stats_node != NULL; stats_node = stats_node->next) {
	/* calling the function pointers */
	stat = (Stat *) stats_node->data;
	if (stat->type == ENERGY_PLOT)
	  return ON;
  }
  return OFF;
}
BooleanType stats_check_wavef(GList * stats) {
  GList * stats_node;
  Stat * stat;
  for (stats_node = stats; stats_node != NULL; stats_node = stats_node->next) {
	/* calling the function pointers */
	stat = (Stat *) stats_node->data;
	if (stat->type == WAVEFUNCTION_PLOT)
	  return ON;
  }
  return OFF;
}

BooleanType stats_check(GList * stats, StatType s_type) {
  GList * stats_node;
  Stat * stat;
  for (stats_node = stats; stats_node != NULL; stats_node = stats_node->next) {
	
	stat = (Stat *) stats_node->data;
	if (stat->type == s_type)
	  return ON;
  }
  return OFF;
}
