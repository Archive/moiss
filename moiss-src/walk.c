#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <gsl/gsl_rng.h>
#include <math.h>
#include <glib.h>
#include "stdio.h"
#include "moiss.h"
#include "../gmoiss-src/gmoiss.h"
#include "walk.h"
#include "output.h"
#include "gui.h"
#include "method.h"
#include "rand.h"
#include "stats.h" 

void walk_block(ensemble * e, MonteCarlo * mc)
{ 
  double bEsum, bEsum2,bE2sum;
  double porctg, regi;
  double esig,sigma;
  int bol_avg= OFF; /* do averages? */
  int bol_EvsT= OFF;
  int block;
  int num_blocks,bol_wf; /* number of blocks from the Monte Carlo */
  int num_steps;
  double Eb, Eb2;
  GList * stats_list;
  bEsum = 0;
  bEsum2 = 0;
  bE2sum =0;
  num_blocks = mc->blocks;
  num_steps = mc->iterations;
  regi = (double) num_blocks / 10.0;
  
  stats_list = mc->stats;
  bol_wf = stats_check_wavef(stats_list);
  bol_avg = stats_check_average(stats_list);
  bol_EvsT = stats_check(stats_list,ENERGY_PLOT);
  printf("** %d %d %d \n",bol_wf,bol_avg,bol_EvsT);
  // printf("%d \n",bol_EvsT);
  printf("Blocks  Steps     Energy       %% \n");
  
  for (block = 1; block < num_blocks+1; block++) 
    {
      Eb=Eb2=0;
      block_count++;
      (*Walk) (e, stats_list, num_steps,&Eb,&Eb2);
      // printf("%f \n",Eb);
      porctg = 100.0 * block / num_blocks;
      if(num_blocks<10)
	printf(" %3d   %5d   %.10f   %2.1f  \n", block, block * num_steps
	       , Eb, porctg);
      else
	if ((fmod((double) block, regi)) == 0)
	  printf(" %3d   %5d   %.10f   %2.1f \n", block, block * num_steps
		 , Eb, porctg);
      
      //Arreglar lo de mezcla de EvsT y average !!!!!!!!!!!!!
      if (bol_EvsT==1){
	energy_vs_time[block_count]=Eb;
	bEsum += Eb;
	bEsum2 +=Eb*Eb;
	bE2sum +=Eb2;
      }
      
    }
  bEsum/=(double) num_blocks;
  bEsum2 /=(double) num_blocks;
  bE2sum/=(double) num_blocks;
  e->Paccept/=(double) (num_blocks*e->ini_walkers* num_steps);

  esig = sqrt( (bEsum2-bEsum*bEsum)/(double) num_blocks);
  sigma = sqrt(bE2sum-bEsum*bEsum);
  /* FIXXXXXXX este idf por favor (stats), alguna dia*/
  
  if (bol_EvsT==1) {
    printf("<A> =%f \n",e->Paccept);
    printf("Average Energy:  %.12f  (+/-) %.12f\n", bEsum,esig);
    printf("S(H) = %f  D(H) = %f\n",sigma,sigma/fabs(bEsum));

    
  }
  /*autocor(num_blocks); */
}




void walk_with_branch (ensemble * e, GList * stats, int num_steps,double *estep, double *e2step)

{
  double vsum = 0;
  double vave = 0;
  double sum = 0;
  int i, j = 0, k;
  double potential,oldpotential;
  int num_branches;
  double a = 0.1;		/* Factor for acceptance, may be varied */
  int bol_wf = OFF; /* do normalize?*/
  double iestep,ie2step;
  bol_wf = stats_check_wavef(stats);
  
  // printf("**** %d \n",bol_wf);

  
  for (i = 0; i < num_steps; i++) {
     iestep=ie2step=0.0;
    for (j = e->num_walkers-1; j >= 0; j--) {
      oldpotential = e->walkers[j].Egy_Lcl;
      for (k = 0; k < ncoord; k++)
	  e->walkers[j].R[k] += normal_dist() * diff_sigma;

      
      potential = (*V2) (&e->walkers[j]);
      e->walkers[j].Egy_Lcl=potential;
      
      num_branches = exp(-((0.5 * (potential + oldpotential)) - e->ref_energy) * dt)
	
	+ gsl_rng_uniform(rng);
      
	switch (num_branches) {
	case 0:
	  copy_walker(&(e->walkers[e->num_walkers]), &(e->walkers[j]));
	  e->num_walkers--;
	  break;
	case 1:
	  //vsum += potential;
	  iestep+=potential;
	  ie2step+=potential*potential;
	  break;
	default:
	  if (num_branches > max_branch)
	    num_branches = max_branch;
	  iestep+=potential*(double) num_branches;
	  ie2step+=potential*potential*(double) num_branches ;
	  for (k = 1; k < num_branches; k++) 
		{
		  e->num_walkers++;
		  copy_walker(&e->walkers[j], &e->walkers[e->num_walkers]);
		}
	  break;
	}
    }
    
    //   vave = vsum /(double) e->num_walkers;
	iestep /=(double)e->num_walkers;
	ie2step /=(double)e->num_walkers;
	
	*estep+=iestep;
	*e2step+=ie2step;
    
    do_stats(e, stats);
    sum += vave;
    e->ref_energy = iestep
      -a * (e->num_walkers - e->ini_walkers) / (e->ini_walkers * dt);
    //      printf("*** %f %f %d \n",e->ref_energy,vave,e->num_walkers);
    vsum = 0;
    // printf("%d \n",bol_wf);
    if(bol_wf)
      stats_histogram(e);
  }
  //  avg = sum / num_steps;
  //energy_vs_time[block_count] = avg;
  // return avg;
  *estep /=(double)num_steps;
  *e2step /=(double)num_steps;
  
  
}


void  walk_with_branch_IS(ensemble * e, GList * stats, int num_steps,double *estep, double *e2step)
{
  int i, j = 0, k=0, d, p;
  int num_branches;
  double Q,A;
  double Accept=0;
  int bol_wf = OFF; /* do normalize?*/
  double Vnew=0;
  double iestep=0,ie2step=0; /* average of the local energy */
  double sigma; // variance of the gaussian of displacement
  double Ddt; //D*dt
  double dt_eff = 1.0; //effective time step 
  double dt_avg = 1.0; // average of the dt_effective
  double Iaccept = 0; // counter of the times we averaged dt_eff, accepted
  walker oldwalker; //walker before the move

  bol_wf = stats_check_wavef(stats);
  // sigma of the gaussian, with D=0.5 it's sqrt 2D dt
  // when we want to do non adiabatic, we'll need to replace
  // sigma by a more intelligent, by particle case.
  sigma=sqrt(dt);
  Ddt=0.5*dt;
  //setting the energys to zero
  *estep = 0;
  *e2step = 0;
	
  //malloc'ing the new walker.
  oldwalker = new_walker(e->dim, num_pars);
  //printf("Numwalkers = %d, Iniwalkers = %d\n", e->num_walkers, e->ini_walkers);

  for (i = 0; i < num_steps; i++) 
    {// for the steps, we initialize things for this step.
      iestep=ie2step=0;
	  // adjusting the energy a-la Umrigar 1993
	  e->ref_energy = e->energy - (dt/dt_avg)*log10((double)e->num_walkers/(double)e->ini_walkers);
      dt_avg=0;
	  for(j = e->num_walkers-1;j>=0;j--) 
		{//for in all walkers
		  
		  Accept=0.0;
		  for (p=0; p < num_pars; p++) 
			{
			  //we'll backup the walker
			  copy_walker(&(e->walkers[j]), &oldwalker);
			  
			  for (d = 0; d < e->dim; d++) 
				{// start for in moving
				  e->walkers[j].R[p+d]+=
					normal_dist() * sigma + e->walkers[j].Force[k] * 0.5*dt;
				  // gaussian displacement with a mean of sigma and
				  // then a force displacement of Fq * D * dt
				}//end moving
			  (*Derivs)(&e->walkers[j]);
			  if(sign (oldwalker.Psi)!=sign(e->walkers[j].Psi))
				{// rejected :(
				  //printf("oldwalker.psi = %f, psi =%f\n",oldwalker.Psi,e->walkers[j].Psi); 
				  copy_walker(&oldwalker, &(e->walkers[j]));
				  printf("********* Rejected fixed node!!******\n");
				  //inserted a derivs
				  (*Derivs)(&e->walkers[j]);
				  //}//end if of last particle
				}//end if rejected
			  else{//accepted :)
				Q=0;
				for (d = 0; d < e->dim; d++) 
				  {//for dimensions
					
					e->walkers[j].Force[p+d]=
					  2 * e->walkers[j].grad[p+d] / e->walkers[j].Psi;
					
					Q+=(oldwalker.Force[p+d] + e->walkers[j].Force[p+d])*
					  ( 
					   Ddt*oldwalker.Force[p+d] 
					   + 2.0*oldwalker.R[p+d]+
					   - 2.0*e->walkers[j].R[p+d]+
					   - Ddt* e->walkers[j].Force[p+d]
					   );
				  }// end for in dimensions
				
				 A =((e->walkers[j].Psi * e->walkers[j].Psi)/(oldwalker.Psi *oldwalker.Psi))* exp(0.25 * Q);
				 if (A < gsl_rng_uniform(rng)) 
				   {//rejected :(
					 copy_walker(&oldwalker, &(e->walkers[j]));
					 (*Derivs)(&e->walkers[j]);
					 //}//end if of last particle
				   }//end rejected
				 else
				   {//accepted by metropolis
					 Accept = Accept + 1.0/(double)num_pars;
					 
					   
					 //}//end for in dimensions
					 
				   }//end accepted by metropolis
			  }//end else accepted by fixed node
			}//end for in particles
		  //get the potential
		  Vnew= (*V2) (&e->walkers[j]);
		  //set the new effective time step
		  dt_eff = dt * Accept;
		  dt_avg+= dt_eff;
		  Iaccept++;
		  //update the local energy and add it to the configuration energy 
		  e->walkers[j].Egy_Lcl =  -0.5 * 
			e->walkers[j].del2 / e->walkers[j].Psi + Vnew;
		  //debugging
		  //printf("*** nwalker = %d elocal = %f total= %d \n", j, e->walkers[j].Egy_Lcl, e->num_walkers);
		  //now doing the branching
		  num_branches = (int) (exp(-1.0*dt_eff*
									(0.5*(e->walkers[j].Egy_Lcl+oldwalker.Egy_Lcl)
									 -e->ref_energy)) +
								gsl_rng_uniform(rng));
		  //printf("num branches = %d\n",num_branches);
		  //if(num_branches != 1) printf("reproduccion cogi cogi %d\n",num_branches);
		  switch(num_branches) 
			{//start switch
			case 0:
			  copy_walker(&(e->walkers[e->num_walkers-1]), &(e->walkers[j]));
			  e->num_walkers--;
			  break;
			case 1:
			  iestep+= e->walkers[j].Egy_Lcl;
			  ie2step+= e->walkers[j].Egy_Lcl *e->walkers[j].Egy_Lcl ;
			  e->energy += e->walkers[j].Egy_Lcl ;
			  //printf("energy = %f %f\n", e->energy,  e->walkers[j].Egy_Lcl);
			  break;
			default:
			  if (num_branches > 10)
				num_branches = 10;
			  iestep+= e->walkers[j].Egy_Lcl * (double) num_branches;
			  ie2step+= e->walkers[j].Egy_Lcl *e->walkers[j].Egy_Lcl *(double) num_branches ;
			  e->energy += e->walkers[j].Egy_Lcl * (double) num_branches;
			  for (k =1; k < num_branches; k++)
				{//start for
				  e->num_walkers++;
				  copy_walker(&(e->walkers[j]), &(e->walkers[e->num_walkers-1]));
				  
				}//end for
			  break;
			}//end switch
		  
	}//end for in all walkers
	  dt_avg = dt_avg / Iaccept;
	  //updating the average of the energy of the ensemble
	  e->energy = e->energy / (double)(e->num_walkers);
	  iestep/=  ((double) e->num_walkers);
	  ie2step/= ((double) e->num_walkers) ;
	  //printf("** iestep  %f %f\n", iestep, ie2step);
	  //adding the average energy
	  *estep += iestep;
	  *e2step += ie2step;
	  
	  //debug information
	  //printf("** %d %f \n", i, e->energy);
	  
	}//ends for in all steps
  *estep= *estep / ((double) num_steps);
  *e2step= *e2step / ((double) num_steps);
  //printf("estep %f %f\n", *estep, *e2step);
}











