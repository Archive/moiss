#ifndef RAND
#define RAND

double  normal_dist (void);

/* Constants for our own exponential, cauchy and gaussian
   random number generators, taken from Fishman */
double q2_exp;
double b_exp,p_exp,B_exp,D_exp,a_exp,c_exp,A_exp,H_exp;
double a_cauchy,b_cauchy,q_cauchy;
double W_cauchy,A_cauchy,B_cauchy;
double H_cauchy,P_cauchy;
double Y_normal,F_normal;

#endif  /* RAND */


