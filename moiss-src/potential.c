#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <stdio.h>
#include <glib.h>
#include "moiss.h"
#include "potential.h"
#include "trialfun.h"
#include "gui.h"

double V_HOs(walker * w)
     /* Multidimensional Harmonic Oscillator Potential */
     /*OJO para una sola particula */
{
    int i;
    double q, q_q = 0;
    for (i = 0; i < w->dim; i++) {
	q = w->R[i];
	q_q += q * q;
    }
    return (0.5 * HO_const* (q_q));
}

double V_Morse(walker * w)
{
    int i;
    double q, q_q = 0;
    for (i = 0; i < w->dim; i++) {
      q_q +=w->R[i]*w->R[i] ;
    }
    q=sqrt(q_q);
    return 0.5*(exp(-2*q)-2*exp(-q));
}



void GD_HOs(walker * w)
{
  int i;
  double q, q_q = 0;
  double t1 = trial_alpha;
  t1=t1*t1;
  for (i = 0; i < w->dim; i++) {
    q = w->R[i];
    q_q += q * q;
    w->grad[i]=-t1*q;
  }
  w->Psi= exp(-0.5 * t1* (q_q));
  for (i = 0; i < w->dim; i++) 
    w->grad[i]*=(w->Psi);
    
  w->del2 = (w->Psi)*( -w->dim * t1 + t1*t1 * q_q  );
  

}

double V_hydrogen(walker * w)
     /*  3 D hydrogen potential  */
{
  int i;
  double q, q_q = 0;
  
  for (i = 0; i < w->dim; i++) {
    q = w->R[i];
    q_q += q * q;
  }
  return (-1.0 / sqrt(q_q));
}

void GD_hydrogen(walker *w)
{
  int i;
  double q, q_q = 0;
  double r;
  for (i = 0; i < w->dim; i++) {
    q = w->R[i];
    q_q += q*q;
  }
  r = sqrt(q_q);
  w->Psi = trialfun_cte_H * exp(-trial_alpha * r);
  for (i = 0; i < w->dim; i++) 
    w->grad[i]=-(w->Psi)*trial_alpha*w->R[i]/r;
  
  w->del2=(w->Psi)*trial_alpha*(r*trial_alpha-2.0) /r;
}

double V_well(walker * w)
     /* Multidimensional Infinite Potential Well */
     /* centado en cero */
     /*OJO para una sola particula */
{

    int i, purito = 0;
    for (i = 0; i < w->dim; i++)
	if (w->R[i] >= (-half_box[i]) &&
	    w->R[i] <= (half_box[i]))
	    purito++;
    if (purito == w->dim)
	return 0.0;
    else
	return tunnel_h;
}

double V_tunnel_open(walker * w)
     /* Multidimensional Finite Potential Well */
     /* el extremo a esta en el origen */
     /* el extremo b esta en tunnel_a */
     /*OJO para una sola particula */
{
    int i, purito = 0;
    for (i = 0; i < w->dim; i++)
	if (w->R[i] < 0.0)
	    purito++;
    if (purito > 0)
	return 1000;
    else {
	for (i = 0; i < w->dim; i++)
	    if (w->R[i] > tunnel_a)
		purito++;
	if (purito > 0)
	    return tunnel_h;
	else
	    return 0.0;
    }
}

double V_bump(walker * w)
     /* Multidimensional Potential Well with a bump */
     /* el extremo a esta en el origen */
     /* el extremo b esta en tunnel_a */
     /*OJO para una sola particula */
{
    int i, purito = 0;
    for (i = 0; i < w->dim; i++)
	if (w->R[i] <= 0 ||
	    w->R[i] >= (half_box[i] * 2))
	    purito++;
    if (purito > 0.0)
	return 1000.0;
    else {
	purito = 0;
	for (i = 0; i < w->dim; i++)
	    if (w->R[i] >= tunnel_a &&
		w->R[i] <= tunnel_b)
		purito++;
	if (purito > 0)
	    return tunnel_h;
	else
	    return 0.0;
    }
}

double V_annular_well(walker * w)
     /* Multidimensional Harmonic Oscillator Potential */
     /*OJO para una sola particula */
{
    int i;
    double q, q_q = 0, r;
    for (i = 0; i < w->dim; i++) {
	q = w->R[i];
	q_q += q * q;
    }
    r = sqrt(q_q);
    if (r < tunnel_a)
	return tunnel_h;
    else if (tunnel_a < r && r < tunnel_b)
	return 0;
    else
	return tunnel_h;
}


double V_circular_well(walker * w)
     /* Multidimensional Harmonic Oscillator Potential */
     /*OJO para una sola particula */
{
    int i;
    double q, q_q = 0, r;
    for (i = 0; i < w->dim; i++) {
	q = w->R[i];
	q_q += q * q;
    }
    r = sqrt(q_q);
    if (r < tunnel_a)
	return 0;
    else
	return tunnel_h;
}






/*********************************************************************************************/
/*********************************************************************************************/
/*********************************************************************************************/
/*****************************  Se tiene que arreglar   **************************************/
/*********************************************************************************************/
/*********************************************************************************************/
/*********************************************************************************************/

double V_Lehnard_Jones (walker * w) {
    /* You must set lehnard_jones_sigma and epsilon to something useful
       Some interesting values are:
       Sigma                     Epsilon
       Helium: 2.6369            10.948
       Neon: 2.749               35.6
       Argon: 3.405              119.4
       (taken from:
       "A variational Monte Carlo study of argon, neon and helium
       clsusters"
       Rick S. W., Lynch, D.L. and Doll J. D.
       J. Chem. Phys. 95(5) 3506
       You should be consistent with the units for the following potential
       V(rij) = 4 epsilon (sigma 12 / rij) - (sigma 6 / rij))
       
    */

    int i, j, k;
    double factor, po = 0;
    double d;

    factor = 4.0 * lehnard_jones_epsilon; 
    
    
    for (i = 0; i < num_pars; i++) {
	for (j =i + 1; j < num_pars; j++){
	    /*  d = distance(new_walker_from(w), i, j); */
	    d = 0;
	    for (k = 0; k < w->dim; k++) {

		d = d + pow(( w->R[k+i] -
			     w->R[k+j]),2.0);
		
	    }
	}
	d = sqrt(d);
		po = po +
		pow((lehnard_jones_sigma/d),12.0) -
		pow((lehnard_jones_sigma/d), 6.0);
	    }
   // printf("%f \n",factor * po);
    return factor * po;
}
double V_He(walker *w)
{
    int j;
    double r1, r2, q1, q2, q_q1 = 0, q_q2 = 0;
    double r12, q12, q_q12 = 0;
    
    for (j = 0; j < w->dim; j++) {
	q1 = w->R[j];
	q2 = w->R[j+3];
	q12 = T(q1, q2);
	q_q1 += q1 * q1;
	q_q2 += q2 * q2;
	q_q12 += q12 * q12;
    }
    r12 = sqrt(q_q12);
    r1 = sqrt(q_q1);
    r2 = sqrt(q_q2);
    return -Z_He / r1 - Z_He / r2 + 1 / r12;
}

void GD_HE_Hilleras(walker *w)
{
  int j;
  double r1, r2, q1, q2, q_q1 = 0, q_q2 = 0;
  double a=1.6875;
  
  for (j = 0; j < 3; j++) {
    q1 = w->R[j];
    q2 = w->R[j+3];;
    q_q1 += q1 * q1;
    q_q2 += q2 * q2;
  }
  
  r1 = sqrt(q_q1);
  r2 = sqrt(q_q2);
   w->Psi= exp(-a * (r1+r2));
   w->del2=2*a*(w->Psi)*(-r1-r2+(a*r1*r2))/(r1*r2);
   
   for (j = 0; j < 3; j++){ 
    w->grad[j]= -a *   w->R[j]*(w->Psi)/r1;
        w->grad[j+3]= -a*w->R[j+3]*(w->Psi)/r2;
  }

  w->del2=2*a*(w->Psi)*(-r1-r2+(a*r1*r2))/(r1*r2);
    


 
}



void GD_HE_Kellner(walker *w)
{
  int j;
  double r1, r2, q1, q2, q_q1 = 0, q_q2 = 0;
  double t1,t2,t3,t4,t5,t6,t7,t8,t9;
  for (j = 0; j < w->dim; j++) {
    q1 = w->R[j];
    q2 = w->R[j+3];
    q_q1 += q1 * q1;
    q_q2 += q2 * q2;
  }
  r1 = sqrt(q_q1);
  r2 = sqrt(q_q2);
  t1=exp(-Zprima_2 * (r1+ r2));
  t2=c*(r1-r2);
  t3=cosh(t2);
  t9=sinh(t2);
  t4=t9*trialfun_cte_He_1*t1;
  t5=trialfun_cte_He_1*Zprima_2;
  t6=t5/r1;
  t7=t5/r2;
  t8=r1*r2;
  w->Psi=trialfun_cte_He_1*t1*t3;
  for (j = 0; j < w->dim; j++) {
    w->grad[j]  =w->R[j]   *(-t6*t1*t3+t5*c/r1);
    w->grad[j+3]=w->R[j+3] *(-t7*t1*t3-t5*c/r2);
    }
    w->del2=2.0*trialfun_cte_He_1*t1*
      (t3*c*c*t8+Zprima_2*Zprima_2*t2*t8+r2*c*t9-t9*c*r1-t3*r1-t3*r2)/
      t8;
}



















