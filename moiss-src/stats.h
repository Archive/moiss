/* This file is part of MOISS
   
   Copyright (C) 1998-99 Alan Aspuru Guzik
			 Raul Perusquia Flores
			 Carlos Amador Bedolla
			 Francisco Bustamante
			 Dario Bressanini
   
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
               
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
                           
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
                                    
*/



#ifndef __STATS_H__
#define __STATS_H__
#include "../gmoiss-src/gmoiss.h"
/* does an histogram of walker positions (call each loop) */
/* requires special initialization in moiss.c */
void stats_histogram (ensemble *e);
/* a dummy stats function used when you haven't written a stats function
   yet */
void stats_dummy (ensemble *e);
void do_stats (ensemble * e, GList * stats);
BooleanType stats_check(GList * stats, StatType s_type);
BooleanType stats_check_average(GList * stats);
BooleanType stats_check_wavef(GList * stats);
#endif /* __STATS_H__ */

