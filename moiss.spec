%define ver      0.5
%define prefix   /usr

Summary:   Molecular Integration and Sampling Software
Name:      gmoiss
Version:   %ver
Release:   1
Copyright: GPL
Group:     Scientific
Source:   gmoiss-0.5.tar.gz
URL:       http://moiss.pquim.unam.mx/moiss/
Packager: Alan Aspuru and Daniel Manrique  <aspuru@eros.pquim.unam.mx>
Requires: gtk+ >= 1.1.1
Requires: gnome-libs

%description
This is a n-particle n-dimensions Diffusion Monte Carlo (DMC) Schroedinger
equation integrator. It is very easy to implement new potentials and theory 
levels, because modularity is one of our highest priorities.  

Our goal is to provide a freely available general purpose Quantum Monte Carlo 
(QMC) program released under the GNU/GPL license.  
		        						       
%prep
%setup -q
CFLAGS=$RPM_OPT_FLAGS \
./configure --prefix=/usr

%build
make

%install
make install

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-, root, root)
%doc README COPYING ChangeLog NEWS TODO AUTHORS ABOUT-NLS  INSTALL
%{prefix}/bin/moiss
%{prefix}/bin/gmoiss


%changelog
* Tue Mar 16 1999 Daniel Manrique <roadmr@eros.pquim.unam.mx>
- Second RPM built ever :)

* Thu Oct 15 1998 Alan Aspuru <aspuru@eros.pquim.unam.mx>
- First RPM built ever


